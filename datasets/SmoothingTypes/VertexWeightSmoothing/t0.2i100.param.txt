Target edge length: 0.2
Adjusted length: 0.528755
Iterations: 100
Alpha: 1.333
Beta: 0.8
Smoothing type: VertexWeights
Limit Flips: false
Straightening Type: None
Collapsing Order: Splitweight