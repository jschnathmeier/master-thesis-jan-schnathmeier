import sys
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import csv
from pathlib import Path


def visualizeall(files, info, name, labels=[]):
  #visualize(files, info, name, 0, 1)
  #return
  for i in range(7):
    visualize(files, info, name, 0, i+1, -1,labels)
  visualize(files, info, name, 0, 2, 3,labels)

def visualize(files, info, name, Xaxis, Yaxis, Ydivisor, labels=[]):
  cols=['iteration','time_ms','basefaces','metafaces','valence_avg','valence_sd','edgelength_avg','edgelength_sd']
  x=[]
  y=[]
  for file in files:
    with open(file,'r') as csvfile:
      plots= csv.reader(csvfile, delimiter=',')
      newx=[]
      newy=[]
      for row in islice(plots, 1, None):
        newx.append(float(row[Xaxis]))
        if (Ydivisor == -1):
          newy.append(float(row[Yaxis]))
        else:
          newy.append(float(row[Yaxis])/float(row[Ydivisor]))
    x.append(newx)
    y.append(newy)

  pltlist=[]
  for i in range(len(x)):
    pltlist.append(x[i])
    pltlist.append(y[i])
    if not labels==[]:
      line, = plt.plot(*pltlist[-2::])
      line.set_label(labels[i])
    else:
      plt.plot(*pltlist[-2::])
      
  if not labels==[]:
    plt.legend()
    
  plt.title(str(len(files))+' Runs, ' + info + ', 100 It, T 0.2')

  plt.xlabel(cols[Xaxis])
  if (Ydivisor == -1):
    plt.ylabel(cols[Yaxis])
  else:
    plt.ylabel(cols[Yaxis] + ' / ' + cols[Ydivisor])
  axes = plt.gca()
  if ((Yaxis==1) or (Yaxis==2) or (Yaxis==7)):
    axes.set_yscale('log')

  Path("plots").mkdir(parents=True, exist_ok=True)
  
  if (Ydivisor == -1):
    plt.savefig('plots/' + name + '-' + cols[Xaxis] + '-' + cols[Yaxis] + '.png', bbox_inches = 'tight', pad_inches = 0)
    plt.savefig('plots/' + name + '-' + cols[Xaxis] + '-' + cols[Yaxis] + '.pdf', bbox_inches = 'tight', pad_inches = 0)
  else:
    plt.savefig('plots/' + name + '-' + cols[Xaxis] + '-' + cols[Yaxis] + '_' + cols[Ydivisor] + '.png', bbox_inches = 'tight', pad_inches = 0)
    plt.savefig('plots/' + name + '-' + cols[Xaxis] + '-' + cols[Yaxis] + '_' + cols[Ydivisor] + '.pdf', bbox_inches = 'tight', pad_inches = 0)
  #plt.show()
  plt.clf()
 
def main(argv):
  files=[]
  for i in range(5):
    files.append('HeuristicMedium' + str(i) + '.csv')
  for i in range(5):
    files.append('Heuristic2Medium' + str(i+1) + '.csv')
  for i in range(10):
    files.append('RandomMedium' + str(i) + '.csv')
    
  files.append('HeuristicMedium0-average.csv')
  files.append('Heuristic2Medium1-average.csv')
  files.append('RandomMedium0-average.csv')
  
  visualizeall(files[0:5], 'Heuristic 1', 'Heuristic1')
  visualizeall(files[5:10], 'Heuristic 2', 'Heuristic2')
  visualizeall(files[10:20], 'Random Collapses', 'Random')
  
  visualizeall(files[20:23], 'Averages', 'COAverages', ['Heuristic 1', 'Heuristic 2', 'Random'])
    
if (__name__ == "__main__"):
    main(sys.argv)
