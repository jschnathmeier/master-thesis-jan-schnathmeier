from glob import glob
import os
import sys
import csv
import math

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def makeaverage(files_list):
  data=[]

  for file in files_list:
    with open(file, 'r') as f:
      reader = csv.reader(f)
      i = -1
      for row in reader:
        i+=1
        if i==0:
          continue
        if len(data)<i:
          data.append([[],[],[],[],[],[],[],[]])
        for j in range(8):
          data[i-1][j].append(float(row[j]))
          
  for i in range(len(data)):
    for j in range(len(data[i])):
      #print(data[i][j])
      if (len(data[i][j])!=0):
        data[i][j] = sum(data[i][j])/len(data[i][j])
        
  newfile = os.path.splitext(files_list[0])[0] + "-average.csv"
  with open(newfile, 'a') as fn:
    fn.write('iteration,time-ms,basefaces,metafaces,valence.avg,valence.sd,edgelength.avg,edgelength.sd\n')
    for row in data:
      for col in row:
        if (col == row[-1]):
          fn.write(''.join(str(col)))
        else:
          fn.write(''.join(str(col)) + ',')
      fn.write('\n')
    print("Created output " + newfile)
      
                   
def main(argv):
  files=[]
  for i in range(5):
    files.append('HeuristicMedium' + str(i) + '.csv')
  for i in range(5):
    files.append('Heuristic2Medium' + str(i+1) + '.csv')
  for i in range(10):
    files.append('RandomMedium' + str(i) + '.csv')
  makeaverage(files[0:5])
  makeaverage(files[5:10])
  makeaverage(files[10:20])
    
if (__name__ == "__main__"):
    main(sys.argv)
