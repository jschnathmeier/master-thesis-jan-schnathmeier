Target edge length: 0.2
Adjusted length: 2.27963
Iterations: 100
Alpha: 1.333
Beta: 0.8
Smoothing type: VertexDistances
Limit Flips: true
Straightening Type: None
Collapsing Order: Splitweight