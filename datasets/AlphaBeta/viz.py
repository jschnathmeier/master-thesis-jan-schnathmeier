import sys
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import csv
from pathlib import Path


def visualizeall(files, info, name, labels=[]):
  #visualize(files, info, name, 0, 1)
  #return
  for i in range(7):
    visualize(files, info, name, 0, i+1, -1,labels)
  visualize(files, info, name, 0, 2, 3,labels)

def visualize(files, info, name, Xaxis, Yaxis, Ydivisor, labels=[]):
  cols=['iteration','time_ms','basefaces','metafaces','valence_avg','valence_sd','edgelength_avg','edgelength_sd']
  x=[]
  y=[]
  for file in files:
    with open(file,'r') as csvfile:
      plots= csv.reader(csvfile, delimiter=',')
      newx=[]
      newy=[]
      for row in islice(plots, 1, None):
        newx.append(float(row[Xaxis]))
        if (Ydivisor == -1):
          newy.append(float(row[Yaxis]))
        else:
          newy.append(float(row[Yaxis])/float(row[Ydivisor]))
    x.append(newx)
    y.append(newy)

  pltlist=[]
  for i in range(len(x)):
    pltlist.append(x[i])
    pltlist.append(y[i])
    if not labels==[]:
      line, = plt.plot(*pltlist[-2::])
      line.set_label(labels[i])
    else:
      plt.plot(*pltlist[-2::])
      
  if not labels==[]:
    plt.legend()
    
  plt.title(str(len(files))+' Runs, ' + info + ', 100 It, T 0.2')

  plt.xlabel(cols[Xaxis])
  if (Ydivisor == -1):
    plt.ylabel(cols[Yaxis])
  else:
    plt.ylabel(cols[Yaxis] + ' / ' + cols[Ydivisor])
  axes = plt.gca()
  if ((Yaxis==1) or (Yaxis==2) or (Yaxis==7)):
    axes.set_yscale('log')

  Path("plots").mkdir(parents=True, exist_ok=True)
  if (Ydivisor == -1):
    plt.savefig('plots/' + name + '-' + cols[Xaxis] + '-' + cols[Yaxis] + '.png', bbox_inches = 'tight', pad_inches = 0)
    plt.savefig('plots/' + name + '-' + cols[Xaxis] + '-' + cols[Yaxis] + '.pdf', bbox_inches = 'tight', pad_inches = 0)
  else:
    plt.savefig('plots/' + name + '-' + cols[Xaxis] + '-' + cols[Yaxis] + '_' + cols[Ydivisor] + '.png', bbox_inches = 'tight', pad_inches = 0)
    plt.savefig('plots/' + name + '-' + cols[Xaxis] + '-' + cols[Yaxis] + '_' + cols[Ydivisor] + '.pdf', bbox_inches = 'tight', pad_inches = 0)
  #plt.show()
  plt.clf()
 
def main(argv):
  files=[]
  for i in range(20):
    files.append('Sphere1.5-0.66/t0.2i1' + str(i).zfill(2) + '.csv')
  for i in range(20):
    files.append('Sphere1.33-0.8/t0.2i1' + str(i).zfill(2) + '.csv')
  for i in range(20):
    files.append('Sphere2-0.5/t0.2i1' + str(i).zfill(2) + '.csv')
    
  files.append('Sphere1.5-0.66/t0.2i100-average.csv')
  files.append('Sphere1.33-0.8/t0.2i100-average.csv')
  files.append('Sphere2-0.5/t0.2i100-average.csv')
  
  visualizeall(files[0:20], r'$\alpha$=3/2, $\beta$=2/3', 'A1.5B0.66')
  visualizeall(files[20:40], r'$\alpha$=4/3, $\beta$=4/5', 'A1.33B0.8')
  visualizeall(files[40:60], r'$\alpha$=2, $\beta$=1/2', 'A2B0.5')
  
  visualizeall(files[60:63], 'Averages', 'ABAverages', [r'$\alpha$=1.5, $\beta$=0.66', r'$\alpha$=1.33, $\beta$=0.8', r'$\alpha$=2, $\beta$=0.5'])
    
if (__name__ == "__main__"):
    main(sys.argv)
