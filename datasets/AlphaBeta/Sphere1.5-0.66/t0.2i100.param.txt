Target edge length: 0.2
Adjusted length: 0.682457
Iterations: 100
Alpha: 1.5
Beta: 0.666
Smoothing type: VertexDistances
Limit Flips: false
Straightening Type: None
Collapsing Order: Splitweight