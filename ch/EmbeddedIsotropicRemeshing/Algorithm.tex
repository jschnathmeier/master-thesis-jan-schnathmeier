\section{Algorithm}
\label{sec:Algorithm}

As can be derived from its name, Incremental Isotropic Remeshing works incrementally, with a series of operations being performed in every iteration. Our modified approach for Embedded Isotropic Remeshing works in the same way, but with a few differences in the individual steps. The parameters of our Embedded Remeshing are the following:
\begin{itemize}
\item The embedding $\Phi$.
\item A target edge length $T$ which the algorithm iterates towards.
\item Tolerance bounds $\alpha$ and $\beta$, which provide an acceptable edge length interval around $T$.
\item A number of iterations $n$.
\item Optional: Different parameters for collapsing, smoothing, and straightening options: $\left\{co, sm, st\right\}$.
\end{itemize}

\begin{algorithm}[H]
\caption{Embedded Incremental Isotropic Remeshing}\label{alg:Remeshing}
\begin{algorithmic}[1]
\Function{RemeshEmbedding}{$\Phi, T, n, \alpha, \beta, co, sm, st$}
\State $\text{upper\_bound} := \alpha\cdot T$
\State $\text{lower\_bound} := \beta\cdot T$
\For {$i:=0; i<n; ++i$}
\State $\textsc{Splits}(\Phi, \text{upper\_bound})$
\State $\textsc{Collapses}(\Phi, \text{lower\_bound}, co)$
\State $\textsc{Flips}(\Phi)$
\State $\textsc{Smoothing}(\Phi, sm)$
\State $\textsc{Straightening}(\Phi, st)$
\EndFor
\State \Return $\Phi$
\EndFunction
\end{algorithmic}
\end{algorithm}

Algorithm \ref{alg:Remeshing} is the Embedded Isotropic Remeshing algorithm. It is apparent that the main structure of the algorithm is the same as for conventional Incremental Isotropic Remeshing, aside from an added straightening step. Since straightening is a new addition to the algorithm, we make it optional in our implementation. We also aim to optimize properties of the \textsc{Collapses} and \textsc{Smoothing} steps, and offer multiple methods for handling these steps. As for the tolerance bounds, they default to $\alpha=\frac{4}{3}, \beta=\frac{4}{5}$, but may need further refinement as well, since embedded edges are not always straight, meaning length can vary a lot more.

\begin{wrapfigure}[15]{r}{0.3\textwidth}
  \begin{centering}
   \includegraphics[width=0.3\textwidth]{img/artifactsplits.png}\par
  \end{centering}
  \caption{Ugly artifacts on $\mathcal{B}$}
  \label{fig:artifactsplits}
\end{wrapfigure}

\textsc{Straightening} iterates over $v^{\mathcal{M}}_i\in V^{\mathcal{M}}$ and removes the embedding $\left\{e^\mathcal{B}_i\right\}\in \Phi\left(E^\mathcal{M}\right), \left\{e^\mathcal{B}_i\right\}=\Phi\left(e^{\mathcal{M}}_i\right)$, for every meta edge $e^\mathcal{M}_i\in E^{\mathcal{M}}$ adjacent to $v^\mathcal{M}_i$. This clears up the entire patch around $v^\mathcal{M}_i$ and enables a cleanup of the underlying part of the base mesh. Afterwards, the meta edges $e^\mathcal{M}_i$ are retraced. While this is a costly operation, the gain in mesh quality may be worth it. Detailed comparisons can be found in Chapter \ref{ch:Evaluation} which contains tests and evaluation of test data.

The $st$(raightening) parameter determines whether no straightening, edgewise straightening, or patchwise straightening should be performed. Edgewise straightening is a simpler form of straightening where, rather than retracing each meta vertex patch, the meta edges are instead retraced on an edge-wise basis. Since an edge is always connected to two vertices, this saves half the retracing operations, however it leaves more artifacts in the underlying base mesh. Figure \ref{fig:artifactsplits} shows artifacts that appeared in some of our runs due to a lack of cleanup for the underlying base mesh.

Similar to $st$, there are parameters $co$(llapses) and $sm$(oothing), switching between different implementations for their respective functions. The next Section \ref{sec:CollapseOrder} discusses the \textsc{Collapses} step, and Section \ref{sec:Smoothing} discusses the \textsc{Smoothing} step.
