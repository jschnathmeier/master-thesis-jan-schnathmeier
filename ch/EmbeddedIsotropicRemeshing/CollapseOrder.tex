\section{Collapse Order}
\label{sec:CollapseOrder}

\begin{wrapfigure}[9]{r}{0.65\textwidth}
  \vspace{-30pt}
   \def\svgwidth{0.65\textwidth}
   {\centering
   \input{img/CollapsingValence.pdf_tex}\par
   }
  \vspace{-5pt}
  \caption{Valence change due to collapse.}
  \label{fig:CollapsingValence}
\end{wrapfigure}

The Incremental Isotropic Remeshing algorithm performs collapses on edges whose length falls below $\beta\cdot T$. However, a naive implementation of those collapses can lead to various problems. Consider the following property:
\begin{itemize}
\item On a manifold trimesh, collapsing a vertex $v^\mathcal{M}_A$ into a vertex $v^\mathcal{M}_B$ results in a new valence of $v^\mathcal{M}_B$: $\left|v^\mathcal{M}_B\right|_{\text{new}}=\left|v^\mathcal{M}_B\right|-1+\left|v^\mathcal{M}_A\right|-3=\left|v^\mathcal{M}_B\right|+\left|v^\mathcal{M}_A\right|-4$.
\begin{itemize}
\item Figure \ref{fig:CollapsingValence} visualizes this property; $v^\mathcal{M}_B$ loses one neighbor since $v^\mathcal{M}_A$ is removed. Three neighbors of $v^\mathcal{M}_A$ are irrelevant to $v^\mathcal{M}_B$, as they are already neighbors of $v^\mathcal{M}_B$ or $v^\mathcal{M}_B$ itself (fig: red edges). This leaves $v^\mathcal{M}_B$ with a valence change of $-1$ for losing $v^\mathcal{M}_A$, $\left|v^\mathcal{M}_A\right|$ for the neighbors of $v^\mathcal{M}_A$ and $-3$ for the irrelevant neighbors of $v^\mathcal{M}_A$.
\end{itemize}
\item Effectively this means that whenever a vertex $v^\mathcal{M}_A$ with $\left|v^\mathcal{M}_A\right|>4$ is collapsed into another vertex $v^\mathcal{M}_B$, $\left|v^\mathcal{M}_B\right|$ increases.
\end{itemize}
Since the average valence is usually close to 6, collapsing a vertex $v^\mathcal{M}_A$ into a vertex $v^\mathcal{M}_B$ will most often increase the valence of $v^\mathcal{M}_B$. At the same time, the valence of the two vertices connected to both $A$ and $B$ is decremented by one. Given this property, it is now clear that the order in which collapses are performed can make a significant difference. Particularly, collapsing edges in the order in which they appear in the data structure can result in the same few vertices always being collapsed first. This results in some vertices gaining huge valence, which must then be compensated for in the \textsc{Flip} part of the algorithm.

This problem can be simply addressed, by checking for collapses in a random order. This avoids any unfair preferential treatment of earlier vertices. However, bad randomization and performing too many collapses in one iteration can still lead to problems. Especially in the case where Embedded Remeshing is used as a way to initialize an embedding. It is usually the case that at the start $\Phi\left(\mathcal{M}\right)=\mathcal{B}$ and $T > \max\left(\left|e^\mathcal{M}_i\right| \quad e^\mathcal{M}_i\in \Phi\left(E^\mathcal{M}\right)\right)$, so almost every edge should be collapsed in a single operation, and the randomized order defines the remaining embedding.

Since collapsing too much at once introduces randomness into the result, there needs to be a rule to mitigate excessive collapsing. Hormann et al. present a method locking vertices \cite{hormann1999hierarchical} that is very applicable here. We add the following steps to \textsc{Collapse}:

\begin{enumerate}
\item Unlock all vertices $V^{\mathcal{M}}$.
\item Disallow collapsing locked vertices.
\item Before a vertex $v^\mathcal{M}_i\in V^{\mathcal{M}}$ is collapsed, lock all of its neighbors.
\end{enumerate}

\begin{wrapfigure}[15]{r}{0.5\textwidth}
  \vspace{-15pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/HormannGrid.pdf_tex}\par
   }
  \vspace{-5pt}
  \caption{Maximal collapse distribution, $\frac{1}{3}\left|V^{\mathcal{M}}\right|$}
  \label{fig:HormannGrid}
\end{wrapfigure}

These simple locks add a wonderful property, in that there can be no chain of collapses (eg. collapse $v_a$ into $v_b$ and then $v_b$ into $v_c$) in a single iteration of \textsc{Collapse}. This also distributes the collapses quite evenly, retaining a balanced mesh structure. Even more, the total number of possible collapses per iteration is now limited to at most $\frac{1}{3}\left|V^{\mathcal{M}}\right|$, since the average valence is 6. Figure \ref{fig:HormannGrid} shows a mesh where the green vertices can be collapsed and the red vertices will be locked in turn; a configuration like that collapses the maximum of $\frac{1}{3}\left|V^{\mathcal{M}}\right|$ vertices. In practice, much fewer collapses happen since they are not distributed as evenly, and not every edge is short enough to be collapsed.

In our implementation, there are additional constraints to consider in order to optimize the collapse order. Due to the nature of the embedding, it is sometimes necessary to split edges of $E^\mathcal{B}$ in order to make space to embed edges of $\Phi\left(E^\mathcal{M}\right)$.  These new vertices and edges of $\mathcal{B}$ are temporary and only inserted to guarantee connectivity, and whenever they are no longer needed they are deleted. We want to collapse in an order that minimizes the amount of split operations that are performed on the base mesh $\mathcal{B}$ due to pre-processing.

From this perspective it makes sense to give embedded meta edges $e^{\mathcal{M}}_i\in \Phi\left(E^\mathcal{M}\right)$ weights, based on the non-original base edges they consist of: $e^{\mathcal{M}}_i=\left\{e^{\mathcal{B}_i}\right\}, e^{\mathcal{B}_i}\in E^{\mathcal{B}}$. For this purpose we assign a weight function for base edges $e^{\mathcal{B}_i}$.

\begin{equation}\label{eq:BaseEdgeWeight}
 w^{\mathcal{B}}(e^{\mathcal{B}}_i) := \bigg\{\begin{array}{lr}
        1, & \text{for } e^{\mathcal{B}}_i\in E^{\mathcal{B}'}\nonumber \\
        2\cdot w_{\mathcal{B}}\left(anc\left(e^{\mathcal{B}}_i\right)\right), & \text{otherwise} \nonumber
        \end{array}\nonumber
\end{equation}

\begin{figure}
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/SplitWeights.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Weights of new edges of $\mathcal{B}$ after splits.}
  \label{fig:SplitWeights}
\end{figure}

Where $E^{\mathcal{B}'}$ denotes the \textit{original} edges of $\mathcal{B}$ and $anc\left(e^{\mathcal{B}}_i\right)$ returns the \textit{ancestor} of $e^{\mathcal{B}}_i$, meaning the edge $e^{\mathcal{B}}_i$ was split off from. In this way heavily split up areas of $\mathcal{B}$ exponentially increase in weight indicating the need for a cleanup. Figure \ref{fig:SplitWeights} visualizes how this works. Part (a) shows two original faces of $\mathcal{B}$, where all edges have weight 1. In part (b), the central edge is split. The two edges representing the original edge retain weight 1, whereas the two new edges (yellow) double their weight to two. Finally, (c) shows a split of a yellow edge where the newly created edges (red) again double their weight to four. This type of edge weighting heavily favors the original edges of $\mathcal{B}$, and also provides a data structure to roll back changes by collapsing away non-original edges.

Given the weight function $w^{\mathcal{B}}$ presented in Equation \ref{eq:BaseEdgeWeight}, weights of meta edges are easily derived by summing over the weights of their base edge components:

\vspace{-10pt}
\begin{equation}\label{eq:MetaEdgeWeight}
w^{\mathcal{M}}\left(e^{\mathcal{M}}_i\right):=\sum_{e^{\mathcal{B}}_i\in\Phi\left(e^{\mathcal{M}}_i\right)} w^{\mathcal{B}}\left(e^{\mathcal{B}}_i\right)
\end{equation}

A possible improvement of such a weight function would be factoring in edge length, but a safer approach is keeping it simple at first to compare performance -- improvements for edge weight heuristics can come later if necessary. For now we use simple edge weights to rank our meta edges.

Using these weights, we observe that base edges of high weight cluster around meta vertices, since the area around them becomes increasingly more split as edges need to reach vertices. Consequently, removing an edge of a triangle often decreases the weights of the remaining edges of the triangle, since the shared vertices become easier to reach. At worst, the weight remains the same. With these observations, and the weighting method presented in Equation \ref{eq:MetaEdgeWeight}, we derive a heuristic that orders collapses based on the approximate weight changes they would introduce to the mesh (without explicitly performing the collapses).

\begin{wrapfigure}[9]{r}{0.5\textwidth}
  \vspace{-5pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/CollapseHeuristic.pdf_tex}\par
   }
  \vspace{-5pt}
  \caption{Weight changes upon collapse.}
  \label{fig:CollapseHeuristic}
\end{wrapfigure}

Figure \ref{fig:CollapseHeuristic} visualizes the edges changed by collapsing the halfedge $h^{\mathcal{M}}_x$ from $v^{\mathcal{M}}_A$ to $v^{\mathcal{M}}_B$. Halfedges $h^{\mathcal{M}}_p$ and $h^{\mathcal{M}}_{on}$ exist only before the collapse, so their weight is factored into our heuristic negatively. $h^{\mathcal{M}}_x$ entering vertex $v^{\mathcal{M}}_B$ is replaced by $|v^{\mathcal{M}}_A|-3$ halfedges entering $v^{\mathcal{M}}_B$, and the topology around the vertices they come from does not change. We estimate that each of these edges increases in weight by $w^{\mathcal{M}}(h^{\mathcal{M}}_x)$. Thus we derive the following collapse heuristic:

\vspace{-10pt}
\begin{equation}\label{eq:CollapseHeuristic}
\textsc{ch}\left(h^{\mathcal{M}}_x\right) := \left(\left|v^{\mathcal{M}}_A\right|-4\right)\cdot w^{\mathcal{M}}\left(h^{\mathcal{M}}_x\right) - w^{\mathcal{M}}\left(h^{\mathcal{M}}_p\right) - w^{\mathcal{M}}\left(h^{\mathcal{M}}_{on}\right)
\end{equation}

Where $v^{\mathcal{M}}_A$ is the \textit{from\_vertex} and $v^{\mathcal{M}}_B$ is the \textit{to\_vertex} of $h^{\mathcal{M}}_x$, and $h^{\mathcal{M}}_p, h^{\mathcal{M}}_{on}$ are the previous halfedge and opposite next halfedge of $h^{\mathcal{M}}_x$ respectively. Using this heuristic from Equation \ref{eq:CollapseHeuristic}, we sort all meta edges from low to high values, and check low value halfedges for collapses first. Given the properties optimized by this heuristic, it should perform better than collapsing based on a randomized set of edges --  a detailed comparison of these methods can be found in Section \ref{subsec:HeuristicVsRandomCollapses}.

\textbf{Note on non-triangles}: The heuristic described above assumes that the faces next to $h^{\mathcal{M}}_x$ and its opposite halfedge $h^{\mathcal{M}}_{x\_o}$ are triangles. It is, however, easily extensible to non-triangles in the following way:

\vspace{-10pt}
\begin{align}\label{eq:CollapseHeuristicNonTriangles}
\textsc{ch}'\left(h^{\mathcal{M}}_x\right) := \left(\left|v^{\mathcal{M}}_A\right|-2-\textsc{tr}\left(h^{\mathcal{M}}_x\right)-\textsc{tr}\left(h^{\mathcal{M}}_{x\_o}\right)\right)\cdot w^{\mathcal{M}}\left(h^{\mathcal{M}}_x\right) \nonumber \\
- \left(\textsc{tr}\left(h^{\mathcal{M}}_x\right)\cdot w^{\mathcal{M}}\left(h^{\mathcal{M}}_p\right)\right) -  \left(\textsc{tr}\left(h^{\mathcal{M}}_{x\_o}\right)\cdot w_{\mathcal{M}}\left(h^{\mathcal{M}}_{on}\right)\right)
\end{align}

Where  $\textsc{tr}\left(h^{\mathcal{M}}_x\right)$ is a function that returns $1$ if $h^{\mathcal{M}}_x$ is part of a triangle and $0$ otherwise. It can be seen that Equation \ref{eq:CollapseHeuristicNonTriangles} and Equation 
\ref{eq:CollapseHeuristic} evaluate to the same output if the faces of $h^{\mathcal{M}}_x$ and $h^{\mathcal{M}}_{x\_o}$ are both triangles. If either of them is not a triangle, then the factor before $w^{\mathcal{M}}\left(h^{\mathcal{M}}_x\right)$ is incremented by $1$, and the weight of the corresponding edge is not subtracted. This represents that edge being connected to $v^\mathcal{M}_B$ instead of being deleted.

Of course, an even more precise method than such a heuristic would be to simulate every halfedge collapse and find their values thusly, however this would be much more computationally expensive than evaluating a relatively simple heuristic. The tradeoff between time gained due to collapsing in an optimized order versus the time lost by taking longer to evaluate what order to collapse in should favor using heuristics.
