\section{Implementation Details}
\label{sec:ImplementationDetails}

\begin{wrapfigure}[17]{r}{0.55\textwidth}
  \vspace{-20pt}
  \begin{centering}
   \includegraphics[width=0.55\textwidth]{img/extremeartifacts.png}\par
  \end{centering}
  \caption{Ugly artifacts on $\mathcal{B}$}
  \label{fig:extremeartifacts}
\end{wrapfigure}
  \vspace{-5pt}
Now that all the components of Embedded Remeshing have been presented, a few more things have to be said regarding optimizing the algorithm, and perhaps algorithms on embedded meta meshes in general.
\begin{itemize}
\item \textbf{High valence vertices}: Creating vertices of high valence is costly, since connectivity of new edges $e^{\mathcal{M}}_i$ is often only possible via performing splits on $\mathcal{B}$. This effect cascades as more and more edges are connected to a vertex. As the amount of free space in the area near an embedded vertex decreases, the length of edges created by splits decreases as well. Then, each new meta edge requires a multiple of the splits the previous one required.\footnote{In development there were cases where inefficient implementations increased the number of vertices in a mesh by a factor of 1000 or more.} This makes a careful implementation with respect to meta vertices very important. Figure \ref{fig:extremeartifacts} and Figure \ref{fig:ExtremeSpital} show extreme examples making it clear why optimization is vital.
\end{itemize}

\begin{figure}[H]
  \begin{centering}
  \includegraphics[width=\textwidth]{img/extremespiral.png}
  \end{centering}
\vspace{-20pt}
  \caption{Extreme spiraling artifacts (created deliberately to visually demonstrate the effect).}
  \label{fig:ExtremeSpital}
\end{figure}

In some cases this effect is especially pronounced and forms spirals, similar to the swirls observed by Praun et al. \cite{praun2001consistent}
\begin{itemize}
\item \textbf{Edge bundling}: The same effect can occur when edges are bundled too close together, as seen in Figure \ref{fig:artifactsplits}, although concentration on vertices is more common.
\item \textbf{Cleanup}: Tracing into and deleting embedded meta edges from the base mesh continually creates and deletes elements in the base mesh $\mathcal{B}$. The way in which edge cleanup and garbage collection are used on $\mathcal{B}$ can have a strong impact on performance.
\end{itemize}

Ultimately, algorithms on our data structure require careful planning, given a dense meta mesh. On sparse meta meshes $\mathcal{M}$ (compared to the underlying base mesh $\mathcal{B}$) these problems do not occur often, since there is sufficient room for tracing without changing the underlying base mesh $\mathcal{B}$.
