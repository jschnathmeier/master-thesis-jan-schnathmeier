\subsection{Vertex Distance Smoothing}
\label{subsec:VertexDistanceSmoothing}

Since using vertex distances as weights could fail when presented with unevenly distributed patch-vertices, it makes sense to consider a method that is patch-vertex-distribution invariant. The idea is to select the base vertex for which the maximal distance from the patch vertices is minimal. This should push a smoothed vertex $v^\mathcal{M}_a$ more towards the middle of its patch, as shown in Figure \ref{fig:vertexdistance}.

Vertex Distance Smoothing is mostly the same as Vertex Weight Smoothing as presented in Algorithm \ref{alg:vertexweights}. The difference is in how weights are calculated. Instead of summing over squares as in Equation \ref{eq:vertexweight}, we maximize the minimum distance to \textit{push} $\Phi\left(v^\mathcal{M}_a\right)$ into the center of its patch $P^\mathcal{B}\left(\Phi\left(v^\mathcal{M}_a\right)\right)$.

\begin{wrapfigure}[24]{l}{0.25\textwidth}
  \vspace{-1pt}
   \def\svgwidth{0.25\textwidth}
   {\centering
   \input{img/VertexDistance.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Vertex Distance Smoothing: compare to Figure \ref{fig:vertexweights2}.}
  \label{fig:vertexdistance}
\end{wrapfigure}

We optimize the following weight:

\begin{equation}\label{eq:vertexdistance}
w\left(v^\mathcal{B}_b\right)=\min_{v^{\mathcal{M}}_p\in{b}^\mathcal{M}_P}\left(\textsc{Dist}\left(v^{\mathcal{M}}_b, \Phi\left(v^{\mathcal{B}}_p\right)\right)\right) \rightarrow \max\left(w\right)
\end{equation}

By modifying the \textsc{Score} function to correspond to Equation \ref{eq:vertexdistance} we turn Vertex Weight Smoothing into Vertex Distance Smoothing. Intuitively, we are growing spheres around the embedded meta vertices $\Phi\left(v^\mathcal{M}_i\right)$ of the surrounding patch $P^\mathcal{B}\left(\Phi\left(v^\mathcal{M}_a\right)\right)$, until there is only one vertex left not covered by any sphere. $\Phi\left(v^\mathcal{M}_a\right)$ is then relocated to that vertex.

In the average case, the results of this algorithm should be very similar to those of Vertex Weight Smoothing as seen in Figure \ref{fig:vertexweights1}. However in cases such as Figure \ref{fig:vertexweights2} it would properly smooth the vertex towards the center of the patch as seen in Figure \ref{fig:vertexdistance}.

Overall, it is much harder to smoothe vertices inside complex patches. Figure \ref{fig:SmoothingComplex}.(a) shows a patch with Euler characteristic 1, where the center of the patch vertices lies outside of the patch. Here, the weighted center would lie outside of the face itself, and so would the Vertex Distance according to \ref{fig:vertexdistance}. But in both cases, smoothing would not find the optimal spot for the red vertex to be relocated to.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/SmoothingComplex.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Sample complex or concave patches that may be difficult to smoothe.}
  \label{fig:SmoothingComplex}
\end{figure}

Patches with a convex hull such as Figure \ref{fig:SmoothingComplex}.(b) are also generally harder to smooth properly. Where a vertex is smoothed to may depend on the rather arbitrary curvature of the edges around its patch, if they are curved and long enough. Again, this is a similar problem as the one we observed when using Forest Fire Smoothing.

Since we are working in a 3D space, much more complex patches could be thought of too, on which proper smoothing becomes exceedingly difficult. However in practice, keeping our faces simple somewhat limits how complicated patches surrounding vertices can get. In most cases, especially when using Embedded Remeshing to decimate a meta mesh $\mathcal{M}$ starting from $\Phi\left(\mathcal{M}\right)=\mathcal{B}$, the vast majority of smoothing operations happen on simple patches with low curvature. Thus, it may be optimal to simply choose the method with the best average case performance while ignoring the worst cases.

Due to their strong similarity, it is unclear whether Vertex Weight Smoothing or Vertex Distance Smoothing leads to better result in practice.  In order to offer some variety, and compare methods, we implement the three previously presented options. The tests and evaluation in Chapter \ref{ch:Evaluation} should make it more clear which type of smoothing is preferable. A detailed comparison between the two can be found in Section \ref{subsec:SmoothingTypes}.
