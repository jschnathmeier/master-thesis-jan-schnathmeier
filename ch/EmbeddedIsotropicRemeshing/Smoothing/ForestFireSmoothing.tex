\subsection{Forest Fire Smoothing}
\label{subsec:ForestFireSmoothing}


\begin{minipage}[t]{0.3\textwidth}
  \def\svgwidth{\textwidth}
  \captionsetup{type=figure}
  \centering\raisebox{\dimexpr \topskip-\height}{\input{img/ForestFire1.pdf_tex}}
  \captionof{figure}{Forest Fire Smoothing: concept}
  \label{fig:forestfire1}
\end{minipage}\hfill
\begin{minipage}[t]{0.7\textwidth}
\vspace{-25pt}
\begin{algorithm}[H]
\caption{Forest Fire Smoothing}\label{alg:forestfire}
\begin{algorithmic}[1]
\Function{ForestFire}{$v^\mathcal{M}_a$}
\State $v^\mathcal{B}_b = \Phi(v^\mathcal{M}_a)$
\State $M^\mathcal{B}:=\textsc{MinHeap}\langle\text{vertex},\text{dist},\text{distcmp}\rangle$
\For {$v^\mathcal{B}_i\in V^\mathcal{B}$}
\State $\textsc{Dis}(v^\mathcal{B}_i):=\infty$
\EndFor
\For{$v^\mathcal{B}_p \in P^\mathcal{B}(\Phi(v^\mathcal{M}_a))$} 
\State $\textsc{Dis}(v^\mathcal{B}_p):=0$
\State $\textsc{Push}(M^\mathcal{B}, v^\mathcal{B}_p, \textsc{Dis}(v^\mathcal{B}_p))$
\EndFor
\While{$\neg \textsc{Empty}(M)$}
\State $v^\mathcal{B}_b:= \textsc{First}(M^\mathcal{B})$
\State $N^\mathcal{B} := \textsc{Neighbors}(v^\mathcal{B}_b)$
\For {$v^\mathcal{B}_n \in N^\mathcal{B}$}
\If {$\textsc{Dis}(v^\mathcal{B}_n)>(\textsc{Dis}(v^\mathcal{B}_b)+|\textsc{E}(v^\mathcal{B}_b,v^\mathcal{B}_n)|)$}
\State $\textsc{Dis}(v^\mathcal{B}_n):=\textsc{Dis}(v^\mathcal{B}_b)+|\textsc{E}(v^\mathcal{B}_b,v^\mathcal{B}_n)|$
\State $\textsc{Push}(M^\mathcal{B},v^\mathcal{B}_n,\textsc{Dis}(v^\mathcal{B}_n))$
\EndIf
\EndFor
\State $\textsc{Pop}(M^\mathcal{B})$
\EndWhile
\State\Return $v^\mathcal{B}_b$
\EndFunction
\end{algorithmic}
\end{algorithm}
\vspace{5pt}
\end{minipage}

The first smoothing method we implemented is Forest Fire Smoothing. Figure \ref{fig:forestfire1} visualizes how the method works. To start, the border of the patch around the vertex $v^{\mathcal{M}}_A$ that is to be smoothed is selected. This border is then uniformly pushed inwardly until it converges on one point. That point is then identified as the center of the patch, and vertex $v^{\mathcal{M}}_A$ is relocated there.

\begin{wrapfigure}[21]{l}{0.4\textwidth}
  \vspace{-18pt}
   \def\svgwidth{0.34\textwidth}
   {\centering
   \input{img/ForestFire2.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption[width=.7\linewidth]{Forest Fire Smoothing: worst case}
  \label{fig:forestfire2}
\end{wrapfigure}

Algorithm \ref{alg:forestfire} shows how Forest Fire Smoothing works in pseudo code. The input meta vertex $v^{\mathcal{M}}_a$ gets relocated to the position of the output base vertex $v^{\mathcal{B}}_b$ afterwards. The functions used in Algorithm \ref{alg:forestfire} are \textsc{MinHeap} to build a minheap, \textsc{Dis} to return the distance of a vertex from the border, \textsc{Push} to push an element into the minheap, \textsc{Pop} to remove the top element of the minheap, \textsc{First} to return the first element of the minheap, \textsc{Empty} to tell if the minheap is empty and \textsc{Neighbors} to return the set of neighbors of a vertex.

Since the meta mesh vertices can only be relocated onto base vertices $v^\mathcal{B}_i\in V^\mathcal{B}$, the patch border is pushed forward step by step across base vertices $v^\mathcal{B}_i$. This is done by building a minheap sorted by distance from the border, that initially contains all border vertices in the patch. Then, in every step, check the neighboring vertices of the top element in the heap. If the path from the current top element to the new element is the fastest, put it on top of the heap. When only one element remains in the heap, this is the base vertex furthest away from the patch border, and the meta vertex is relocated there.

The Forest Fire Smoothing method certainly improves vertex positions on average, however there are cases where it deteriorates them as well. Figure \ref{fig:forestfire2} showcases such an example. Specifically, when the patch around a vertex has a very irregular shape with partially concave curvature, the narrowing patch can converge towards several different areas but will then ultimately choose the point which is furthest from the border. But  in the example given in Figure \ref{fig:forestfire2} the initial position of the Vertex $v^{\mathcal{M}}_A$ was certainly better located than the final position $v^{\mathcal{M}}_B$.

Given the often irregular shape of meta mesh edges and patches, it makes sense to look at alternative smoothing methods.
