\subsection{Vertex Weight Smoothing}
\label{subsec:VertexWeightSmoothing}

Seeing how Forest Fire Smoothing potentially relocates a vertex to a local sub-area of the patch that is the thickest, the next approach is rating possible positions by the minimal squared distance from the patches vertices.  This is an approach also known as Karcher Mean or Fréchet Mean \cite{panozzo2013weighted, sharp2019vector}. In order to smooth an embedded meta vertex $\Phi\left(v^\mathcal{M}_a\right)$ we are attempting to find the base vertex $v^\mathcal{B}_{\min}$ which minimizes the weight $w(v^\mathcal{B}_b)$ in relation to the embedded meta vertices $\Phi\left(v^\mathcal{M}_p\right)$ on the border $b^\mathcal{M}_P$ of the patch $P^\mathcal{M}\left(v^\mathcal{M}_a\right)$ surrounding the meta vertex $v^\mathcal{M}_a$. We minimize the weight

\begin{equation}\label{eq:vertexweight}
w\left(v^\mathcal{B}_b\right)=\sum_{v^{\mathcal{M}}_p\in{b}^\mathcal{M}_P}\textsc{Dist}\left(v^{\mathcal{M}}_b, \Phi\left(v^{\mathcal{B}}_p\right)\right)^2 \rightarrow \min\left(w\right)
\end{equation}

and then relocate $\Phi\left(v^\mathcal{M}_a\right)$ to $v^\mathcal{B}_{\min}$:

\begin{minipage}[t]{0.25\textwidth}
  \def\svgwidth{\textwidth}
  \captionsetup{type=figure}
  \centering\raisebox{\dimexpr \topskip-\height}{\input{img/VertexWeights1.pdf_tex}}
  \captionof{figure}{Vertex Weight Smoothing: concept}
  \label{fig:vertexweights1}
\end{minipage}\hfill
\begin{minipage}[t]{0.65\textwidth}
\vspace{-20pt}
\begin{algorithm}[H]
\caption{Vertex Weight Smoothing}\label{alg:vertexweights}
\begin{algorithmic}[1]
\Function{VertexWeights}{$v^\mathcal{M}_a$}
\State $v^\mathcal{B}_{\min} = \Phi(v^\mathcal{M}_a)$
\State $Q^\mathcal{B}=\textsc{Queue}\langle\text{vertex}\rangle$
\For {$v^\mathcal{B}_i\in V^\mathcal{B}$}
\State $\textsc{Dis}(v^\mathcal{B}_i):=\{\infty,\infty,\dots,\infty\}$
\EndFor
\For{$v^\mathcal{M}_r \in \textsc{VVRange}(v^\mathcal{M}_a)$} 
\State $\textsc{Push}(Q^\mathcal{B}, \Phi(v^\mathcal{M}_r))$
\For{$v^\mathcal{B}_q \in Q^\mathcal{B}$} 
\State $N^\mathcal{B} := \textsc{Neighbors}(v^\mathcal{B}_q)$
\For {$v^\mathcal{B}_n \in N^\mathcal{B}$}
\If {$\textsc{Dis}(v^\mathcal{B}_n, \Phi(v^\mathcal{M}_r))>(\textsc{Dis}(v^\mathcal{B}_q, \Phi(v^\mathcal{M}_r))+|e^\mathcal{B}_{q,n}|)$}
\State $\textsc{Dis}(v^\mathcal{B}_n, \Phi(v^\mathcal{M}_r)) = (\textsc{Dis}(v^\mathcal{B}_q, \Phi(v^\mathcal{M}_r))+|e^\mathcal{B}_{q,n}|)$
\State $\textsc{Push}(Q^\mathcal{B}, v^\mathcal{B}_n)$
\EndIf
\EndFor
\EndFor
\EndFor
\For {$v^\mathcal{B}_i\in V^\mathcal{B}$}
\If{$\textsc{Score}(v^\mathcal{B}_{\min})>\textsc{Score}(v^\mathcal{B}_i)$}
\State $v^\mathcal{B}_{\min}:=v^\mathcal{B}_i$
\EndIf
\EndFor
\State\Return $v^\mathcal{B}_{\min}$
\EndFunction
\end{algorithmic}
\end{algorithm}
\end{minipage}
\vspace{10pt}

Figure \ref{fig:vertexweights1} and Algorithm \ref{alg:vertexweights} show the process of Vertex Weight Smoothing. Simply put, for an input meta vertex $v^{\mathcal{M}}_a$ the area inside the patch surrounding $v^{\mathcal{M}}_a$ is iterated over for each meta vertex on the patch, and weights are given for the distance to each of those vertices. The output $v^{\mathcal{M}}_b$ is the base vertex inside patch where the sum of the squares of the distances is minimal. The embedded meta vertex $\Phi\left(v^{\mathcal{M}}_a\right)$ can then be relocated to the position of base vertex $v^{\mathcal{M}}_b$.

\begin{wrapfigure}[18]{l}{0.25\textwidth}
  \vspace{-1pt}
   \def\svgwidth{0.25\textwidth}
   {\centering
   \input{img/VertexWeights2.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Vertex Weight Smoothing: bad case.}
  \label{fig:vertexweights2}
\end{wrapfigure}

Algorithm \ref{alg:vertexweights} uses a variety of functions. $\textsc{Dis}\left(v^\mathcal{B}_i\right)$ handles an array of distances from each vertex on the patch around $v^\mathcal{M}_a$. $\textsc{VVRange}\left(v^\mathcal{M}_a\right)$ returns the 1-ring of vertices around $v^\mathcal{M}_a$. $\textsc{Neighbors}\left(v^\mathcal{B}_q\right)$ returns the 1-ring of base vertex $v^\mathcal{B}_q$ \textit{without} vertices lying on restrictions. And lastly and most importantly, $\textsc{Score}\left(v^\mathcal{B}_{x}\right)$ calculates the score of $v^\mathcal{B}_{x}$ as per Equation \ref{eq:vertexweight}.

Vertex Weight Smoothing has shortcomings as well though. Vertices around a patch are not necessarily distributed uniformly, and as such, a vertex may be relocated near the border of a patch, rather than towards its center. Over several iterations of the algorithm, this can cause some meta meshes to shrink and cover less and less area on the base mesh with each iteration, until convergence into one singular ever shrinking triangle. Figure \ref{fig:vertexweights2} shows such a patch where this effect occurs.

One idea to avoid this problem would be to check for distances from every base vertex instead of every meta vertex around the patch, however this opens up a different set of problems. If the underlying base mesh is not uniform the same problem may still arise, but more importantly the computational cost would multiply. Thus this is not a viable improvement either. 

Yet another option would be to change the way weights are calculated in Equation \ref{eq:vertexweight}, e.g. by using a weighted sum. But this introduces additional parameters (the new weights) that need to be optimized, and thus makes things more complicated.

Instead we slighty change the way Vertex Weight Smoothing way works to approach this problem.
