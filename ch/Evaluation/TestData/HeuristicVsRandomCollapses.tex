\subsection{Heuristic vs. Random Collapses}
\label{subsec:HeuristicVsRandomCollapses}

The tests in this section were performed by running 100 Iterations of Embedded Isotropic Remeshing with a target edge length\footnote{We represent target edge length in relation to the diagonal of the bounding box of $\mathcal{B}$.} of 0.2 on the ''fertility'' mesh with 10000 vertices and the \textit{VertexDistance} smoothing type. The intial embedding is complete, meaning $\Phi\left(\mathcal{M}\right)=\mathcal{B}$, and gets iteratively refined.

The only difference in these test runs is the way in which the collapse order was decided, specifically:
\begin{itemize}
\item \textbf{Random Collapses}: We shuffled the array of edges before each iteration of the algorithm.
\item \textbf{Heuristic 1}: We used the heuristic presented in Section \ref{sec:CollapseOrder} in Equation \ref{eq:CollapseHeuristic}:
\begin{equation}\nonumber
\textsc{ch}(h^{\mathcal{M}}_x) := (|v^{\mathcal{M}}_A|-4)\cdot w_{\mathcal{M}}(h^{\mathcal{M}}_x) - w_{\mathcal{M}}(h^{\mathcal{M}}_p) - w_{\mathcal{M}}(h^{\mathcal{M}}_{on})
\end{equation}
\item \textbf{Heuristic 2}: We slightly modified the above heuristic by adding a factor of $\frac{1}{2}$ before the first term:
\begin{equation}\nonumber
\textsc{ch}(h^{\mathcal{M}}_x) := \frac{1}{2}(|v^{\mathcal{M}}_A|-4)\cdot w_{\mathcal{M}}(h^{\mathcal{M}}_x) - w_{\mathcal{M}}(h^{\mathcal{M}}_p) - w_{\mathcal{M}}(h^{\mathcal{M}}_{on})
\end{equation}
\end{itemize}

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/CollapseOrder.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{(a) Initial state (b) Sample final state after 100 iterations.}
  \label{fig:CollapseOrder}
\end{figure}

Figure \ref{fig:CollapseOrder}.(a) shows the initial state all tests in this section started from, whereas Figure \ref{fig:CollapseOrder}.(b) shows a sample final state with a highly refined embedding $\Phi\left(\mathcal{M}\right)$ on $\mathcal{B}$. Final states vary on a run-to-run basis due to the randomness in the algorithm e.g. when shuffling edges before collapses.

In the following we first have a look at the data produced by 10 runs with random collapses.

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/Random-iteration-time_ms.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Time / iteration.}
  \label{fig:CORandomTime}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/Random-iteration-basefaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$|F^\mathcal{B}|$ / iteration.}
  \label{fig:CORandomBaseFaces}
\end{minipage}

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/Random-iteration-metafaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$|F^\mathcal{M}|$ / iteration.}
  \label{fig:CORandomMetaFaces}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/Random-iteration-basefaces_metafaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$|F^\mathcal{B}|/|F^\mathcal{M}|$ / iteration.}
  \label{fig:CORandomBaseMetaFaces}
\end{minipage}

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/Random-iteration-edgelength_avg.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Edge length / Iteration.}
  \label{fig:CORandomEdgelengthAvg}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/Random-iteration-edgelength_sd.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Edge length SD / Iteration.}
  \label{fig:CORandomEdgeLengthSd}
\end{minipage}

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/Random-iteration-valence_avg.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Valence / Iteration.}
  \label{fig:CORandomValenceAvg}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/Random-iteration-valence_sd.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Valence SD / Iteration.}
  \label{fig:CORandomValenceSd}
\end{minipage}

The previous page shows all the data we collect for each random run of our Embedded Isotropic Remeshing algorithm. Some of these random runs started taking too long and had to be terminated before 100 iterations could be completed. This can be seen in the tables, and is especially pronounced when looking at the elapsed time and number of base faces per iteration. The general pattern is an initial increase in complexity, as the base mesh $\mathcal{B}$ is split up, gaining more faces, followed by a steady decrease as the meta mesh is decimated,
and superfluous non-original base edges are collapsed.

However, stability is not guaranteed. When a mesh with an initial 10,000 faces explodes to over 1,000,000 faces, and time increases accordingly, this showcases problems in the underlying algorithm. Random collapses can work and terminate properly, but the worst case creates too many splits on $\mathcal{B}$ through spiralization and similar effects as shown in Section \ref{sec:ImplementationDetails}.

Excluding the worst cases, the ratio between the base faces and meta faces $\frac{|F^\mathcal{B}|}{|F^\mathcal{M}|}$ slightly oscillates after ~20 iterations, as does the edge length. There is no convergence due to two reasons:

\begin{itemize}
\item The tolerance bounds $\alpha$ and $\beta$ may be set too loosely; setting them wider stops a lot of collapses and splits from happening. Tests regarding that can be found in Section \ref{subsec:SlackVariables}.
\item Our algorithm for flips optimizes variance \textit{locally}, which means it may need many iterations to converge onto a minimum. Implementing a \textit{global} flipping algorithm should decrease oscillation but at a higher computational cost.
\end{itemize}

  \vspace{-15pt}
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/COAverages-iteration-time_ms.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Time / Iteration.}
  \label{fig:COAveragesTime}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/COAverages-iteration-basefaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$|F^\mathcal{B}|$ / Iteration.}
  \label{fig:COAveragesBaseFaces}
\end{minipage}
\vspace{10pt}

As randomness and especially outliers introduce a lot of noise into our data, we average it over the 10 runs shown on the left, and compare it to averaged data of several runs over Heuristic 1  introduced in Equation \ref{eq:CollapseHeuristic} which guides collapsing order. In order to get an additional dataset to compare to, we modify our Heuristic 1 slightly, call the modified version Heuristic 2, and compare that as well. Obviously, our Heuristic 1 is not perfect, and would require a lot of testing and fine-tuning to perform optimally. The modified Heuristic 2 serves as a lower bound; whereas our heuristic 1 beats random collapses, the modified Heuristic 2 performs worse than random collapses.

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/COAverages-iteration-edgelength_avg.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Edge length / Iteration.}
  \label{fig:COAveragesEdgelengthAvg}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/COAverages-iteration-edgelength_sd.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Edge length SD / Iteration.}
  \label{fig:COAveragesEdgelengthSd}
\end{minipage}
\vspace{10pt}

As the spikes in Figures \ref{fig:COAveragesTime} and \ref{fig:COAveragesBaseFaces} indicate runs that did not terminate, it can be seen that our Heuristic 1 is more stable than collapsing randomly\footnote{The spikes are produced by runs that had to be stopped, so their height is arbitrary and depends on the last iteration that finished. What matters is the number of spikes, not their height.}, but still suffers from some problems as well. Even slight modifications, such as the one resulting in Heuristic 2, can drastically alter the behavior for the worse. Since scalability and stability on large meshes are important qualities, it would be worth it to spend more time on improving the collapse order. Alternatively or additionally, changing the way in which the underlying base mesh $\mathcal{B}$ is split could improve performance a lot, by reducing the number of base faces $|F^\mathcal{B}|$ and thus also the time spent per iteration.

Overall, the main advantage of using a heuristic can be clearly seen in Figures \ref{fig:COAveragesTime}-\ref{fig:COAveragesEdgelengthSd} -- that advantage is robustness. An average random run may perform on par with a run using our Heuristic 1, but a worst case is more likely. Seeing that the examples converged after around 20 iterations, yet many errors still occured later, quitting in time would also improve robustness.

Due to time constraints, the remaining tests were performed on meshes with ~1,000 vertices, as compared to the ~10,000 vertices fertility mesh of the test cases in this section. This was necessary, since our current implementation does not scale linearly with mesh size\footnote{In our implementation a few global operations remain, such as garbage collection. While base mesh pre- and post-processing have been mostly localized, we still sometimes call methods that iterate over the entire mesh and thus scale poorly. This could probably be improved, facilitating better scalability.}, and single runs took a few days on the fertility mesh -- especially the outliers.
