\subsection{Limiting Flips}
\label{subsec:LimitingFlips}

One parameter of our Embedded Isotropic Remeshing algorithm is whether edge flips should be limited or not. Limiting flips means forbidding any edge flip on an embedded edge $\Phi\left(e^\mathcal{M}_x\right)$ that would result in an increase in the length of the flipped edge $\Phi\left(e^\mathcal{M}_{x'}\right)$, compared to its length before flipping: $|\Phi\left(e^\mathcal{M}_{x'}\right)|>|\Phi\left(e^\mathcal{M}_x\right)|$. The idea is to reduce spiralization this way. Since spiraled edges are usually much longer than the average edge length, flipping edges into spirals would be forbidden by this rule.

\begin{figure}[ht]
   \vspace{-20pt}
   \def\svgwidth{0.9\textwidth}
   {\centering
   \input{img/LimitingFlips.pdf_tex}\par
   }
   \vspace{-1pt}
   \vspace{-5pt}
  \caption{(a) Initial state (b) Sample final state after 100 iterations.}
  \label{fig:LimitingFlips}
   \vspace{-10pt}
\end{figure}

We performed 20 test runs for each setting, over 100 iterations of embedded isotropic remeshing each, with $T=0.2$, on a torus of 1000 vertices. Those 20 runs each were then averaged, to compare metrics while minimizing noise. Figure \ref{fig:LimitingFlips} exemplifies one of these test runs. They were performed on a torus with the starting configuration $\Phi\left(\mathcal{M}\right)=\mathcal{B}$ (just like our other test runs). Figure \ref{fig:LimitingFlips}.(b) shows a sample completed run, and again there was variation in the final outputs.

%The following tables compare our different metrics for the averaged runs.

  \vspace{5pt}
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/LFAverages-iteration-time_ms.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Time / Iteration.}
  \label{fig:LFAveragesTime}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/LFAverages-iteration-basefaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$|F^\mathcal{B}|$ / Iteration.}
  \label{fig:LFAveragesBaseFaces}
\end{minipage}
  
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/LFAverages-iteration-metafaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$|F^\mathcal{M}|$ / Iteration.}
  \label{fig:LFAveragesMetaFaces}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/LFAverages-iteration-basefaces_metafaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$(|F^\mathcal{B}|/|F^\mathcal{M}|)$ / Iteration.}
  \label{fig:LFAveragesBaseMetaFaces}
\end{minipage}

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/LFAverages-iteration-edgelength_avg.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Edge length / Iteration.}
  \label{fig:LFAveragesEdgelengthAvg}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/LFAverages-iteration-edgelength_sd.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Edge length SD / Iteration.}
  \label{fig:LFAveragesEdgeLengthSd}
\end{minipage}

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/LFAverages-iteration-valence_avg.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Valence / Iteration.}
  \label{fig:LFAveragesValenceAvg}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/LFAverages-iteration-valence_sd.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Valence SD / Iteration.}
  \label{fig:LFAveragesValenceSd}
\vspace{7pt}
\end{minipage}

In Figures \ref{fig:LFAveragesBaseFaces}-\ref{fig:LFAveragesValenceAvg} there is barely any difference between allowing and limiting flips. The only discenable differences in those figures are some slight spikes in the number of base faces and time per iteration when flips were limited, 

In contrast, Figure \ref{fig:LFAveragesValenceSd} shows a high difference in the standard deviation of the valence from the average between runs with limited vs unlimited flips. The difference in standard deviation from the average valence is easily explained: Flips are only performed when they would improve valence, but limiting flips forbids some of these flips. Thus, valence is sometimes less optimized locally, and this compounds towards a higher standard deviation, compared to allowing all local improvements.\footnote{Note that the average valence stayed at exactly 6 throughout these runs, which is possible since the torus these runs were ran on was a very regular small mesh (1000 vertices) of genus 1, as opposed to the genus 0 mesh used in the previous section where the average valence cannot reach 6. Similarly, the fertility mesh in Section \ref{subsec:HeuristicVsRandomCollapses} is of genus 4, facilitating an increase of the average way over 6 as the number of meta vertices $V^\mathcal{M}$ decreases.}

Since limiting flips gives no measurable benefits, while increasing standard deviation from the average valence, it is not advisable to do so. Although perhaps in combination with a \textit{random} collapse order, limiting flips would improve the performance, it is preferable to use heuristics to determine collapse order, as was shown in Section \ref{subsec:HeuristicVsRandomCollapses}.
