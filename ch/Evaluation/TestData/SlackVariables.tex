\subsection{Tolerance Bounds}
\label{subsec:SlackVariables}

During the \textsc{Collapses} and \textsc{Splits} steps of our Embedded Isotropic Remeshing algorithm, embedded meta edges $\Phi\left(e^\mathcal{M}_i\right)\in \Phi\left(E^\mathcal{M}\right)$ are split if their length exceeds $\alpha\cdot T$, or collapsed if their length subceeds $\beta\cdot T$, where $T$ is the target edge length. It is clear that $\alpha\geq 1$ and $\beta\leq 1$, and for traditional meshes the optimal values are $\alpha=\frac{4}{3}$ and $\beta=\frac{4}{5}$ \cite{CGII15}.

Since embedded edges $\Phi\left(e^\mathcal{M}_i\right)$ can be curved, it makes sense to try out greater tolerance intervals than $\{\frac{4}{5}\cdotT,\frac{4}{3}\cdotT\}$. We ran tests for three configurations
\begin{itemize}
\item $\alpha=\frac{4}{3}, \beta=\frac{4}{5}$
\item $\alpha=\frac{3}{2}, \beta=\frac{2}{3}$
\item $\alpha=2, \beta=\frac{1}{2}$
\end{itemize}

\begin{figure}[ht]
   \vspace{-10pt}
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/ToleranceBounds.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{(a) Initial state (b) Sample final state after 100 iterations.}
  \label{fig:ToleranceBounds}
\end{figure}

Figure \ref{fig:ToleranceBounds} shows the initial configuration of these runs as well as a sample final state after 100 iterations. For each of the different configurations we executed 20 runs over 100 iterations each on a base mesh $\mathcal{B}$ in the form of a sphere with 1000 vertices. The 20 corresponding runs are each averaged to reduce noise, and then visualized.

The following data compares those averages:

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/ABAverages-iteration-time_ms.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Time / Iteration.}
  \label{fig:ABAveragesTime}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/ABAverages-iteration-basefaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$|F^\mathcal{B}|$ / Iteration.}
  \label{fig:ABAveragesBaseFaces}
\end{minipage}
  
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/ABAverages-iteration-metafaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$|F^\mathcal{M}|$ / Iteration.}
  \label{fig:ABAveragesMetaFaces}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/ABAverages-iteration-basefaces_metafaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$(|F^\mathcal{B}|/|F^\mathcal{M}|)$ / Iteration.}
  \label{fig:ABAveragesBaseMetaFaces}
\end{minipage}

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/ABAverages-iteration-edgelength_avg.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Edge length / Iteration.}
  \label{fig:ABAveragesEdgelengthAvg}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/ABAverages-iteration-edgelength_sd.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Edge length SD / Iteration.}
  \label{fig:ABAveragesEdgeLengthSd}
\end{minipage}

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/ABAverages-iteration-valence_avg.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Valence / Iteration.}
  \label{fig:ABAveragesValenceAvg}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/ABAverages-iteration-valence_sd.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Valence SD / Iteration.}
  \label{fig:ABAveragesValenceSd}
\vspace{7pt}
\end{minipage}

Figures \ref{fig:ABAveragesTime}-\ref{fig:ABAveragesValenceSd} contrast the performance of our three settings for $\alpha$ and $\beta$. In these test runs with 1000 base vertices $V^\mathcal{B}$ there were no runs that had to be terminated, as the overhead caused by global methods is much lower. Thus, outliers where base mesh size grows are quickly overcome.

The first point of note is that in Figure \ref{fig:ABAveragesTime} the default run $\alpha=1.33, \beta=0.8$ took longer than the other two runs, as expected, but $\alpha=1.5, \beta=0.66$ outperformed $\alpha=2, \beta=0.5$. The reason $\alpha=1.33, \beta=0.8$ performs the worst is apparently that the overly restrictive tolerance bounds cannot account for curvature in embedded meta edges $\Phi\left(e^\mathcal{M}_i\right)\in \Phi\left(E^\mathcal{M}\right)$. $\alpha=1.5, \beta=0.66$ outperforming the more loose $\alpha=2, \beta=0.5$ could be interpreted as $\alpha=2, \beta=0.5$ giving \textit{too much tolerance}.

However, when looking at the other datapoints, it becomes apparent that out of our options $\alpha=2, \beta=0.5$ is clearly the most preferable. Isotropic Remeshing aims to optimize average edge length and valence to create a maximally regular mesh, and in these categories $\alpha=2, \beta=0.5$ is the clear winner. Figures \ref{fig:ABAveragesEdgelengthAvg}-\ref{fig:ABAveragesValenceSd} show much less oscillation and also much lower standard deviations for the average edge length and valence metrics when using $\alpha=2, \beta=0.5$. These improvements certainly warrant the slightly higher time per iteration compared to $\alpha=1.5, \beta=0.66$.

Note that these experiments were performed on a sphere to reduce the effects of uneven surfaces on edge lengths, but $\alpha$ and $\beta$ could certainly be adjusted to better fit other shapes of meshes as well.
