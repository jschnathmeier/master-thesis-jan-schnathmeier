\subsection{Smoothing Types}
\label{subsec:SmoothingTypes}

Lastly, we look at the different types of smoothing, as presented in Section \ref{sec:Smoothing}. We compare Vertex Weight Smoothing\footnote{See Section \ref{subsec:VertexWeightSmoothing}, Algorithm \ref{alg:vertexweights}.} and Vertex Distance Smoothing\footnote{See Section \ref{subsec:VertexDistanceSmoothing} -- the same algorithm, but weights from Equation \ref{eq:vertexdistance}.}.

\begin{figure}[ht]
   \def\svgwidth{0.9\textwidth}
   {\centering
   \input{img/ForestSlip.pdf_tex}\par
   }
  \caption{Embedding $\Phi(\mathcal{M})$ \textit{slipping} off $\mathcal{B}$ and vanishing in 19 iterations.}
  \label{fig:ForestSlip}
\end{figure}

We skip Forest Fire Smoothing. Forest Fire Smoothing on meshes of genus 0 causes the embedding to \textit{slip} off the surface of $\mathcal{B}$ and converge onto a single triangle; essentially vanishing. Figure \ref{fig:ForestSlip} illustrates this process, by showing different iterations of a run that vanished in 19 iterations. Due to this problem, our other smoothing methods are much more optimized, and rewriting Forest Fire Smoothing would not be worth it, given the time constraints and the two other available and functional smoothing methods. Data of runs of a much less optimized algorithm where the embedding quickly vanishes would also be hard to compare to runs that do not vanish over 100 iterations.

As we only performed tests using Vertex Weight Smoothing and Vertex Distance Smoothing, Figure \ref{fig:SmoothingTypes} shows the starting configuration and the configuration after 100 iterations of a random run. Note that none of the runs vanished like Forest Fire Smoothing does. Again, we compare averages over 20 runs of 100 iterations each.

\vspace{-10pt}
\begin{figure}[ht]
   \def\svgwidth{0.9\textwidth}
   {\centering
   \input{img/SmoothingTypes.pdf_tex}\par
   }
  \caption{(a) Initial state (b) Sample final state after 100 iterations.}
  \label{fig:SmoothingTypes}
\end{figure}

%With that out of the way, we again look at 20 averaged runs each for our smoothing types:

\vspace{-10pt}
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/STAverages-iteration-time_ms.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Time / Iteration.}
  \label{fig:STAveragesTime}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/STAverages-iteration-basefaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$|F^\mathcal{B}|$ / Iteration.}
  \label{fig:STAveragesBaseFaces}
\end{minipage}
  
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/STAverages-iteration-metafaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$|F^\mathcal{M}|$ / Iteration.}
  \label{fig:STAveragesMetaFaces}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \captionsetup{type=figure}
  \includegraphics[width=0.95\textwidth]{img/STAverages-iteration-basefaces_metafaces.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{$(|F^\mathcal{B}|/|F^\mathcal{M}|)$ / Iteration.}
  \label{fig:STAveragesBaseMetaFaces}
\end{minipage}

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/STAverages-iteration-edgelength_avg.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Edge length / Iteration.}
  \label{fig:STAveragesEdgelengthAvg}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/STAverages-iteration-edgelength_sd.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Edge length SD / Iteration.}
  \label{fig:STAveragesEdgeLengthSd}
\end{minipage}

\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/STAverages-iteration-valence_avg.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Valence / Iteration.}
  \label{fig:STAveragesValenceAvg}
\end{minipage}\hfill
\begin{minipage}[t]{0.5\textwidth}
  \begin{centering}
  \includegraphics[width=0.95\textwidth]{img/STAverages-iteration-valence_sd.pdf}
  \end{centering}
  \vspace{-7pt}
  \captionof{figure}{Valence SD / Iteration.}
  \label{fig:STAveragesValenceSd}
\vspace{7pt}
\end{minipage}

With a single glance at Figures \ref{fig:STAveragesTime}-\ref{fig:STAveragesValenceSd} it is apparent that Vertex Weight Smoothing outperforms Vertex Distance Smoothing in every single metric. The clear recommendation is to use Vertex Weight Smoothing as the default smoothing type.
