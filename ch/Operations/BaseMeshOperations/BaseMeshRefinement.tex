\subsection{Base Mesh Refinement}
\label{subsec:BaseMeshRefinement}

The previous Section \ref{subsec:RestrictedPathTracing} introduced restricted path tracing -- bidirectional A* tracing restricted to patches delimited by embedded meta edges $\Phi\left(e^\mathcal{M}_i\right)$. We define a set of edge restrictions $R^\mathcal{B}_E\subseteq E^\mathcal{B}$. Furthermore we define a set of vertex restrictions $R^\mathcal{B}_V\subseteq V^\mathcal{B}$, consisting of the vertices incident to edges of $R^\mathcal{B}_E$. Traces are not allowed to pass over edges of $R^\mathcal{B}_E$. 

Formally, these sets of restrictions $R^\mathcal{B}_E$ and $R^\mathcal{B}_V$ are defined as:

\vspace{-10pt}
\begin{align}
 R^\mathcal{B}_E := \left\{e^{\mathcal{B}}_{i}\in E^\mathcal{B} \Bigg| e^{\mathcal{B}}_{i}\in\bigcup_{\Phi\left(e^\mathcal{M}_j\right) \in \Phi\left(E^\mathcal{M}\right)} \Phi\left(e^\mathcal{M}_j\right)\right\} \nonumber \\
  R^\mathcal{B}_V := \left\{v^{\mathcal{B}}_{i}\in V^\mathcal{B} \Big| \exists e^{\mathcal{B}}_{j}\in R^\mathcal{B}_E \quad v^{\mathcal{B}}_{i}\text{ is incident to }e^{\mathcal{B}}_{j}.\right\} \nonumber
\end{align}

While the restrictions $R^\mathcal{B}_E$ and $R^\mathcal{B}_V$ are useful in order to restrain our trace method into simple patches, and thus not breaking homotopy, the restrictions can bring other problems with them. It can happen that there is no consecutive path of edges $\left\{e^{\mathcal{B}}_{A,B},e^{\mathcal{B}}_{B,C},\dots, e^{\mathcal{B}}_{N,M}\right\}$ to represent an embedded edge $\Phi\left(e^\mathcal{M}_{A,M}\right)$. In such a case our tracing method fails to embed the edge $\Phi\left(e^\mathcal{M}_{A,M}\right)$, so naturally this should be avoided. 

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/PreProcessing.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Base mesh refinement: edges $e^{\mathcal{B}}_r\in R^\mathcal{B}_E$ in red, vertices $v^{\mathcal{B}}_r\in R^\mathcal{B}_V$ in red, edges $e^{\mathcal{B}}_i$ that need to be split in teal. (a) pre-refinement (b) post-refinement}
  \label{fig:PreProcessing}
\end{figure}

Figure \ref{fig:PreProcessing}.(a) shows an embedded face $\Phi\left(f^\mathcal{M}_x\right)\in\Phi\left(F^\mathcal{M}\right)$ in a problematic configuration. The restricted elements of the face $\Phi\left(f^\mathcal{M}_x\right)$ are marked in red, and two problematic edges are marked in teal. The problem with those teal edges is that both of their incident vertices are in $R^\mathcal{B}_V$, thus creating a blockade. Notice that connecting $v^\mathcal{B}_A$ and $v^\mathcal{B}_B$ or $v^\mathcal{B}_B$ and $v^\mathcal{B}_C$ is impossible due to the state of those teal edges. Thus, we refine $\mathcal{B}$ by splitting every edge $v^\mathcal{B}_{X,Y}$, for which both incident vertices $v^\mathcal{B}_X$ and $v^\mathcal{B}_Y$ are restricted, $v^\mathcal{B}_X, v^\mathcal{B}_Y\in R^\mathcal{B}_V$. 

In the resulting mesh, edges between every pair of base vertices $v^\mathcal{B}_i, v^\mathcal{B}_j$ can be traced with respect to the edge restrictions $R^\mathcal{B}_E$. This can be seen in Figure \ref{fig:PreProcessing}.(b) where both teal edges from (a) have been split, resulting in two new vertices and full connectivity inside the restricted area. Since every edge $e^\mathcal{B}_{i}$ which requires splitting is adjacent to an embedded edge $\Phi\left(e^\mathcal{M}_{j}\right)$, we can refine parts of $\mathcal{B}$ locally instead of having to iterate over all edges in $E^\mathcal{B}$. Before we trace an embedded meta edge $\Phi\left(e^\mathcal{M}_x\right)$, we apply the following refinement algorithm:

\begin{algorithm}[H]
\caption{Patch pre-processing}\label{alg:PreProcessPatch}
\begin{algorithmic}[1]
\Function{PreProcessPatch}{$\Phi(e^\mathcal{M}_x)$}
\For {$\Phi(h^\mathcal{M}_i)\in \textsc{SurroundingPatchBoundary}(\Phi(e^\mathcal{M}_x))$}\label{alg:PreProcessPatchLoop1}
\For {$h^{\mathcal{B}}_j\in \Phi(h^\mathcal{M}_i)$}\label{alg:PreProcessPatchLoop2}
\For {$h^{\mathcal{B}}_{X,Y}\in \textsc{LeftHalfCircle}(\Phi(h^\mathcal{M}_i))$}\label{alg:PreProcessPatchLoop3}
\If {$v^\mathcal{B}_Y\in R^\mathcal{B}_V$}
\State $\textsc{Split}(h^{\mathcal{B}}_{X,Y})$
\EndIf
\EndFor
\EndFor
\EndFor
\EndFunction
\end{algorithmic}
\end{algorithm}

Line \ref{alg:PreProcessPatchLoop1} of Algorithm \ref{alg:PreProcessPatch} loops over the halfedges $\Phi\left(h^\mathcal{M}_i\right)$ of the \textsc{SurroundingPatchBoundary}\footnote{As defined previously, the surrounding patch $P^\mathcal{B}(\Phi\left(e^\mathcal{M}_i\right))$ of an edge $\Phi\left(e^\mathcal{M}_i\right)$ consists of the two incident faces of $\Phi\left(e^\mathcal{M}_i\right)$. The border of this patch consists of the inner sequence of embedded meta halfedges surrounding it.} of $\Phi\left(e^\mathcal{M}_x\right)$. The function \textsc{LeftHalfCircle}$(h^{\mathcal{B}}_{X,Y})$ returns all halfedges not in $R^\mathcal{B}_E$ traversed by circulating outgoing halfedges of $v^\mathcal{B}_X$ until a halfedge in $R^\mathcal{B}_E$ is reached. Thus, the only base edges that are considered for a split are exactly the base edges $b^\mathcal{B}_i$ inside the patch $P^\mathcal{B}(\Phi\left(e^\mathcal{M}_i\right))$ surrounding $\Phi\left(e^\mathcal{M}_x\right)$ which touch the patch border.

\begin{wrapfigure}[24]{r}{0.50\textwidth}
    \vspace{-20pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/CollapseStressTest.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Excessively refined mesh $\mathcal{B}$ after stress test vs original state $\mathcal{B}'$ side by side.}
  \label{fig:CollapseStressTest}
\end{wrapfigure}

With that, pre-processing before edge tracing is well defined. Nonetheless it is vital to keep in mind that splits on $E^\mathcal{B}$ change the underlying base mesh $\mathcal{B}$. Proceeding with this refinement without post-processing to clean up $\mathcal{B}$ can lead to excessively refined base meshes. Figure \ref{fig:CollapseStressTest} shows such an example -- a mesh that was deliberately excessively refined by running a collapse stress test on $\mathcal{M}$ and not including any post-processing. This manifests in areas of $\mathcal{B}$ becoming extremely dense, since embedding meta edges into already split areas results in \textit{even more} splits in later traversals.

The excessive refinement caused on the mesh in Figure \ref{fig:CollapseStressTest} alone should be reason enough to find a way of post-processing that avoids this. Ideally, non-original edges in $E^\mathcal{B}$ should only exist as long as they need to, and then be collapsed. Much like we defined a pre-processing algorithm for a patch \textit{before} tracing an edge, we also define a post-processing algorithm to undo all changes that can be undone \textit{after} a trace completes, and every time an edge $\Phi\left(e^\mathcal{M}_i\right)\in \Phi\left(E^\mathcal{M}\right)$ is removed from the embedding $\Phi\left(\mathcal{M}\right)$.

\begin{algorithm}[H]
\caption{Patch post-processing}\label{alg:PostProcessPatch}
\begin{algorithmic}[1]
\Function{PostProcessPatch}{$\Phi(e^\mathcal{M}_x)$}
\For {$\Phi(h^\mathcal{M}_i)\in \textsc{Patch}(\Phi(e^\mathcal{M}_x))\cup \Phi(h^\mathcal{M}_{x_1})\cup \Phi(h^\mathcal{M}_{x_2})$}\label{alg:PostProcessPatchLoop1}
\For {$h^{\mathcal{B}}_j\in \Phi(h^\mathcal{M}_i)$}\label{alg:PostProcessPatchLoop2}
\For {$h^{\mathcal{B}}_{X,Y}\in \textsc{LeftHalfCircle}(\Phi(h^\mathcal{M}_i))$}\label{alg:PostProcessPatchLoop3}
\If {$v^{\mathcal{B}}_{X}\notin V^\mathcal{B'}\land h^{\mathcal{B}}_{X,Y}\notin H^\mathcal{B'}$}
\State $\textsc{ConditionalMerge}(h^{\mathcal{B}}_{X,Y})$
\EndIf
\EndFor
\EndFor
\EndFor
\EndFunction
\end{algorithmic}
\end{algorithm}

Edge traversal works in the same way for Algorithms \ref{alg:PreProcessPatch} and \ref{alg:PostProcessPatch}, with the exception that the post-processing Algorithm \ref{alg:PostProcessPatch} iterates over the newly traced edge $\Phi\left(e^\mathcal{M}_x\right)$ as well.\footnote{This is denoted by also iterating over $\Phi\left(h^\mathcal{M}_{x_1}\right)$ and $\Phi\left(h^\mathcal{M}_{x_2}\right)$ in Line \ref{alg:PostProcessPatchLoop1} of Algorithm \ref{alg:PostProcessPatch}.} $V^\mathcal{B'}, H^\mathcal{B'}$ denote the \textit{original} halfedges and vertices of $\mathcal{B}$; we want to collapse non-original vertices along non-original halfedges. For those halfedges we check if they should be collapsed:

\begin{algorithm}[H]
\caption{Conditional merging}\label{alg:ConditionalMerge}
\begin{algorithmic}[1]
\Function{ConditionalMerge}{$h^{\mathcal{B}}_{X,Y}$}
\If {$v^\mathcal{B}_X\in \Phi(V^\mathcal{M})$}\label{alg:ConditionalMergeCond1}
\State\Return{}
\EndIf
\If {$(v^\mathcal{B}_Y\in \Phi(V^\mathcal{M})\land\neg\textsc{IsMetaEdge}(h^\mathcal{B}_{X,Y})\land\textsc{IsMetaEdge}(v^\mathcal{B}_{X}))$}\label{alg:ConditionalMergeCond2}
\State\Return{}
\EndIf
\If {$\textsc{IsMetaEdge}(v^\mathcal{B}_X)\land\textsc{IsMetaEdge}(v^\mathcal{B}_{Y})\land(\textsc{MetaEdge}(v^\mathcal{B}_X)\neq\textsc{MetaEdge}(v^\mathcal{B}_{Y}))$}\label{alg:ConditionalMergeCond3}
\State\Return{}
\EndIf
\If {$\textsc{IsMetaEdge}(v^\mathcal{B}_X)\land\textsc{IsMetaEdge}(v^\mathcal{B}_{Y})\land\neg\textsc{MetaEdge}(h^\mathcal{B}_{X,Y})$}\label{alg:ConditionalMergeCond4}
\State\Return{}
\EndIf
\State $\textsc{Collapse}(h^{\mathcal{B}}_{X,Y})$\label{alg:ConditionalMergeFin}
\EndFunction
\end{algorithmic}
\end{algorithm}

Where \textsc{MetaEdge}($v^\mathcal{B}_x$)/\textsc{MetaEdge}($h^\mathcal{B}_y$) returns the meta edge that $v^\mathcal{B}_x$ or $h^\mathcal{B}_y$ lies on, and $-1$ if it does not lie on a meta edge. Step by step: 
\begin{itemize}
\item \textbf{Line \ref{alg:ConditionalMergeCond1}}: Check if the \textit{from\_vertex} $v^\mathcal{B}_X$ of $h^{\mathcal{B}}_{X,Y}$ is an embedded meta vertex. We disallow collapsing in those cases since the \textit{from\_vertex} is deleted in a collapse.
\item \textbf{Line \ref{alg:ConditionalMergeCond2}}: Check if the \textit{to\_vertex} $v^\mathcal{B}_Y$ of $h^{\mathcal{B}}_{X,Y}$ is an embedded meta vertexy $h^{\mathcal{B}}_{X,Y}$ does \textit{not} lie on a meta edge, and $v^\mathcal{B}_X$ does lie on a meta edge. If those conditions are true, collapsing $h^{\mathcal{B}}_{X,Y}$ would merge a meta edge with a meta vertex it does not belong to, so a collapse cannot happen.
\item \textbf{Line \ref{alg:ConditionalMergeCond3}}: Check if both vertices $v^\mathcal{B}_X$ and $v^\mathcal{B}_Y$ lie on distinct meta edges. In that case, collapsing $h^{\mathcal{B}}_{X,Y}$ would merge two separate meta edges, so it cannot be permitted.
\item \textbf{Line \ref{alg:ConditionalMergeCond4}}: Check if both vertices $v^\mathcal{B}_X$ and $v^\mathcal{B}_Y$ lie on meta edges, but $h^{\mathcal{B}}_{X,Y}$ does not. This catches the case where an edge passes by itself, and also shouldn't be collapsed.
\item \textbf{Line \ref{alg:ConditionalMergeFin}}: If none of the above conditions apply, $h^{\mathcal{B}}_{X,Y}$ is collapsed.
\end{itemize}

Using the post-processing Algorithm \ref{alg:PostProcessPatch} after every edge trace, and a similar method accompanying every embedded edge deletion, the base mesh $\mathcal{B}$ is kept clean of superfluous non-original elements -- making it so that after every operation $\mathcal{B}$ is as close to $\mathcal{B}'$ as possible without merging elements of the embedding $\Phi\left(\mathcal{M}\right)$. We also provide global pre- and post-processing methods from a previous implementation, but local methods are generally preferable since they scale better.

\textbf{Addendum}:  Note that during the initial triangulation conditions are slightly different. While not enough edges $\Phi\left(e^\mathcal{M}_i\right)$ are traced into $\mathcal{B}$, there are no borders to constrain new traces. This could lead to wrong homotopy and block other traces later on, due to differences between the connectivities of $\mathcal{M}$ and $\Phi\left(\mathcal{M}\right)$. In order to avoid this potential problem, we further expand our set of restrictions to include borders between Voronoi regions. Then we extend our restrictions by those edges that connect Voronoi regions with restrictions.
 
\begin{align}
  R^\mathcal{B}_{\text{Voronoi\_Edges}} := \left\{e^{\mathcal{B}}_{A,B}\in E^\mathcal{B} \Big| \textsc{Voronoi}\left(v^{\mathcal{B}}_A\right) \neq \textsc{Voronoi}\left(v^{\mathcal{B}}_B\right)\right\}\nonumber
\end{align}

Pre-processing during the initial triangulation also has to split edges in the new set of restrictions $R^\mathcal{B}_{\text{Voronoi\_Edges}}$ that have an incident vertex lying on a meta edge. Aside from that, pre-processing works just like normally during the initial triangulation. After the initial triangulation is done, post-processing is performed globally, to remove unneeded unoriginal base edges around Voronoi borders.

Figure \ref{fig:TetraThing} showcases a meta mesh $\mathcal{M}$ embedded into a base mesh $\mathcal{B}$ of genus 4 that has been collapsed until only one meta vertex $v^\mathcal{M}_A$ remains. This demonstrates the robustness of our pre-processing and trace operations, as edges of $E^\mathcal{M}$ were correctly collapsed hundreds of times until no more collapses were possible, and no traces failed either. All of this was executed properly on a high-genus base mesh $\mathcal{B}$, supporting our premise of all embedded faces $\Phi\left(f^\mathcal{M}_i\right)\in\Phi\left(F^\mathcal{M}\right)$ being simple. Post-processing on $\mathcal{B}$ was disabled for demonstrative purposes -- the artifacts on $\mathcal{B}$ show why it is needed.

\begin{figure}%[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/TetraThing.pdf_tex}\par
   }
  \caption{A meta mesh $\mathcal{M}$ embedded into a base mesh $\mathcal{B}$ of genus 4, collapsed down to one embedded meta vertex $\Phi\left(v^\mathcal{M}_A\right)$. Artifacts remain on $\mathcal{B}$ as no post-processing was done.}
  \label{fig:TetraThing}
\end{figure}
