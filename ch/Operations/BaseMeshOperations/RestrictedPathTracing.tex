\subsection{Restricted Path Tracing}
\label{subsec:RestrictedPathTracing}

\begin{wrapfigure}[18]{r}{0.50\textwidth}
    \vspace{-20pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/CatEdges.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Embedded edges $\Phi\left(e^\mathcal{M}_{i}\right)\in \Phi\left(E^\mathcal{M}\right)$; meta edges $e^{\mathcal{M}}_i\in E^{\mathcal{M}}$ overlayed.}
  \label{fig:CatEdges}
\end{wrapfigure}

Meta edges $e^{\mathcal{M}}_i\in E^{\mathcal{M}}$ are embedded into the base mesh $\mathcal{B}$ as consecutive chains of base edges such that $\Phi\left(e^\mathcal{M}_{i}\right):=\{e^{\mathcal{B}}_1, e^{\mathcal{B}}_2, \dots, e^{\mathcal{B}}_n\}$. Figure \ref{fig:CatEdges} visualizes such an embedding in practice. Here, embedded edges $\Phi\left(e^\mathcal{M}_{i}\right)$ are drawn into an underlying base mesh $\mathcal{B}$ in colors, and the corresponding meta edges $e^{\mathcal{M}}_i$ are overlayed and colored in the same way.

When finding an embedding for an edge $e^{\mathcal{M}}_i$, the embedding should generally be as short as possible.\footnote{Much like in regular meshes, where edges are direct lines between vertices -- the shortest path.} Formally speaking, in order to embed an edge $e^{\mathcal{M}}_{A,B}$ from vertices $v^{\mathcal{M}}_A$ to $v^{\mathcal{M}}_B$ into $\mathcal{B}$, we are trying to minimize:

\begin{align}
\min_{\Phi\left(e^\mathcal{M}_{A,B}\right)}\left(\left|\Phi\left(e^\mathcal{M}_{A,B}\right)\right|\right) \nonumber
\end{align}
where
\begin{align}
\left|\Phi\left(e^\mathcal{M}_{A,B}\right)\right|:=\sum_{e^{\mathcal{B}}_i\in \Phi\left(e^\mathcal{M}_{A,B}\right)}\left(\left|e^{\mathcal{B}}_i\right|\right)\nonumber
\end{align}
with
\begin{align}
\Phi\left(e^\mathcal{M}_{A,B}\right):= \left\{e^{\mathcal{B}}_{A,C}, e^{\mathcal{B}}_{C,D}, \dots, e^{\mathcal{B}}_{N,B}\right\},
v^{\mathcal{B}}_A = \Phi\left(v^\mathcal{M}_A\right),
v^{\mathcal{B}}_B = \Phi\left(v^\mathcal{M}_B\right) \nonumber
\end{align}

With an edge in the form $e_{X,Y}$ denoting $X$ as its \textit{from\_vertex} and $Y$ as its \textit{to\_vertex}. In other words, we are trying to find the edge embedding $\Phi\left(e^\mathcal{M}_{A,B}\right)$ for which the length $\left|\Phi\left(e^\mathcal{M}_{A,B}\right)\right|$ is minimal.

Finding a minimal path between two vertices $v^\mathcal{B}_A$ and $v^\mathcal{B}_B$ on a graph is a well-established problem, which has been solved previously by Dijkstra's Algorithm \cite{dijkstra1959note}, which basically performs a breadth-first search from $v^\mathcal{B}_A$ until $v^\mathcal{B}_B$ is reached. This can be improved by, instead, performing two breadth-first searches from $v^\mathcal{B}_A$ and $v^\mathcal{B}_B$ until they meet somewhere in the middle. Bidirectional search is more efficient since instead of growing one circle with a radius equaling the distance between $v^\mathcal{B}_A$ and $v^\mathcal{B}_B$, two circles are grown with half the radius. The surface of those two circles is lower, and thus the search has to step over less vertices.

A further improvement is to introduce heuristics into Dijkstra's Algorithm, which the algorithm known as A* \cite{hart1968formal} does. Dijkstra's Algorithm performs a breadth-first search, expanding the searched area evenly as a sphere. But when it comes to tracing geodesic paths between two vertices $\Phi\left(v^\mathcal{M}_A\right)$ and $\Phi\left(v^\mathcal{M}_B\right)$, the Euclidean distance can be used to decide in which directions to search first. Rather than expand the search area evenly, A* weights areas that optimize a potential $\pi_i(v_x)$ higher. Our potential functions $\pi_A(v^\mathcal{B}_x), \pi_B(v^\mathcal{B}_x)$ return the Euclidean distances between  $v^\mathcal{B}_x$ and $\Phi\left(v^\mathcal{M}_A\right)$ or $\Phi\left(v^\mathcal{M}_B\right)$ respectively.

The geodesic distance between two points $v^\mathcal{B}_A$ and $v^\mathcal{B}_B$ on the surface of a mesh is measured as the sum of the edge lengths of the shortest edge chain between $v^\mathcal{B}_A$ and $v^\mathcal{B}_B$. However in most cases, the Eucledian distance between $v^\mathcal{B}_A$ and $v^\mathcal{B}_B$ is a good heuristic approximating the geodesic distance. For bidirectional A* between vertices $v^\mathcal{B}_A$ and $v^\mathcal{B}_B$, we require two potential functions $\pi_A(v^\mathcal{B}_x), \pi_B(v^\mathcal{B}_x)$ which estimate the distances between $v^\mathcal{B}_A$ and $v^\mathcal{B}_x$ as well as $v^\mathcal{B}_B$ and $v^\mathcal{B}_x$ respectively. However, A* requires its heuristic functions to fulfil certain properties.

In order to use those potential functions in A*, they have to be \textit{consistent} and \textit{admissible}, otherwise A* fails. A heuristic function is admissible if it never overestimates the cost of reaching the goal, and consistent  if its estimate is always less than or equal to the estimated distance from any neighboring vertex to the goal, plus the cost of reaching that neighbor. $\pi_A(v^\mathcal{B}_x)$ and $\pi_B(v^\mathcal{B}_x)$ are admissible but not consistent potential functions, however their average is a consistent potential function \cite{ikeda1994fast}. Thus we derive the potential functions:
\begin{align}
p_A\left(v^\mathcal{B}_x\right) = \frac{1}{2}\left(\pi_A\left(v^\mathcal{B}_x\right)-\pi_B\left(v^\mathcal{B}_x\right)\right)+\frac{\pi_B\left(v^\mathcal{B}_A\right)}{2}\nonumber\\
p_B\left(v^\mathcal{B}_x\right) = \frac{1}{2}\left(\pi_B\left(v^\mathcal{B}_x\right)-\pi_A\left(v^\mathcal{B}_x\right)\right)+\frac{\pi_A\left(v^\mathcal{B}_B\right)}{2}\nonumber
\end{align}
Note that the terms $\frac{\pi_B\left(v^\mathcal{B}_A\right)}{2}$ and $\frac{\pi_A\left(v^\mathcal{B}_B\right)}{2}$ are constant, and only there to ensure that $p_A\left(v^\mathcal{B}_A\right)=0$ and $p_B\left(v^\mathcal{B}_B\right)=0$. The vertices $v^\mathcal{B}_i$ are then sorted according to path lengths and heuristics $p_A\left(v^\mathcal{B}_X\right), p_B\left(v_x\right)$ into two minheaps, and A* is performed with a few restrictions. 

We want to restrict A* to only find paths inside sectors enclosed by embedded meta edges $\Phi\left(e^\mathcal{M}_i\right)$. That way edges cannot overlap, and cyclic edge order is preserved. This is achieved during tracing, by excluding certain vertices when searching for neighbors. Whenever a vertex $v^\mathcal{B}_j$ is on top of the heap during A*, its outgoing halfedges are added onto the heap with the distance value of $v^\mathcal{B}_j$ plus the edge length to $v^\mathcal{B}_j$ and adjusted by our heuristics. The restriction comes into place here; whenever a neighboring vertex of $v^\mathcal{B}_j$ lies on a meta edge or meta vertex, that neighbor is not considered. Furthermore, during initial triangulation, neighbors lying on Voronoi borders, except the one corresponding to the edge that is being traced, are also not considered.

These restrictions ensure that a trace stays inside its sector, making it important to ensure that traces always starts inside the correct sector (or the two sides of a trace cannot connect). This is done by circulating over the outgoing meta and base halfedges of the start and end vertices $v^\mathcal{B}_x$, $v^\mathcal{B}_y$ of the edge to be trace $\Phi\left(e^\mathcal{M}_{x,y}\right)$, and finding outgoing base halfedges that respect cyclical edge order. Tracing is then performed only in the sectors these halfedges point to.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/TracingVisualized.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Bidirectional A* tracing of (a) edge $\Phi(e^\mathcal{M}_{B,C})$ during initial triangulation (b) edge $\Phi(e^\mathcal{M}_{A,D})$ after a flip. The base halfedges marked in red were traversed in A*.}
  \label{fig:TracingVisualized}
\end{figure}

Figure \ref{fig:TracingVisualized} visualizes the halfedges of $\mathcal{B}$ traversed during A* edge tracing, by marking them in red.
\begin{itemize}
\item \textbf{(a)}: Here, the edge $\Phi\left(e^\mathcal{M}_{B,C}\right)$ (brown) was just traced during the \textit{initial triangulation} of $\Phi\left(\mathcal{M}\right)$. During initial triangulation, traces are restricted to the Voronoi regions of the two vertices they connect, and only allowed to traverse the single Voronoi border corresponding to the edge. In this case, the traces traversed the entire parts of their respective Voronoi regions in the tracing sector, since the shape enclosing the traced base edges is rectangular and not round.

Thus, the restriction towards Voronoi regions during initial triangulation not only ensures proper topology, but also makes the trace operation slightly faster, since there are less vertices that can be legally traversed by A*. In the middle of $\Phi\left(e^\mathcal{M}_{B,C}\right)$ a few base edges can be seen where both halfedges were selected, meaning the trace reached those edges from both sides. Edge $\Phi\left(e^\mathcal{M}_{B,C}\right)$ itself was then drawn into $\mathcal{B}$ based on the shortest path found by A*.
\item \textbf{(b)}: The previous edge $\Phi\left(e^\mathcal{M}_{B,C}\right)$ was flipped, and is now $\Phi\left(e^\mathcal{M}_{A,D}\right)$. Since the embedding has already been initialized, A* can traverse over the entirety of the sector spanned by $\left\{\Phi\left(e^\mathcal{M}_{A,B}\right),\Phi\left(e^\mathcal{M}_{B,C}\right),\Phi\left(e^\mathcal{M}_{C,D}\right),\Phi\left(e^\mathcal{M}_{D,A}\right)\right\}$. This property manifests itself in the areas traversed resembling quarter circles around $\Phi\left(v^\mathcal{M}_A\right)$ and $\Phi\left(v^\mathcal{M}_D\right)$.
\end{itemize}

Generally, there is no fixed homotopy for a traced edge. If the patch in which a trace is being performed contains handles, traced edges can pass by on either sides of those handles. This is where the restriction that all embedded meta faces $\Phi\left(f^\mathcal{M}_i\right)\in\Phi\left(F^\mathcal{M}\right)$ are simple\footnote{A patch is simple if it contains no internal handles and has one continuous closed boundary.} becomes useful. Whenever we trace in a simple patch, the homotopy of the traced edge is unambiguous. This is why we ensure that traces happen in restricted sectors without internal handles.
