\subsection{Vertex Relocation}
\label{subsec:VertexRelocation}

\begin{figure}[ht]
\begin{centering}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/RelocateEdgeSplit.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Vertex relocation from $\Phi\left(v^\mathcal{M}_A\right)$ to $\Phi\left(v^\mathcal{M}_B\right)$ on an edge of $\Phi\left(E^\mathcal{M}\right)$.}
  \label{fig:relocedgesplit}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/RelocateFaceSplit.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Vertex relocation from $\Phi\left(v^\mathcal{M}_A\right)$ to $\Phi\left(v^\mathcal{M}_B\right)$ on a face of $\Phi\left(F^\mathcal{M}\right)$.}
  \label{fig:relocfacesplit}
\end{subfigure}
\end{centering}
 \caption{Vertex relocation as a concatenation of a split and a collapse.}
 \label{fig:relocwrapper}
\end{figure}

The first operation which updates $\Phi\left(\mathcal{M}\right)$ without changing the topology of $\mathcal{M}$ is vertex relocation. This operation is performed by moving an embedded vertex $\Phi\left(v^\mathcal{M}_A\right)$ into a position in an adjacent face or edge, so it doesn't change the connectivity of $\mathcal{M}$. However, the edges $\Phi\left(e^\mathcal{M}_{A,i}\right)$ of $\Phi\left(v^\mathcal{M}_A\right)$ have to be retraced. And, as stated previously in Section \ref{subsec:HalfedgeCollapse}, the order in which edges are retraced matters, if the patch of surrounding faces of $\Phi\left(v^\mathcal{M}_A\right)$ does not have disk topology. 

Thus it is convenient to define vertex relocation as a concatenation of a split and a collapse, thus alleviating the need for a complex implementation. A vertex relocation would then look as shown in Figure \ref{fig:relocedgesplit} for relocation into an edge and Figure \ref{fig:relocfacesplit} for relocation into a face respectively. Step by step:

\begin{enumerate}
\item Check if the spot for relocation is a base vertex $v^\mathcal{B}_i$ lying inside an adjacent face or on an edge connected to $\Phi\left(v^\mathcal{M}_A\right)$.
\item Do an edge split or face split operation at $\Phi\left(v^\mathcal{M}_B\right)$.
\item Collapse the old vertex $\Phi\left(v^\mathcal{M}_A\right)$ into the new vertex $\Phi\left(v^\mathcal{M}_B\right)$.
\end{enumerate}

In default cases, as showcased above, this works pretty well, however the embedded meta mesh structure permits self-edges, so there needs to be a special look at triangles including self-edges.

\begin{figure}[ht]
\begin{centering}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/RelocateCircleEdgeSplit.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Vertex relocation from $\Phi\left(v^\mathcal{M}_A\right)$ to $\Phi\left(v^\mathcal{M}_B\right)$ on an edge of $\Phi\left(E^\mathcal{M}\right)$.}
  \label{fig:reloccircleedgesplit}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/RelocateCircleFaceSplit.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Vertex relocation from $\Phi\left(v^\mathcal{M}_A\right)$ to $\Phi\left(v^\mathcal{M}_B\right)$ on a face of $\Phi\left(F^\mathcal{M}\right)$.}
  \label{fig:reloccirclefacesplit}
\end{subfigure}
\end{centering}
 \caption{Vertex relocation as a concatenation of a split and a collapse in a triangle with a self-edge}
 \label{fig:reloccirclewrapper}
\end{figure}

Figure \ref{fig:reloccirclewrapper} displays how a relocation would also work as a concatenation of a split and a collapse in a face with a self-edge, making it capable to relocate vertices in any possible face of our embedding.

However in some practical applications, it can be quite desirable to perform vertex relocations on a lower level rather than via concatenation. For instance when remeshing, performing low level relocations can help declutter surrounding patches of embedded vertices $\Phi\left(v^\mathcal{M}_i\right)$ on $\mathcal{B}$ by temporarily emptying them; something which relocation via concatenation doesn't do. Controversely, a low-level relocation is not applicable to all patches, so we keep both relocation by concatenation for stability, and low-level relocation for speed on simple surrounding patches.

In general terms, composite relocation is stable and can be used under any circumstances, whereas low-level relocation is faster and changes $\mathcal{B}$ less, but can fail if the surrounding patch of the vertex being relocated is not simple. Depending on the application, it can make sense to automatically detect non-simple patches to apply composite relocation on, and then apply low-level relocation to the rest.

Our low-level relocation works by removing all edges of a patch, inserting the new vertex, then retracing all edges \textit{in the correct order}. The order is particularly important in order to avoid edges blocking sectors or being traced along the wrong sides of handles. For relocation of a vertex $\Phi\left(v^\mathcal{M}_A\right)$, the order goes as follows:
\begin{enumerate}
\item \textbf{Boundary edges}: These are traced first since they have a clearly defined embedding and should not be blocked by any other edges.
\item \textbf{Group representatives}: For each distinct connected group of vertices on the patch surrounding $\Phi\left(v^\mathcal{M}_A\right)$, trace one edge connected to each group. This builds a minimal spanning tree.
\item \textbf{Non-duplicate non-self edges}: Trace all edges that are not self-edges, but if there is more than one edge connecting two vertices $\Phi\left(v^\mathcal{M}_A\right)$ and $\Phi\left(v^\mathcal{M}_B\right)$, only trace one of them -- otherwise the connectivity of some self-edges may be blocked.
\item \textbf{Self-edges}: Trace all self-edges.
\item \textbf{All remaining edges}: Trace all edges that remained after steps 1-4.
\end{enumerate}
These rules are somewhat complicated, but they are necessary for allowing the tracing of edges in a patch without interference, as long as the Euler characteristic of that patch is 1 or higher. A collapse operation \textit{could} also be implemented in this way, but since we would still need the current edge bending collapse implementation for patches of higher genus, we choose not to do so.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/RetracingDeadlock.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Collapsing Vertex B into Vertex A (a). Intermediate state (b).  Correct retracing result (c). Wrong retracing result  (d).}
  \label{fig:retracingdeadlock}
\end{figure}

There are configurations which cannot be resolved by low-level retracing, specifically patches with Euler characteristic 0 and lower. Figure \ref{fig:retracingdeadlock} shows such an example, where two self-edges interlock and tracing them both is impossible no matter in which order. In that case we fall back to concatenated relocation. 
