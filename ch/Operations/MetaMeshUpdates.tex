\section{Meta Mesh Updates}
\label{sec:MetaMeshUpdates}

In Section \ref{sec:Operations} we defined a series of \textit{atomic operations} which comprise a minimum set of operations necessary to change the connectivity of $\mathcal{M}$, without breaking the topology of the mesh by splitting it into more components. These operations are \textit{halfedge collapse}, \textit{edge rotation}, \textit{edge split} and \textit{face split}. Performing any of these operations will change the connectivity of $\mathcal{M}$, as well as the embedding of the edges $\Phi\left(E^\mathcal{M}\right)$. This section explores how to implement these \textit{atomic operations} while maintaining and respecting our mesh embedding.

\begin{figure}[ht]
    \vspace{-7pt}
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/patches.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{\textit{Surrounding Patches} $P^\mathcal{B}$ (red). (a): Starting mesh. (b): The surrounding patch $P^\mathcal{B}(\Phi(v^\mathcal{M}_A))$ of $\Phi(v^\mathcal{M}_A)$. (c): The surrounding patch $P^\mathcal{B}(\Phi(e^\mathcal{M}_{A,B}))$ of $\Phi(e^\mathcal{M}_{A,B})$.}
  \label{fig:patches}
    \vspace{-7pt}
\end{figure}

In the following, an important term is \textit{surrounding patch}. Several operations on the embedding $\Phi\left(\mathcal{M}\right)$ require removing edges of $\Phi\left(E^\mathcal{M}\right)$ and combining the incident faces. Similarly, we may remove an embedded meta vertex $\Phi\left(v^\mathcal{M}_A\right)$ and all meta edges connected to $\Phi\left(v^\mathcal{M}_A\right)$, then combine the faces around $\Phi\left(v^\mathcal{M}_A\right)$. In both cases, those combinations of faces are called the \textit{surrounding patch} of a meta vertex $\Phi\left(v^\mathcal{M}_A\right)$ or a meta edge $\Phi\left(e^\mathcal{M}_{A,B}\right)$ respectively. Figure \ref{fig:patches} visualizes this.

A surrounding patch $P^\mathcal{B}\left(\Phi\left(v^\mathcal{M}_A\right)\right)$ of an embedded meta vertex $\Phi\left(v^\mathcal{M}_A\right)$ consists of the set of base faces enclosed by the ring of edges $\Phi\left(e^\mathcal{M}_{i}\right)$ surrounding $\Phi\left(v^\mathcal{M}_A\right)$. Figure \ref{fig:patches}.(b) shows the faces of the surrounding patch of $\Phi\left(v^\mathcal{M}_A\right)$ marked in red, as well as the ring of edges $\Phi\left(e^\mathcal{M}_{i}\right)$ surrounding $\Phi\left(v^\mathcal{M}_A\right)$.

A surrounding patch $P^\mathcal{B}\left(\Phi\left(e^\mathcal{M}_{A,B}\right)\right)$ of a meta edge $\Phi\left(e^\mathcal{M}_{A,B}\right)$ consists of the combination of the faces on either side of $\Phi\left(e^\mathcal{M}_{A,B}\right)$, and is enclosed by the edges of those two faces, as seen in Figure \ref{fig:patches}.(c). Analogously, surrounding patches can also be defined for domain $\mathcal{M}$ as $P^\mathcal{M}$, denoting the corresponding meta faces.

\input{ch/Operations/MetaMeshUpdates/EdgeRotation.tex}
\input{ch/Operations/MetaMeshUpdates/HalfedgeCollapse.tex}
\input{ch/Operations/MetaMeshUpdates/EdgeSplit.tex}
\input{ch/Operations/MetaMeshUpdates/FaceSplit.tex}