\subsection{Edge Rotation}
\label{subsec:EdgeRotation}

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/RotateHeader.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Edge rotation in a non-quad patch.}
  \label{fig:RotateHeader}
\end{figure}

The simplest of the atomic mesh operations is edge rotation. Edge rotation moves an edge \textit{counter-clockwise} inside its surrounding patch. Naively, this could be implemented by deleting the edge and then inserting a new edge at the desired position. In practice, however, it is more efficient to instead change a few \textit{next\-\_halfedge\-\_handle} pointers, since this preserves our data structure and all other pointers from and to the edge, as well as the edge itself. 

Most commonly, an edge rotation of an edge $e^{\mathcal{M}}_x$ is equivalent to an edge flip, given that both adjacent faces are triangles. In every other case, rotation happens by reconnecting both halfedges $h^{\mathcal{M}}_{x1}$ and $h^{\mathcal{M}}_{x2}$ of $e^{\mathcal{M}}_x$ to the respective vertices of their successors $h^{\mathcal{M}}_{x1\_next}$ and $h^{\mathcal{M}}_{x2\_next}$. Figure \ref{fig:RotateHeader} shows how rotation on non-quad patches works. 

\begin{wrapfigure}[19]{r}{0.5\textwidth}
  \vspace{-15pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/RotateComplex.pdf_tex}\par
   }
  \vspace{-5pt}
  \caption{(a-b): Rotation resulting in curve. (c-d): Rotation resulting in \textit{self-edge}.}
  \label{fig:RotateComplex}
\end{wrapfigure}

On traditional surface meshes, there are certain restrictions when it comes to rotating edges. For instance, an edge cannot be rotated if the rotation would cause it to overlap with another edge, or if it would cause the edge to connect a vertex with itself. This is necessary mainly due to the fact that edges are straight lines. In an embedded setting, however, edges are very rarely straight and can in fact curve in different ways. This opens up the opportunity to perform almost\footnote{The exception are boundary edges, since flipping a boundary edge would break the boundary and also leave no space to trace the flipped edge in the first place.} any edge rotation on $\mathcal{M}$.

Figure \ref{fig:RotateComplex}.(a-b) shows an example of an edge rotation that would not be geometrically permissible in a traditional mesh. But by allowing curved edges, any rotation can be performed as long as it is possible to trace a curve in the patch surrounding it. Similarly, Figure \ref{fig:RotateComplex}.(c-d) starts from a mesh configuration with some curved edges since we can legally reach those. In configurations like this one, it is possible to rotate an edge so that after the rotation it connects a vertex with itself. This works, since there will always be at least one vertex enclosed by the new \textit{self-edge}. When implementing a structure that allows \textit{self-edges}, it becomes important not to trace the \textit{self-edge} before at least one edge enclosed by it is traced first. Otherwise, the \textit{self-edge} will be traced as the smallest possible loop from the vertex to itself, and also block the way when attempting to trace any elements that should be inside the loop.
