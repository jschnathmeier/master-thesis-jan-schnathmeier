\subsection{Halfedge Collapse}
\label{subsec:HalfedgeCollapse}

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/RetracingDefault.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Collapsing embedded halfedge $\Phi\left(h^\mathcal{M}_{AB}\right)$ from $\Phi\left(v^\mathcal{M}_A\right)$ to $\Phi\left(v^\mathcal{M}_B\right)$  (a). Intermediate state (b). Result after retracing (c).}
  \label{fig:CollapseRetracingDefault}
\end{figure}

The second atomic operation is the halfedge collapse. A naive way of implementing a halfedge collapse of $h^{\mathcal{M}}_{AB}$ from $v^{\mathcal{M}}_A$ to $v^{\mathcal{M}}_B$ goes as follows:
\begin{enumerate}
\item Remove all embedded edges $\Phi\left(e^\mathcal{M}_i\right)$ connected to $\Phi\left(v^\mathcal{M}_A\right)$ from the embedding $\Phi\left(\mathcal{M}\right)$, then remove  $\Phi\left(v^\mathcal{M}_A\right)$.
\item Perform the collapse operation on the meta mesh $\mathcal{M}$\footnote{This is a standard collapse implementation since $\mathcal{M}$ is a standard mesh}.
\item Trace all embedded edges $\Phi\left(e^\mathcal{M}_i\right)$ whose underlying edges $e^{\mathcal{M}}_i$  were \textbf{not} deleted in the previous step into $\Phi\left(\mathcal{M}\right)$.
\end{enumerate}
Note that operations on the embedding have to be performed twice, both on $\mathcal{M}$ as well as $\Phi\left(\mathcal{M}\right)$, in order to maintain the embedding. Figure \ref{fig:CollapseRetracingDefault} illustrates a halfedge collapse of $\Phi\left(h^\mathcal{M}_{AB}\right)$ with an intermediate state, after all embedded edges connected to $\Phi\left(v^\mathcal{M}_A\right)$ have been removed.

It is important to take a closer look at this intermediate state, otherwise problems can arise when retracing the halfedges post-collapse with new connectivity. In the example shown in Figure \ref{fig:CollapseRetracingDefault}, the intermediate patch is simple, and the deleted vertex $v^{\mathcal{M}}_A$ has no self-edges. Most patches are like that, but this is not always the case.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/RetracingLoop.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Collapsing Vertex $\Phi\left(v^\mathcal{M}_A\right)$ into Vertex $\Phi\left(v^\mathcal{M}_B\right)$ (a). Intermediate state (b).  Correct retracing result (c). Wrong retracing result  (d).}
  \label{fig:CollapseRetracingLoop}
\end{figure}

Figure \ref{fig:CollapseRetracingLoop} is such an example where a naive collapse implementation can lead to incorrect results. Notice that, after removing the connected halfedges, the patch $P^\mathcal{B}\left(\Phi\left(v^{\mathcal{M}}_A\right)\right)$ around $v^{\mathcal{M}}_A$ is not simple, as it has two borders. $v^{\mathcal{M}}_A$ also has a self-edge. Naively retracing the edges of $v^{\mathcal{M}}_A$ after collapsing can lead to a situation as seen in Figure  \ref{fig:CollapseRetracingLoop}.(d), where the self-edge gets traced first (as a minimal loop), and subsequent edges get \textit{stuck}\footnote{See the red arrows in Figure \ref{fig:CollapseRetracingLoop}.(d); they represent the Trace happening from vertex $v^{\mathcal{M}}_B$ and the vertex in the top left. As the self-edge created a new boundary, this trace operation fails.}, as the self-edge blocks their tracing sector. In this case, this happened because the self-edge was originally going around the handle, but didn't after being retraced.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/RetracingGroups.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Collapsing $\Phi\left(v^\mathcal{M}_A\right)$ into Vertex $\Phi\left(v^\mathcal{M}_B\right)$ (a). Intermediate state (b).  Correct retracing result (c). Wrong retracing result  (d).}
  \label{fig:CollapseRetracingGroups}
\end{figure}

This type of problem can also occur with isolated groups of vertices. Any group of vertices enclosed by edges of another group can become unreachable, if the edges of the enclosing group are traced before the contents of the internal group. Figure \ref{fig:CollapseRetracingGroups} illustrates a halfedge collapse originating from a vertex $v^{\mathcal{M}}_A$ which has a self-edge enclosing a vertex of valence 1. Figure \ref{fig:CollapseRetracingGroups}.(d) illustrates what happens when the enclosing edges of the yellow vertex are traced first; the yellow vertex becomes unreachable.

The group problem can be solved by first building a minimal spanning tree for the groups of the sector. In  Figure \ref{fig:CollapseRetracingGroups} for example there are three groups, color coded red, teal and yellow. Since $v^{\mathcal{M}}_B$ is color coded red, connecting it with one vertex of the teal and yellow groups builds that minimal spanning tree and avoids groups blocking each others traces. The spanning tree approach is similar to that used by Praun et al. \cite{praun2001consistent} during triangulation, and avoids most blocking problems. 

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/EdgeBending.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{\textit{Edge bending}: An iterative, locally stable way of collapsing a halfedge on $\Phi\left(\mathcal{M}\right)$.}
  \label{fig:EdgeBending}
\end{figure}
However, this is not enough to account for the existence of self-edges. In order to facilitate proper tracing of the edges in a patch, we propose an alternative way of performing collapses. Instead of removing all edges connected to $v^{\mathcal{M}}_A$, and retracing them connected to $v^{\mathcal{M}}_B$, we approach the collapse iteratively. We call this approach \textit{edge bending}; Figure \ref{fig:EdgeBending} illustrates the procedure in detail.
\begin{enumerate}
\item \textbf{(a)}: The initial patch is the same as can be seen in \ref{fig:CollapseRetracingDefault}, the halfedge $\Phi\left(h^\mathcal{M}_{AB}\right)$ from $\Phi\left(v^\mathcal{M}_A\right)$ to $\Phi\left(v^\mathcal{M}_B\right)$ is to be collapsed.
\item \textbf{(b-f)}: We iteratively \textit{bend} each halfedge $\Phi\left(h^\mathcal{M}_{xA}\right)$ ending in $\Phi\left(v^\mathcal{M}_A\right)$, so that it ends in $\Phi\left(v^\mathcal{M}_B\right)$ instead\footnote{Note that the first bent edge results in a loop if the first face is a triangle, and can be deleted immediately in that case, thus Figure \ref{fig:EdgeBending}.(b) starts by bending the \textit{second} edge.}. Each halfedge is immediately retraced after being bent, since this ensures that each halfedge is traced inside a simple patch, with no unconnected groups that could be cut off.
\item \textbf{(g)}: After $\Phi\left(v^\mathcal{M}_A\right)$ has become a vertex of valence 1, it, as well as the halfedge $\Phi\left(h^\mathcal{M}_{AB}\right)$, is removed from $\mathcal{M}$ and $\Phi\left(\mathcal{M}\right)$. Loops are also removed in this step, such as the duplicate connection between $\Phi\left(v^\mathcal{M}_B\right)$ and the connection to the vertex left of it after removing $\Phi\left(h^\mathcal{M}_{AB}\right)$.
\item \textbf{(h)}: Lastly, the edges inside the patch are retraced in reverse order. This does not affect the topology and as such is not strictly necessary, but, as can be seen in Figure \ref{fig:EdgeBending}.(g), skipping this step leads to swirls and long edges which can become problems later on.
\end{enumerate}

The correctness of this approach is guaranteed because every retrace operation on an edge $\Phi\left(e^\mathcal{M}_{i}\right)\in \Phi\left(E^\mathcal{M}\right)$ is performed inside the surrounding patch $P^\mathcal{B}(\Phi(e^\mathcal{M}_{f_1|f_2}))$ of a shared edge $\Phi(e^\mathcal{M}_{f_1|f_2})$ between two faces $\Phi\left(f^\mathcal{M}_1\right), \Phi\left(f^\mathcal{M}_2\right)\in \Phi\left(F^\mathcal{M}\right)$. Surrounding patches $P^\mathcal{B}\left(\Phi\left(e^\mathcal{M}_i\right)\right)$ of embedded edges $\Phi\left(e^\mathcal{M}_i\right)$ are always simple, because:
\begin{itemize}
\item \textbf{$\Phi\left(f^\mathcal{M}_1\right)$ and $\Phi\left(f^\mathcal{M}_2\right)$ are simple}: Every face in our implementation of the embedding has to be simple, this is an important restriction to make basic mesh operations work. 

Using only simple faces allows us to trace edges inside those faces by merely finding the shortest path between two embedded vertices $\Phi\left(v^\mathcal{M}_i\right)$ and $\Phi\left(v^\mathcal{M}_j\right)$ on $\mathcal{B}$. On simple faces this works without restrictions, since regardless of how a path is traced, the homotopy stays the same.\footnote{Since a simple face has no internal handles and exactly one continuous boundary.} Thus, having simple faces is a vital invariant for our data structure. Consequently, it is convenient that the combination of two simple faces along a single edge is again simple.
\item \textbf{$P^\mathcal{B}(\Phi(e^\mathcal{M}_{f_1|f_2}))$ results from joining $\Phi\left(f^\mathcal{M}_1\right)$ and $\Phi\left(f^\mathcal{M}_2\right)$ by removing one shared edge $\Phi(e^\mathcal{M}_{f1|f2})$}: Joining two simple surfaces along a single shared boundary results in a simple surface. 
\end{itemize}

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/PatchCombination.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Combining simple embedded faces $^\Phi\left(f^\mathcal{M}_1\right)$ and $\Phi\left(f^\mathcal{M}_2\right)$ along the shared edge $\Phi(e^\mathcal{M}_{f_1|f_2})$ resulting in the simple patch $P^\mathcal{B}(\Phi(e^\mathcal{M}_{f_1|f_2}))$.}
  \label{fig:PatchCombination}
\end{figure}

Figure \ref{fig:PatchCombination} visualizes why this is the case. In part (a), we can see two meta faces $f^\mathcal{B}_1$ and $f^\mathcal{M}_2$ embedded into $\mathcal{B}$. $f^\mathcal{B}_1$ and $f^\mathcal{M}_2$ share a common edge $e^\mathcal{M}_{f_1|f_2})$, which is embedded into $\mathcal{B}$ as $\Phi(e^\mathcal{M}_{f_1|f_2})$. Thus, the two faces $f^\mathcal{B}_1$ and $f^\mathcal{M}_2$ make up the surrounding patch of $e^\mathcal{M}_{f_1|f_2}$ as $P^\mathcal{B}(\Phi(e^\mathcal{M}_{f_1|f_2})) = \Phi\left(f^\mathcal{M}_1\right)\cup \Phi\left(f^\mathcal{M}_2\right)$

Given that $f^\mathcal{B}_1$ and $f^\mathcal{M}_2$ are simple, they each have single closed boundaries $\Phi\left(b^\mathcal{M}_1\right)$ and $\Phi\left(b^\mathcal{M}_2\right)$ and no internal handles. The boundary $\Phi\left(b^\mathcal{M}_P\right)$ of the patch $P^\mathcal{B}(\Phi(e^\mathcal{M}_{f_1|f_2}))$ is derived by combining the boundaries $\Phi\left(b^\mathcal{M}_1\right)$ and $\Phi\left(b^\mathcal{M}_2\right)$ except for their shared edge $\Phi(e^\mathcal{M}_{f_1|f_2})$. As can be seen in Figure \ref{fig:PatchCombination}.(b), the yellow boundary $\Phi\left(b^\mathcal{M}_P\right)$ is again closed and continuous.

Since $f^\mathcal{B}_1$ and $f^\mathcal{M}_2$ have no internal handles, any handles that $P^\mathcal{B}(\Phi(e^\mathcal{M}_{f_1|f_2}))$ has would have to interupt the single edge $\Phi(e^\mathcal{M}_{f_1|f_2})$ connecting $f^\mathcal{B}_1$ and $f^\mathcal{M}_2$. But $\Phi(e^\mathcal{M}_{f_1|f_2})$ is entirely shared by $f^\mathcal{B}_1$ and $f^\mathcal{M}_2$, so this cannot be the case; thus $P^\mathcal{B}(\Phi(e^\mathcal{M}_{f_1|f_2}))$ has no internal handles.

Consequently, $P^\mathcal{B}(\Phi(e^\mathcal{M}_{f_1|f_2}))$ has a single closed boundary and no internal handles, meanint it is simple.

Edge bending also has the additional benefit of elegantly dealing with self-edges. The only self-edges we need to consider as potential dangers to our retracing are those connected to the collapsed vertex $\Phi\left(v^\mathcal{M}_A\right)$, since every face is guaranteed to be simple. When the edge bending iteration first reaches a self-edge $\Phi\left(e^\mathcal{M}_{self}\right)$ on $\Phi\left(v^\mathcal{M}_A\right)$, one of its vertex handles is changed to $\Phi\left(v^\mathcal{M}_B\right)$; thus it is momentarily no longer a self-edge. When the self edge $\Phi\left(e^\mathcal{M}_{self}\right)$ is iterated over a second time, everything inside the loop of $\Phi\left(e^\mathcal{M}_{self}\right)$ has already been retraced, so tracing the self-edge $\Phi\left(e^\mathcal{M}_{self}\right)$ becomes safe too.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/val1edgecollapse.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{1. Initial state 2. $\Phi\left(h^\mathcal{M}_{AB}\right)$ collapse 3. 1-Loop removal 4. 2-Loop removal}
  \label{fig:val1edgecollapse}
\end{figure}

\begin{wrapfigure}[10]{r}{0.50\textwidth}
  \vspace{-5pt}
   \def\svgwidth{0.4\textwidth}
   {\centering
   \input{img/Triangles.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{A minimal trimesh in two different configurations}
  \label{fig:triangles}
\end{wrapfigure}

Another special case to consider is the collapse of an edge with an incident vertex of valence 1. In principle, collapses like those are quite simple, since the edge and incident valence 1 vertex can simply be removed from the mesh, and no other topology has to change. However, those collapses introduce loops into the mesh which need to be removed. Figure \ref{fig:val1edgecollapse} shows a collapse of such an edge step by step.

\begin{wrapfigure}[10]{r}{0.50\textwidth}
  \vspace{-45pt}
   \def\svgwidth{0.4\textwidth}
   {\centering
   \input{img/1vertextorus.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{A torus collapsed down to one vertex.}
  \label{fig:1vertextorus}
\end{wrapfigure}

And lastly, collapses cannot be performed if the resulting mesh would stop being a polymesh after the collapse - e.g. have no faces left. This makes it necessary to determine whether a mesh component is minimal, and to forbid collapses in such a case. For example on surface meshes of genus 0 there are exactly two minimal meshes as presented in Figure \ref{fig:triangles}. As a general rule, a face $f^\mathcal{M}_1$ is part of a minimal mesh component if and only if for each halfedge of $f^\mathcal{M}_1$  the opposite halfedge $h^\mathcal{M}_{i\_o}$ is adjacent to the same face $f^\mathcal{M}_2$.\footnote{Assuming all faces are simple like in our setting, otherwise this may not be the case.}

With everything considered, our implementation of edge collapses is robust enough to perform any legal collapse and is even able to collapse complicated meshes down to minimal meshes such as the one in Figure \ref{fig:1vertextorus}, which shows a meta mesh on a torus collapsed down to one vertex.
