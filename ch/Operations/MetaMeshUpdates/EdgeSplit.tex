\subsection{Edge Split}
\label{subsec:EdgeSplit}

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/EdgeSplit1.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{A split operation performed to add vertex $\Phi\left(v^\mathcal{M}_C\right)$ on edge $\Phi\left(e^\mathcal{M}_{AB}\right)$.}
  \label{fig:edgesplit1}
\end{figure}

Another of the atomic mesh operations is the edge split. Figure \ref{fig:edgesplit1} shows how a split operation is performed on edge $\Phi\left(e^\mathcal{M}_{AB}\right)$. (a) shows the neighborhood of edge $\Phi\left(e^\mathcal{M}_{AB}\right)$, (b) shows the insertion of a new vertex $\Phi\left(v^\mathcal{M}_C\right)$ being embedded into $\Phi\left(e^\mathcal{M}_{AB}\right)$, and in part (c) new edges are added to connect $\Phi\left(v^\mathcal{M}_C\right)$ with the vertices incident to its faces.

In the embedded Meta Mesh structure regular splits can be performed in the same way as for regular meshes. Since the neighborhoods around the new vertex $\Phi\left(v^\mathcal{M}_C\right)$ are simple, the order the new edges of $\Phi\left(v^\mathcal{M}_C\right)$ are traced in is irrelevant too. However, there are special types of edges which do not occur in regular meshes which should be looked at.

The special types of edges that can occur in the embedded Meta Mesh structure but not in normal meshes are \textit{self-edges}, and edges with an incident vertex of valence 1.\footnote{In a trimesh, vertices of valence 1 only occur with an adjacent \textit{self-edge}.} When splitting a \textit{self-edge} there is nothing special to consider, since the two faces incident to the new vertex $\Phi\left(v^\mathcal{M}_C\right)$ were simple before the split, and remain simple after it. After a split, a \textit{self-edge} naturally won't be a \textit{self-edge} anymore either. However edges with an incident vertex of valence 1 are a special case in that the two faces, next to the new vertex $\Phi\left(v^\mathcal{M}_C\right)$, that need to be triangulated are the same.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/EdgeSplit2.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{A split being performed on an edge $\Phi\left(e^\mathcal{M}_{AB}\right)$ with an incident vertex of valence 1, however there are two possible results (c-d).}
  \label{fig:edgesplit2}
\end{figure}

Figure  \ref{fig:edgesplit2} visualizes the explained example. On the left there is the neighborhood of edge $\Phi\left(e^\mathcal{M}_{AB}\right)$ which is to be split; with $\Phi\left(v^\mathcal{M}_{B}\right)$ being a vertex of valence 1. In (b), a vertex $\Phi\left(v^\mathcal{M}_C\right)$ is inserted into $\Phi\left(e^\mathcal{M}_{AB}\right)$, and here both faces incident to $\Phi\left(v^\mathcal{M}_C\right)$ are identical. The resulting problem is that, depending on which side of $\Phi\left(v^\mathcal{M}_C\right)$ the triangulation of the face is started from, there are different results (c-d). Since normal split operations are well-defined and will always lead to the same result, this should also be the case for all splits on $\Phi\left(\mathcal{M}\right)$. We require our atomic operations to have a deterministic output.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/EdgeSplit3.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Comparison of the two possible triangulations (a-b) corresponding to Figure \ref{fig:edgesplit2}, and a third, rotated view (c).}
  \label{fig:edgesplit3}
\end{figure}

Fortunately this is the case here too. On a closer look it can be seen that the two possible triangulations around $\Phi\left(v^\mathcal{M}_C\right)$ are actually topologically identical. Figure \ref{fig:edgesplit3}.(a-b) illustrates this by color-coding the edges that are identical for those two triangulations. Figure \ref{fig:edgesplit3}.(c) is another topologically equivalent mesh which can be reached from either of the two previous meshes by rotating $\Phi\left(v^\mathcal{M}_C\right)$ around its self-edge clockwise or counter-clockwise respectively.

With the special cases considered, it is now clear that splits can be performed on all edges $\Phi\left(e^\mathcal{M}_i\right)\in \Phi\left(E^\mathcal{M}\right)$, consistent with the way splits work on regular meshes.