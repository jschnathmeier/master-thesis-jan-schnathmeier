\chapter{Conclusion}
\label{ch:Conclusion}

We designed and implemented a new data structure for embedding a meta mesh $\mathcal{M}$ into a base mesh $\mathcal{B}$ via an embedding $\Phi\left(\mathcal{M}\right)$. This embedding $\Phi\left(\mathcal{M}\right)$ is an injection from $\mathcal{M}$ to $\mathcal{B}$ and includes functions to manipulate both while preserving the necessary properties of the embedding. We implemented a series of atomic operations allowing arbitrary manipulation of $\Phi\left(\mathcal{M}\right)$ while staying in a regular mesh context. Some operations require $\mathcal{B}$ to be changed as well, but all changes on $\mathcal{B}$ are temporary and get reverted as soon as $\Phi\left(\mathcal{M}\right)$ allows it.

Further, we implemented Embedded Isotropic Remeshing on our data structure as a proof of concept for algorithms working on the embedding, as well as a means to initializing a good embedding. Our implementation of Embedded Isotropic Remeshing is functional and stable, but could benefit from some further fine-tuning, in order to scale better for large meshes. Note that this is most likely a limitation of our implementation of the algorithm, not of the underlying data structure itself, as our atomic mesh operations are all local.

Since Embedded Isotropic Remeshing is a newly designed form of the Incremental Isotropic Remeshing algorithm, tailored to our data structure, we tested a series of different parameters over a variety of metrics, to determine which ones work best. This gives us a view into the performance of the algorithm, as well as a set of healthy default parameters.

Taking a step back, our data structure could be best used in applications where a base mesh $\mathcal{B}$ is given, and an abstracted higher resolution view (meta mesh $\mathcal{M}$) is desired. Our mesh embeddings $\Phi\left(\mathcal{M}\right)$ are easy to initialize and maintain, and interface with algorithms in the same way normal meshes do, through the atomic operations we defined. This gives our data structure a high degree of flexibility and ease of use for a broad range of mesh embedding or even surface-to-surface mapping applications.

In the future, our data structure could certainly be expanded upon in a few ways:
\begin{itemize}
\item Edge tracing\footnote{See Section \ref{subsec:RestrictedPathTracing}: Restricted Path Tracing.} could potentially be improved by considering methods such as \cite{kraevoy2004cross} or \cite{bischoff2005snakes} propose, in order to trace through arbitrary meta faces, without having to refine the underlying base mesh $\mathcal{B}$.
\begin{itemize}
\item Alternatively, an even more refined tracing method could proactively split only exactly those edges of $E^\mathcal{B}$ that need to be split, in order to do away with pre-processing and speed up the implementation.
\end{itemize}
\item Initialization of embeddings $\Phi\left(\mathcal{M}\right)$ currently has a few limitations. For example each Voronoi region needs to be of disk topology. These limitations can currently be circumvented by simply initializing a larger $\Phi\left(\mathcal{M}\right)$ and then decimating it, until the desired $\Phi\left(\mathcal{M}\right)$ is reached. A more elegant solution would be an expanded initialization method, that can handle such edge cases.
\item Having multiple base meshes $\mathcal{B}_i$ is currently not supported, but would certainly be a possibility. Finding embeddings $\Phi_i(\mathcal{M})$ to embed one meta mesh $\mathcal{M}$ into many base meshes $\mathcal{B}_i$ should be possible through a simple expansion of our data structure -- in principle it could already be done by running multiple embeddings with matching $\mathcal{M}$. This would extend the functionality of our data structure in the domain of surface-to-surface maps.
\end{itemize}

\begin{figure}[hb]
  \vspace{-12pt}
  \begin{center}
  \includegraphics[width=0.9\textwidth]{img/FertilityMetaMesh2.png}
  \end{center}
  \vspace{-22pt}
  \caption{An embedded mesh $\Phi(\mathcal{M})$ on fertility mesh $\mathcal{B}$ after Embedded Remeshing.}
  \label{fig:NiceFertilityMesh}
  \vspace{-10pt}
\end{figure}