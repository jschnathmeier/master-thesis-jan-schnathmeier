\subsection{Embedding Data Structure}
\label{subsec:EmbeddingDataStructure}

In order to embed a meta mesh into a base mesh further additions to the data structure are needed. To start with, here is a definition of the meta mesh component and how they embed into the base mesh:
\begin{itemize}
\item Each meta vertex is a base vertex (but not every base vertex has to be a meta vertex).
\item Each meta edge is a continuous sequence of base edges; meta edges are not allowed to intersect.
\end{itemize}

\begin{figure}
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/EmbeddingDataStructure.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Data structure of the embedded meta mesh.}
  \label{fig:embeddingdatastructure}
\end{figure}

Figure \ref{fig:embeddingdatastructure} visualizes a meta edge and two meta vertices embedded into a base mesh. The base mesh is colored in gray edges with red vertices, blue vertices denote base vertices that are also meta vertices, and one embedded meta halfedge is colorized in black in the base mesh. The blue line represents an implicit representation of the embedded meta halfedge drawn as a direct connection between two vertices rather than a sequence of base edges.

Much like the halfedge data structure OpenMesh is based on, the embedding is represented via a series of pointers between halfedges and vertices.

\begin{enumerate}
\item Each meta vertex is connected to a base vertex.
\item Each base vertex \textit{can be} connected to a meta vertex if one lies on it, otherwise that pointer is in a special state denotating that the base vertex is not connected.
\item Each meta halfedge is connected to the \textit{first} base halfedge it consists of.
\item Each base halfedge that a meta halfedge lies on is connected to that meta halfedge.
\item Each base halfedge that lies on a meta halfedge points towards the next base halfedge in that sequence (if it is not the last).
\end{enumerate}

\begin{wrapfigure}[18]{r}{0.45\textwidth}
  \vspace{-1pt}
  \begin{centering}
   \includegraphics[width=0.45\textwidth]{img/CatMetaMesh.png}\par
  \end{centering}
   \vspace{-1pt}
  \caption{A base mesh with embedded meta mesh: left - The meta mesh represented explicitly: right}
  \label{fig:catmetamesh}
\end{wrapfigure}

Note that meta faces are not connected to base faces, since the costs outweigh the benefit. Connecting base faces to their respective meta face would require iterating over a lot of base faces every time a new meta edge is traced, whereas in applications so far it was rare that the meta face of a base face needed to be addressed. As such it is computationally cheaper to computationally determine the meta face associated to a base face by doing an outward search until a meta edge is reached.

In this implementation the meta mesh has an explicit representation as a mesh which is then connected with its underlying base mesh via pointers, as seen in Figure \label{fig:catmetamesh}. But it would also be possible to represent the meta mesh entirely implicitly via properties on base mesh elements. It can however be argued that having an explicit representation is worth it since it makes the structure more transparent and operations on the meta mesh easier to apply. Another big advantage of having an explicit representation of the meta mesh is the speed of mesh traversal. To traverse from one meta vertex to another on the base mesh would require the traversal of a potentially long sequence of base halfedges, whereas by traversing meta mesh halfedges a lot of operations can be skipped. This increases the speed and comfort of operations on the meta mesh at the cost of the space required to represent the meta mesh as a second mesh connected to the base mesh.