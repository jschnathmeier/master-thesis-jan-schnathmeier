\subsection{Retracing after collapses}
\label{subsec:CollapseRetracing}


After a collapse operation has been called on the Meta Mesh, the Embedding on the Base Mesh naturally has to reflect this. Initially embedded edges are traced using A*, and the same happens for every new edge after a collapse. When collapsing a vertex B into a vertex A this means that first all edges connecting to vertex B have to be removed from the Base Mesh and then retraced as A-edges afterwards. In Figure \ref{fig:retracingdefault} the initial state is illustrated on the left. After the deletion of all B-edges, the 1-ring of B turns into a single face as seen in the second mesh in Figure  \ref{fig:retracingdefault}. Finally the new edges are traced into the base mesh.

\begin{figure}[h]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/RetracingDefault.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Collapsing vertex B into vertex A (left). Intermediate state (middle). Result after retracing (right).}
  \label{fig:retracingdefault}
\end{figure}

Legal collapses on normal meshes will generally look like this, however the embedded mesh structure adds additional challenges to it. We cannot assume that the 1-ring neighborhood of B will always form a face with a single boundary. and our structure also permits the existence of self-edges. Whereas in the default example in Figure \ref{fig:retracingdefault} the order in which new edges are traced into A is irrelevant, it is highly important in cases with self-edges and of 1-ring neighborhoods with a different Euler characteristic.

\begin{figure}[h]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/RetracingLoop.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Collapsing Vertex B into Vertex A (left). Intermediate state (middle left).  Correct retracing result (middle right). Wrong retracing result  (right).}
  \label{fig:retracingloop}
\end{figure}

Figure \ref{fig:retracingloop} showcases such an example; the 1-ring neighborhood of B here is a cylinder and B has a self-edge. Thus the 1-ring neighborhood of B can be split into two connected groups; the vertices connected to A and the group above (yellow vertices). A correct retracing of the edges would look like the third mesh in the picture, however if the self-edge is traced first it will result in a degenerate self-edge that minimally connects A with itself and then blocks the retracing of the other edges. Tracing any of the edges in the yellow group before the self-edge prevents this from happening, as the direct trivial self-edge will be blocked and thus the self-edge will have to be traced around the back. From this we can infer that when self-edges surround other edges tracing those edges first preserves the topology of the embedded mesh.

\begin{figure}[h]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/RetracingGroups.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Collapsing Vertex B into Vertex A (left). Intermediate state (middle left).  Correct retracing result (middle right). Wrong retracing result  (right).}
  \label{fig:retracinggroups}
\end{figure}

A different potential problem arises from the separation of the 1-ring neighborhood into separate groups. In Figure \ref{fig:retracinggroups} there is an example of a 1-ring neighborhood with 3 separate connectivity groups (A-group, yellow group, pink group). Potential problems arise due to the positioning of the purple group between the yellow group and point A. Here if all the edges of the yellow group to A are traced first it can happen that the edge from pink to A becomes untraceable as seen on the right. In this specific case this can be avoided by tracing the pink-A edge first; in the broader case a group of vertices inside a face consisting of A and two vertices of another group should be traced first. So far I've only seen such cases with valence 1 vertices being the internal group, however maybe this can happen for bigger groups too on higher geni surfaces.

These considerations so far led me to a retracing approach that works in multiple passes as follows:
\begin{enumerate}
\item \textit{Trace the edge of one representative for each non-A group.} This should be first to preserve the previous topology. A single edge per non-A group also cannot enclose or block other groups, at least two edges of the same group are needed for that. 
\item \textit{Trace self-edges.} The way self-edges can break the topology is by cutting off groups; after a representative of each group has its edge traced self-edges should follow the correct path.
\item \textit{Trace the remaining edges.} On cylinder shaped surfaces there have been examples where a group had a separating self-edge; tracing the entire group first would break the topology in that case.
\end{enumerate}

\begin{figure}[h]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/RetracingDeadlock.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Collapsing Vertex B into Vertex A (left). Intermediate state (middle left).  Correct retracing result (middle right). Wrong retracing result  (right).}
  \label{fig:retracingdeadlock}
\end{figure}

However these rules seem to be insufficient as there are examples such as in Figure \ref{fig:retracingdeadlock} where there seems to be a deadlock even when retracing in this order. In Figure  \ref{fig:retracingdeadlock} B has a 1-ring neighborhood with a handle, and two interlocking self-edges going around those handles. This creates a deadlock where no matter which of the edges is traced first it degenerates and prevents the tracing of the other edge.

\begin{figure}[h]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/RetracingDeadlockSolution.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption{Collapsing Vertex B into Vertex A (left). Adding temporary vertices into self-edges (middle left).  Intermediate state (middle). Retracing edges (middle right). Result after removing temporary vertices (right).}
  \label{fig:retracingdeadlocksolution}
\end{figure}

 A possible solution could be splitting those self-edges into two or three edges with new vertices along the edge before the collapse, then undoing it afterwards. Figure \ref{fig:retracingdeadlocksolution} shows this step by step. First the initial mesh with the deadlocked self-edges. The self-edges are each split into three edge segments connecting temporary vertices. The B-edges are deleted. The A-edges are traced. Finally the temporary vertices are removed and the edge segments reunited. This method could potentially avoid all deadlock situations and simplify tracing order after collapsing a lot.