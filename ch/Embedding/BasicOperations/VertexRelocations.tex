\subsection{Vertex Relocations}
\label{subsec:VertexRelocations}

\begin{figure}[h]
\begin{centering}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/RelocateEdgeSplit.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Vertex relocation from A to B into an edge.}
  \label{fig:relocedgesplit}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/RelocateFaceSplit.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Vertex relocation from A to B into a face.}
  \label{fig:relocfacesplit}
\end{subfigure}
\end{centering}
 \caption{Vertex relocation as a concatenation of a split and a collapse.}
 \label{fig:relocwrapper}
\end{figure}

Another basic operation on meshes is vertex relocation. This is done by moving a vertex into a position in an adjacent face or edge, so it doesn't change any of the meta mesh connectivity. However, the edges of the vertex being relocated have to be retraced, and, as stated previously in Section \ref{subsec:Collapses}, the order in which edges are retraced matters if the 1-ring of the vertex does not have disc topology. 

Thus it is convenient to define vertex relocation as a concatenation of a split and a collapse, thus alleviating the need for a complex implementation. A vertex relocation would then look like shown in Figure \ref{fig:relocedgesplit} for relocation into an edge and Figure \ref{fig:relocfacesplit} for face splits respectively. Step by step:

\begin{enumerate}
\item Check if the spot for relocation lies inside an adjacent face or in one of those faces' edges.
\item Do an edge split or face split operation at the position
\item Collapse the old vertex into the new vertex
\end{enumerate}

In default cases as showcased above this works pretty well, however the embedded meta mesh structure permits self-edges so there needs to be a special look at triangles including those.

\begin{figure}[h]
\begin{centering}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/RelocateCircleEdgeSplit.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Vertex relocation from A to B into an edge.}
  \label{fig:reloccircleedgesplit}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/RelocateCircleFaceSplit.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Vertex relocation from A to B into a face.}
  \label{fig:reloccirclefacesplit}
\end{subfigure}
\end{centering}
 \caption{Vertex relocation as a concatenation of a split and a collapse in a triangle with a self-edge}
 \label{fig:reloccirclewrapper}
\end{figure}

Figure \ref{fig:reloccirclewrapper} displays how a relocation would also work as a concatenation of a split and a collapse in a face with a self-edge, however this showcases a problem. In Section \ref{subsec:Collapses} the intial assumption was that collapsing an edge with an incident vertex of valence 1 is not permissible, however as can be seen in figure \ref{fig:reloccircleedgesplit} it would be very convenient if it worked. Thus the next section will show that such collapses can actually be well-defined and allowing them is beneficial.