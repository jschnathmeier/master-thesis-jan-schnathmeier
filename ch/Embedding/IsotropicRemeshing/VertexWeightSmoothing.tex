\subsection{Vertex Weight Smoothing}
\label{subsec:VertexWeightSmoothing}

After realizing the nature of the worst case scenarios when using Forest Fire Smoothing, Vertex Weight Smoothing was an idea to improve performance in such scenarios. Seeing how Forest Fire Smoothing potentially relocates a vertex to a local sub-area of the patch that is the thickest, the next approach is rating possible positions by the minimal distance from the patches vertices. Essentially we are attempting to minimize the weight $w$
\begin{equation}
w:=\sum_{p\in{P}}\textsc{Dist}(b,p)^2 \rightarrow \textsc{min}(w)
\end{equation}
for a meta vertex $a$ and the surrounding patch $P$ by finding the vertex $b$ inside the patch $P$ for which $w$ is minimal. In the embedded meta mesh structure this works as follows:

\begin{minipage}[t]{0.25\textwidth}
  \def\svgwidth{\textwidth}
  \centering\raisebox{\dimexpr \topskip-\height}{\input{img/VertexWeights1.pdf_tex}}
  \captionof{figure}{Vertex Weight Smoothing: concept}
  \label{fig:vertexweights1}
\end{minipage}\hfill
\begin{minipage}[t]{0.65\textwidth}
\vspace{-20pt}
\begin{algorithm}[H]
\caption{Vertex Weight Smoothing}\label{alg:vertexweights}
\begin{algorithmic}[1]
\Function{VertexWeights}{$a\leftarrow$meta vertex}
\State $b \leftarrow$ target base vertex
\State $V \leftarrow$ base vertices
\State $R:=\textsc{VVRange}(a)$ 
\State $Q:=\textsc{Queue}\langle vertex\rangle$
\For {$v\in V$}
\State $v_{\textsc{dis}}:=\{\infty,\infty,\dots,\infty\}$
\EndFor
\For{$r \in R$} 
\State $Q_{\textsc{push}}(\textsc{BaseVertex}(r))$
\EndFor
\For{$q \in Q$} 
\State $N := \textsc{Neighbors}(q)$
\For {$n \in N$}
\If {$n_{\textsc{dis}}[q_{\textsc{ID}}]>(q_{\textsc{dis}}[q_{\textsc{ID}}]+|\textsc{E}(b,n)|)$}
\State $n_{\textsc{dis}}[q_{\textsc{ID}}]:=b_{\textsc{dis}}[q_{\textsc{ID}}]+|\textsc{E}(b,n)|$
\State $Q_{\textsc{push}}(n,n_{\textsc{dis}})$
\EndIf
\EndFor
\EndFor
\For{$v\in V$}
\If{$b_{\textsc{score}}>\sum_i{v_{\textsc{dist}}}[i]^2$}
\State $b_{\textsc{score}}:=\sum_i{v_{\textsc{dist}}}[i]^2$
\State $b:=v$
\EndIf
\EndFor
\State\Return $b$
\EndFunction
\end{algorithmic}
\end{algorithm}
\end{minipage}

\begin{wrapfigure}[24]{l}{0.35\textwidth}
  \vspace{-1pt}
   \def\svgwidth{0.35\textwidth}
   {\centering
   \input{img/VertexWeights2.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Vertex Weight Smoothing: bad case}
  \label{fig:vertexweights2}
\end{wrapfigure}

Figure \ref{fig:vertexweights1} and Algorithm \ref{alg:vertexweights} show the process of Vertex Weight Smoothing. Simply put, for an input meta vertex $a$ the area inside the patch surrounding $a$ is iterated over for each meta vertex on the patch, and weights are given for the distance to each of those vertices. The output $b$ is the base vertex inside patch where the sum of the squares of the distances is minimal. Meta vertex $a$ can then be relocated to the position of base vertex $b$.

However after implementing this smoothing algorithm, its shortcomings became immediately apparent. Vertices around a patch are not necessarily distributed uniformly, and as such a vertex may be relocated near the border of a patch rather than towards its center. Over several iterations of the algorithm this caused some meta meshes to shrink and cover less and less area on the base mesh with each iteration, until they converged into one singular ever shrinking triangle. Figure \ref{fig:vertexweights2} shows such a case.

One idea to avoid this problem would be to check for distances from every base vertex instead of every meta vertex around the patch, however this opens up a different set of problems. If the underlying base mesh is not uniform the same problem may still arise, but more importantly the computational cost would increase by a multiple. Thus this is not a viable improvement either.

However while discussing the merits of the Vertex Weights Smoothing algorithm I thought of a modification that may be worth considering.