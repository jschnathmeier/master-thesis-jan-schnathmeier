\subsection{An Incremental Isotropic Remeshing Algorithm}
\label{subsec:IsotropicRemeshingAlgorithm}

In order to test the stability of the embedded meta mesh structure and make use of the previously implemented basic mesh operations the next step is implementing mesh algorithms on the structure. The algorithm featured in this chapter is Incremental Isotropic Remeshing; an algorithm that modifies the topology of a mesh to optimize certain properties. In specific, the goal is to create a new mesh with edge lengths close to a target edge length, and few vertices with valence diverging from six. The resulting mesh should then be more uniform compared to the input, which is desirable since it makes it easier to work with and manipulate.

The term isotropic means that this type of algorithm tries to optimize the shape of triangles to be as equilateral as possible, as opposed to anisotropic remeshing which adapts triangles to surface properties such as curvature. Remeshing algorithms are computationally complex compared to other mesh decimation techniques, but usually generate meshes of high quality. \cite{CGII15}

Incremental Isotropic Remeshing for a target edge length $t$ works by iteratively applying the following steps:
\begin{itemize}
\item \textbf{Edge splits:} Any edge that exceeds the target edge length $t$ multiplied by a factor $\alpha$ (typically $\alpha=4/3$) is split by inserting a new vertex in the middle of it. 
\item \textbf{Edge collapses:} Any edges shorter than $t$ multiplied by a factor $\beta$ (typically $\beta=4/5$) are collapsed. These first two steps create a mesh with an average edge length closer to $t$ than the original mesh.
\item \textbf{Edge rotation:} Whereever an edge rotation would bring the average valence of the mesh closer to 6, that rotation is applied. This is sensible since vertices in a completely regular triangle mesh have a valence of six. However in a non-trimesh meta mesh case this step has to be re-thought.
\item \textbf{Tangential smoothing:}  The final step in this incremental remeshing algorithm smoothes the vertices tangentially and normally; then projects them back onto the object surface. However in the case of an embedded meta mesh, meta vertex positions are restricted to base vertex positions, and as such this step cannot be applied sensibly. It is therefor advisable to find a viable alternative method of vertex relocation that results in equilateral triangles combined with the other steps.
\end{itemize}
These steps are then repeated in several iterations until the mesh converges towards a desirable state, or the mesh is good enough by some metric (eg. standard deviation from target edge length and vertex valence reaching a certain threshold).

The algorithm performs quite well on traditional trimeshes, however as already noted above there are steps that would need changes in order to work for an embedded meta mesh, particularly the smoothing and projection. The requirement here is a smoothing operator that moves vertices in a way that improves the local topology while keeping the meta vertices on base vertices. This also means that a projection step becomes entirely redundant since the meta vertices shouldn't (and can't) be moved to anywhere but base vertex positions in the first place.

As such, the next sections will discuss potential smoothing operations for meta mesh vertices, showcasing their strengths and weaknesses.