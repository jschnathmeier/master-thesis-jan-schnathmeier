\subsection{Forest Fire Smoothing}
\label{subsec:ForestFireSmoothing}

The first smoothing method I implemented and want to discuss here is Forest Fire Smoothing. Figure \ref{fig:forestfire1} visualizes how the method works. To start, the border of the patch around the vertex A that is to be smoothed is selected. This border is then uniformly pushed inwardly until it converges on one point; this point is then identified as the center of the patch, and vertex A is relocated there.

Since the meta mesh vertices can only be positioned on base vertices, the patch border is pushed forward step by step on base vertices. This is done by building a minheap sorted by distance from the border that initially contains all border vertices in the patch. Then, in every step, check the neighboring vertices of the top element in the heap. If the path from the current top element to the new element is the fastest, put it on top of the heap. When only one element remains in the heap, this is the base vertex furthest away from the patch border, and the meta vertex is relocated there.

\begin{minipage}[t]{0.3\textwidth}
  \def\svgwidth{\textwidth}
  \centering\raisebox{\dimexpr \topskip-\height}{\input{img/ForestFire1.pdf_tex}}
  \captionof{figure}{Forest Fire Smoothing: concept}
  \label{fig:forestfire1}
\end{minipage}\hfill
\begin{minipage}[t]{0.6\textwidth}
\vspace{-20pt}
\begin{algorithm}[H]
\caption{Forest Fire Smoothing}\label{alg:forestfire}
\begin{algorithmic}[1]
\Function{ForestFire}{$a\leftarrow$meta vertex}
\State $b \leftarrow$ target base vertex
\State $V \leftarrow$ base vertices
\State $P:=\textsc{Patch}(a)$ 
\State $M:=\textsc{MinHeap}\langle\text{vertex},\text{dist},\text{distcmp}\rangle$
\For {$v\in V$}
\State $v_{\textsc{dis}}:=\infty$
\EndFor
\For{$p \in P$} 
\State $p_{\textsc{dis}}:=0$
\State $M_{\textsc{push}}(p, p_{\textsc{dis}})$
\EndFor
\While{$\neg M_{\textsc{empty}}$}
\State $b:= M_{\textsc{first}}$
\State $N := \textsc{Neighbors}(b)$
\For {$n \in N$}
\If {$n_{\textsc{dis}}>(b_{\textsc{dis}}+|\textsc{E}(b,n)|)$}
\State $n_{\textsc{dis}}:=b_{\textsc{dis}}+|\textsc{E}(b,n)|$
\State $M_{\textsc{push}}(n,n_{\textsc{dis}})$
\EndIf
\EndFor
\State $M_{\textsc{pop}}$
\EndWhile
\State\Return $b$
\EndFunction
\end{algorithmic}
\end{algorithm}
\end{minipage}

\begin{wrapfigure}[17]{l}{0.4\textwidth}
  \vspace{-18pt}
   \def\svgwidth{0.34\textwidth}
   {\centering
   \input{img/ForestFire2.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption[width=.7\linewidth]{Forest Fire Smoothing: worst case}
  \label{fig:forestfire2}
\end{wrapfigure}

  \vspace{15pt}

Algorithm \ref{alg:forestfire} shows how Forest Fire Smoothing works in pseudo code. The input meta vertex $a$ gets relocated to the position of output base vertex $b$ afterwards.

The Forest Fire Smoothing method certainly improves vertex positions on average, however there are cases where it deteriorates them as well. Figure \ref{fig:forestfire2} showcases such an example. Specifically, when the patch around a vertex has a very irregular shape with partially concave curvature, the narrowing patch can converge towards several different areas but will then ultimately choose the point which is furthest from the border. But  in the example given in Figure \ref{fig:forestfire2} the initial position of the Vertex $A$ was certainly better located than the final position $B$.

Given the often irregular shape of meta mesh edges and patches it makes sense to look at alternative smoothing methods.
