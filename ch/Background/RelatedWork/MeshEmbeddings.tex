\subsection{Mesh Embeddings}
\label{subsec:MeshEmbeddings}

There are many algorithms and methods that embed a mesh $\mathcal{M}$ into a mesh $\mathcal{B}$. Our meta mesh embedding is one such method, but there are many different ways in which meshes are embedded into meshes, and as such it is sensible to exemplarily look at a few here.

For instance in the field of quad meshing embeddings are often used:
\begin{itemize}
\item \cite{campen2012dual}: Dual Loops Meshing is an interactive algorithm that generates quad layouts by embedding loops into meshes.
\item \cite{bommes2013integer}: Another approach makes use of embeddings for generating quad layouts based on integer-grid maps.
\item \cite{dong2006spectral}: Spectral surface quadrangulation making use of the Morse-Smale complex. The steps of the algorithm include an embedded mesh that gets optimized and later used for remeshing.
\item And many other quad meshing methods making use of embeddings such as \cite{ray2006periodic}, \cite{ebke2014level} and \cite{campen2015quantized} \dots
\end{itemize}

\begin{wrapfigure}[13]{r}{0.4\textwidth}
  \vspace{-20pt}
   \def\svgwidth{0.4\textwidth}
   {\centering
   \input{img/sorkine2002bounded2.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Partitioning and parameterization via patch growing. \cite{sorkine2002bounded}}
  \label{fig:sorkine}
\end{wrapfigure}

Note that the context of quad mesh embeddings is a slightly different one than that of our meta meshes. In quad mesh embeddings the edges are usually not explicitly embedded into the base mesh but rather represented via isolines of a parameterization on a surface $\mathcal{B}$. The embedded mesh $\mathcal{M}$ itself is also often not a starting parameter of the quadrangulation but created by an algorithm instead.

Of course, mesh embeddings are not just used in quad meshing, but also in other areas such as texture mapping. Texture mapping is a classic embedding task since it basically equates to embedding a texture mesh onto a surface mesh of an object, which usually lies in a three-dimensional space. One relevant method to look at in this context is \cite{sorkine2002bounded}. Sorkine et al. segment the input mesh by growing patches, an approach similar to our embedding initialization. Figure \ref{fig:sorkine} exemplifies this patch growing process on the surface mesh of a horse. The patch borders are then the texture seams, but if one were to interpret the patch borders as meta edges the resulting structure would correspond to a meta mesh embedding.

Specifically for meshes with texture seams -- computed via some texturing method -- there are a few processing methods such as e.g.:
\begin{itemize}
\item \cite{garland1998simplifying}: A method to simplify surfaces with colors and textures. This works via iterative edge contraction and quadratic error metrics.
\item \cite{sander2001texture}: This method partitions a mesh into charts using heuristics and creates a parameterization within each chart to minimize stretching. It then finally simplifies the mesh, optimizes the parameterization and builds a texture atlas from the charts.
\item \cite{liu2017seamless}: A method to remove texture seams and perform seam-aware mesh processing. Liu et al. start with a base mesh $\mathcal{B}$ and incrementally refine an embedding $\Phi\left(\mathcal{M}\right)$ on it via decimation while leaving $\mathcal{B}$ untouched.
\end{itemize}
One thing mesh embedding methods generally have in common is injectivity. Elements of $\mathcal{M}$ get uniquely mapped onto elements of $\mathcal{B}$ through an embedding $\Phi\left(\mathcal{M}\right)$, but $\mathcal{M}$ often has less elements than $\mathcal{B}$, so not every element in $\mathcal{B}$ has a corresponding element in $\mathcal{M}$. When a bijective map is needed, mesh embeddings can usually be expanded and become bijective surface-to-surface maps.

We avoid going into too much detail on any of these methods since they are only broadly related to our data structure. Considering the wide application of mesh embeddings, the following related works sections will narrow the scope towards methods comparable or related to our meta mesh embedding. The next Section \ref{subsec:SurfaceToSurfaceMapping} will elaborate on surface-to-surface mapping techniques which are the type of mesh embeddings most closely related to our embedding.