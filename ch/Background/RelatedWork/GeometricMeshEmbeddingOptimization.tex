\subsection{Geometric Embedding Optimization}
\label{subsec:GeometricMeshEmbeddingOptimization}

When working with mesh embeddings, much like when working with meshes, it is desirable to optimize their shape in order to make it more regular or optimize other qualities. For instance, most of the methods discussed in Section \ref{subsec:MeshEmbeddings} that create mesh embeddings can be extended to surface-to-surface maps. However, extending them like that will often introduce strong distortion. There are a variety of options when it comes to mesh optimization depending on the use-case, and the same can be said for mesh embeddings. Considering that our embedded meta mesh structure is a mesh embedding it would be advisable to find quality mesh embedding optimization methods that can be applied to it, when using it in practice. As such, this section will explore a few such mesh embedding optimization methods.

\begin{wrapfigure}[17]{r}{0.5\textwidth}
   \def\svgwidth{0.5\textwidth}
   {\centering
   {\scriptsize \input{img/Schreiner2004Inter3.pdf_tex}\par}
   }
   \vspace{-1pt}
  \caption{Vertex optimization by \cite{schreiner2004inter}}
  \label{fig:schreiner3}
\end{wrapfigure}

First, we take another look at Inter-Surface Mapping \cite{schreiner2004inter}. In Section \ref{subsec:SurfaceToSurfaceMapping} we already explored how Schreiner et al. create a surface-to-surface map by embedding the edges of two surfaces into each other. After an initial rough embedding, it is then improved iteratively and made smoother. The approach used for optimization is inspired by the previously mentioned \cite{sander2001texture} (mesh partitioning) and \cite{guskov2000normal}. In each iteration a vertex is moved within its one-ring neighborhood, decreasing its distortion metric. That same optimization is also performed for every new vertex introduced through a vertex split, as well as their neighborhoods.

Figure \ref{fig:schreiner3} shows how vertex optimization for a vertex $v$ between two connected meshes $M^1$ and $M^2$ works.\footnote{In our notation we would write $\mathcal{M}_1$ and $\mathcal{M}_2$, but that does not match Figure \ref{fig:schreiner3}.}
\begin{enumerate}
\item Parameterize the one-ring neighborhood of $v$ onto a planar polygon, see Figure \ref{fig:schreiner3}.(c).
\item Delete all edges incident to $\hat{v}$ and $\tilde{v}$ respectively.
\item Optimize the location of $\hat{v}$ using line searches \cite{sander2001texture}, while keeping $\hat{v}$ inside $N\left(\hat{v}\right)$ to preserve the surface-to-surface map; then map these changes back to $M^1$, see  Figure \ref{fig:schreiner3}.(d).
\item Retain the location of $\hat{v}$ with the lowest distortion, as long as it is better than the initial state.
\end{enumerate}

\begin{wrapfigure}[6]{r}{0.4\textwidth}
  \vspace{-20pt}
   \def\svgwidth{0.4\textwidth}
   {\centering
   {\scriptsize \input{img/Schreiner2004Inter4.pdf_tex}\par}
   }
   \vspace{-1pt}
  \caption{Map between $M^1$ and $M^2$ and morph. \cite{schreiner2004inter}}
  \label{fig:schreiner4}
\end{wrapfigure}

The authors discuss several methods of measuring distortion but decided on that of \cite{sander2001texture} since it smoothly penalizes scale distortion. Figure \ref{fig:schreiner4} shows the effect of the this optimization method in reducing distortion. Here, the edges of $M^1$ are meshed onto the surface of $M^2$, and at the close-up feature point it can be observed that distortion has been optimized. This technique can then be used to create low-distortion morphs between two meshes as also seen in Figure \ref{fig:schreiner1} previously in Section \ref{subsec:SurfaceToSurfaceMapping}.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/Campen2014Quad1.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Quad layout embedding pipeline. \cite{campen2014quad}}
  \label{fig:campen1}
\end{figure}

\begin{wrapfigure}[13]{r}{0.5\textwidth}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/Campen2014Quad2.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Node optimization with gradient descent \cite{campen2014quad}}
  \label{fig:campen2}
\end{wrapfigure}

The next mesh embedding optimization method we take a look at is ''Quad Layout Embedding via Aligned Parameterization'' \cite{campen2014quad}. Figure \ref{fig:campen1} shows a rough outline of the steps of the algorithm.

The detailed steps of the Quad Layout Embedding are not so important here, but the setup is quite interesting. The embedded quad layout here corresponds to our meta mesh $\mathcal{M}$, in that the goal is to embed it into an underlying mesh $\mathcal{B}$ in a similar way. This parameterization is then optimized in a few steps in order to contain the structure of $\mathcal{M}$, but align to the curvature of $\mathcal{B}$.

Since the topic of this section is geometric mesh embedding optimization, we are particularly interested in the later steps of the method, and skip the cross-field generation. Up until this point, the embedding of arcs and patches has been optimized, but nodes remained restricted. Node optimization then happens guided by gradient descent. Figure \ref{fig:campen2}.(a) shows the trajectory of a node moving towards a solution with low residual; this eliminates distortions and inversions. Similary, Figure \ref{fig:campen2}.(b) visualizes the descent directions with the negative gradient \textbf{d} in yellow (direction the node moves in) and the estimator in red; deviation between those is usually low.

The nodes of the embedding are then iteratively improved in this fashion. Since they should lie on base vertices which are discrete, and snapping a node to the nearest vertex may hamper convergence, the underlying base vertices are simply relocated along with the nodes gradient descent. As soon as the node no longer lies on the vertex or its surrounding patch, the vertex is snapped back to its original position in order not to change the underlying mesh. At last, after the node positions have been optimized, a well refined quad mesh is generated.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/Schmidt2019Surface1.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Surface-to-surface map optimization. \cite{schmidt2019distortion}}
  \label{fig:schmidt1}
\end{figure}

Lastly, there is the paper ''Distortion-Minimizing Injective Maps Between Surfaces'' by \cite{schmidt2019distortion}. Figure \ref{fig:schmidt1} shows a parameterization of two meshes on the left, mapping onto one another in the middle, and distortion minimization on the right. This already reveals the parallels to the previously discussed \cite{schreiner2004inter}, a work upon which this method builds as well. The aim is to generate a map between two (parts of) disk-topology surfaces, and to minimize distortion on that map.

Given two disc-topology input meshes $\mathcal{B}_1$ and $\mathcal{B}_2$, they are parameterized and overlapped. Through this overlapping, a map between $\mathcal{B}_1$ and $\mathcal{B}_2$ is created, similar to the method used in inter-surface mapping. The edges $E^{\mathcal{B}_1}$ are mapped onto $\mathcal{B}_2$ and vice versa based on a parameter domain. Cuts between $\mathcal{B}_1$ and $\mathcal{B}_2$ are calculated and considered in the mapping. Finally, the shape of the mapping is changed to fit on a plane; this minimizes distortion of the mapped faces.

%Figure \ref{fig:schmidt3} visualizes how embeddings work using the method of Schmidt et al. Given input meshes $\mathcal{A}$ and $\mathcal{B}$, an inter-surface map $\Phi(\mathcal{A}):\mathcal{A}\rightarrow\mathcal{B}$ is created by embedding the connectivity of $\mathcal{A}$ into the surface of $\mathcal{B}$. Lastly $\mathcal{M}_{\mathcal{B}}$ on the right is the meta mesh, which contains the vertices of both $\mathcal{A}$ and $\mathcal{B}$ and an additional set of intersection vertices where edges of $\mathcal{A}$ and $\mathcal{B}$ meet.

%Whereas \cite{schreiner2004inter} optimizes distortion locally, \cite{schmidt2019distortion} use a global distortion optimization metric.  Here, distortion is measured isometrically, since isometric measures are more sensitive towards misaligned geometric features; Figure \ref{fig:schmidt2} shows a comparison of different energies. Those energies are symmetric Dirichlet (SD) \cite{schreiner2004inter}, as-rigid-as-possible (ARAP) \cite{liu2008local}, symmetric ARAP (SARAP) \cite{poranne2016simple} and lastly as-similar-as-possible (ASAP). Schmidt et al. use the symmetric Dirichlet energy to measure distortion.

%In the implementation, the algorithm computes a 2D mesh overlay of meshes $\mathcal{A}$ and $\mathcal{B}$. Then, it calculates gradients for each vertex, and Hessian matrices using automatic differentiation. Specific feature vertices are picked out as landmark vertices, in order to constrain those points while optimizing the rest.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/bischoff2005snakes.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Snake representation (a), invalid snaxels (b), removed in cleaning (c) \cite{bischoff2005snakes}.}
  \label{fig:bischoff2005snakes}
\end{figure}

The three methods presented in this section so far take surface-to-surface maps and geometrically improve them. Conversely, there are also methods that take a mesh embedding as an input and improve it directly without having to compute a surface-to-surface map first. One such method is ''Snakes on triangle meshes'' \cite{bischoff2005snakes} represented in Figure \ref{fig:bischoff2005snakes}.

This method represents curves (also called snakes) on surfaces, and evolves them according to their curvature to become the locally shortest curves. These curves are essentially embedded edges, comparable to our set of embedded edges $\Phi\left(E^{\mathcal{M}}\right)$. Applications include computing object diameters, mesh partitioning and mesh editing. Of interest to us is the representation of the snakes on their underlying mesh $\mathcal{B}$ defined by the following embedding:

\begin{itemize}
\item A snake vertex $\Phi\left(v^{\mathcal{M}}_x\right)$ is called a \textit{snaxel}, and is embedded into an edge $e^\mathcal{B}_i$ of the underlying mesh $\mathcal{B}$.
\item The segments of the snakes, or edges $e^\mathcal{M}_i$ have to lie in the interior of triangles $f^\mathcal{B}_j$; meaning they split those triangles between two \textit{snaxels} $\Phi\left(v^{\mathcal{M}}_k\right)$, $\Phi\left(v^{\mathcal{M}}_l\right)$.
\item \textit{Snaxels} are oriented, so they best correspond to meta halfedges $e^\mathcal{M}_i$.
\end{itemize}

The way in which these snakes are embedded is particularly interesting, since it it shares some similarities to our approach. Given an embedding of snakes, they are continually evolved to become shorter until their length reaches a local minimum; see Figure \ref{fig:bischoff2005snakes}. This is a form of geometric embedding optimization on a mesh embedding and does the following:

\begin{enumerate}
\item Snaxels are moved forward in time along their supporting edges. When multiple snaxels coincide on one vertex, that vertex is split to accomodate all of them. If splits result in inconsistencies, the underlying mesh is cleaned up as seen in Figure \ref{fig:bischoff2005snakes}.(b-c).
\end{enumerate}


































