\subsection{Surface-to-Surface Mapping}
\label{subsec:SurfaceToSurfaceMapping}

Our goal is creating an embedding $\Phi\left(\mathcal{M}\right)$ of a meta mesh $\mathcal{M}$ onto a base mesh $\mathcal{B}$. Given an injective mapping, every meta vertex $v^{\mathcal{M}}_i\in V^\mathcal{M}$ corresponds to exactly one $v^{\mathcal{B}}_j\in V^\mathcal{B}$ such that $\Phi\left(v^{\mathcal{M}}_i\right)=v^{\mathcal{B}}_j$. Meta edges $e^{\mathcal{M}}_i\in E^\mathcal{M}$ on the other hand, are embedded as sequences of base edges such as $\Phi\left(e^{\mathcal{M}}_{A,D}\right):=\left\{e^\mathcal{B}_{A,B},e^\mathcal{B}_{B,C},e^\mathcal{B}_{C,D}\right\}$. Some of the surface-to-surface mapping methods presented in this sections use structures that resemble this embedding in different ways. For example, the way in which edges of a cut-graph are inserted into surface meshes $\mathcal{B}_i$ is often the same way we embed edges $\Phi\left(e^{\mathcal{M}}_i\right)\in\Phi\left(E^\mathcal{M}\right)$. This section aims to highlight similarities between a few select surface-to-surface maps and our mesh embedding.

To start off it is important to understand the difference between \textit{surface-to-surface maps} and \textit{mesh embeddings}. 
\begin{itemize}
\item A \textbf{surface-to-surface map} or \textbf{inter-surface map} denotes a (continuous) bijection $\Psi$ between two surfaces $\mathcal{B}$ and $\mathcal{M}$. This means that for each point on $\mathcal{B}$ there exists a point on $\mathcal{M}$, bijectively mapped as defined by  $\Psi$ and vice versa.
\item A \textbf{mesh embedding} denotes an injective mapping $\Phi\left(\mathcal{M}\right)$ of a meta mesh $\mathcal{M}$ onto a base mesh $\mathcal{B}$ so that every vertex of $\mathcal{M}$ and every edge of $\mathcal{M}$ are mapped onto elements of $\mathcal{B}$. Note that while $\Phi$ maps every vertex and edge of $\mathcal{M}$ onto $\mathcal{B}$, not every element of $\mathcal{B}$ has to be connected to an element of $\mathcal{M}$. This makes a \textit{surface-to-surface map} a special bidirectional \textit{mesh embedding}. \textit{Mesh embeddings} can generally be extended until their injection becomes a bijection, and they turn into \textit{surface-to-surface maps}. One way to do this is by parameterizing the faces of $\Phi\left(\mathcal{M}\right)$ until the \textit{surface-to-surface map} is complete.
\end{itemize}

The following methods create a surface-to-surface map via mesh segmentation.

\begin{figure}[ht]
  \vspace{-10pt}
\begin{centering}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/praun2001consistent2.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Example boundary inconsistencies resulting from naive tracing.}
  \label{fig:praun2}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/praun2001consistent1.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Patch boundary curves from the star connectivity.}
  \label{fig:praun1}
\end{subfigure}
\end{centering}
  \vspace{-10pt}
 \caption{Figures from \cite{praun2001consistent}}
 \label{fig:praun}
\end{figure}

The first surface-to-surface mapping method we will look at, \cite{praun2001consistent}, calculates consistent mesh parameterizations between a set of input meshes. The method starts with a selection of feature points, selected either by hand or via an algorithm. These feature points need to be properly connected to form a consistent mesh; naively tracing boundaries can lead to problems such as edges intersecting or cyclic edge order being broken\footnote{When looking at a vertex $v_x$ projected onto a 2D surface, the outgoing halfedges $h_i$ have a clear order relative to one another. In a halfedge datastructure they can be iterated over in that order by using the \textit{next\_halfedge} and \textit{opposite\_halfedge} pointers. More on the halfedge data structure in Section \ref{sec:HalfedgeDataStructure}. This cyclical order is important when traversing over halfedge data structures in a deterministic way.}. Figure \ref{fig:praun2} highlights this issue. The problem of tracing embedded edges in the correct order is particularly interesting, because it also emerges when tracing edges of our meta mesh. Praun et al. handle this by tracing edges that create a minimal spanning tree embedding first, and closing loops afterwards.

Figure \ref{fig:praun1} shows the mapping of a simple star layout onto a horse using naive curve-tracing on the left, and the improved spanning tree method on the right. Whereas the left side shows overlapping edges, no edge overlaps happen after using the improved method. What's interesting about this method is that the meta mesh is given as an input -- see the star in Figure \ref{fig:praun1} in the middle between the horses -- and then mapped onto the input surface mesh.

\begin{figure}[ht]
  \vspace{-10pt}
\begin{centering}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/Kraevoy2004Cross1.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Examples for invalid paths. (a): blue path reaching vertex i from different sides - (b-c): red vertex placed in different patches.}
  \label{fig:kraevoy1}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/Kraevoy2004Cross2.pdf_tex}\par
   }
  \vspace{-1pt}
  \caption[width=.9\linewidth]{Base domain construction for genus 3 models.}
  \label{fig:kraevoy2}
\end{subfigure}
\end{centering}
  \vspace{-10pt}
 \caption{Figures from \cite{kraevoy2004cross}}
 \label{fig:kraevoy}
\end{figure}

\begin{wrapfigure}[17]{r}{0.5\textwidth}
  \vspace{-30pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/Kraevoy2004Cross3.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Base Domain Construction, (a-b): edge paths, (c-d): face paths, (e-f): base domains \cite{kraevoy2004cross}}
  \label{fig:kraevoy3}
\end{wrapfigure}

The next method we take a look at is ''Cross-Parameterization and Compatible Remeshing of 3D Models'' by \cite{kraevoy2004cross}. Much like other surface-to-surface mapping methods, it takes two meshes with some matching features as an input and calculates a cross-parameterization between them based on those features. However unlike the previous \cite{praun2001consistent}, the method of Kraevoy et al. does not take a meta mesh as an input along with meshes but rather generates one incrementally. Note that in \cite{kraevoy2004cross} what we call meta mesh is called a \textit{base domain}. This algorithm works in three steps:

\textbf{Common base domain construction}: The first part is also the one most relevant to our work. In this step, a common base domain is constructed iteratively by adding edges step by step. These edges must have matching start and end vertices in both input meshes -- this creates a bijection -- and fit a set of criteria in order not to block each other. The rules for edge adding are similar to those of \cite{praun2001consistent} in a sense, since they both aim to find a good embedding on multiple meshes. Specifically, edges may not intersect, their cyclic order must be preserved, and future paths may not be blocked. Figure \ref{fig:kraevoy1} exemplifies why these rules are necessary by showing what can happen if they are not properly applied.

The way in which Kraevoy et al. go about adding edges follows a different approach than \cite{praun2001consistent}. First, as many edges as possible are added without modifying the connectivity of the source and target meshes. This is done by adding the shortest paths between each pair of vertices that don't block others. Whenever a new addition creates overlapping edges, or edges tracing through the wrong patches, those edges are updated and recomputed. This results in a base domain as can be seen in Figure \ref{fig:kraevoy3}.(a-b). The second step traces the remaining edges, by iterating over faces instead of edges to ensure connectivity is possible. When a path has been found, necessary splits on the underlying meshes are performed- and those paths added too -- see Figure \ref{fig:kraevoy3}.(c-d). The last part of the Figure \ref{fig:kraevoy3}.(e-f) shows the underlying base domains.

The remaining two steps of the algorithm are cross-parameterization and compatible remeshing, based on the common base domain constructed in the first step. Since the focus here is on the embedding, we won't go in detail on those parts here.

This method even works on complicated meshes of higher genus, such as the ones that can be seen in Figure \ref{fig:kraevoy2}. The way in which edges are traced over faces is also especially interesting and will become relevant later, to compare with our tracing methods for meta edges.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/Schreiner2004Inter1.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Inter-surface mapping and morph. \cite{schreiner2004inter}}
  \label{fig:schreiner1}
\end{figure}

Then there is Inter-Surface Mapping \cite{schreiner2004inter}, which builds upon the previously discussed method of \cite{kraevoy2004cross} in the initial steps of the algorithm. The goal of this method is to produce a map between two triangulated surfaces, without needing to parameterize over an intermediate domain first. This works by creating a mutual tessellation \cite{turk1992re} of the two surfaces, meaning the map will be finer than the original mesh faces. This can be seen in Figure \ref{fig:schreiner1}.(a-b). Here the left surfaces has edges from the right surface inserted into it and vice versa for the right surface. Figure \ref{fig:schreiner1}.(c) is the surface from (b) with the normals of (a) mapped onto it, and (d) shows a 50\% morph between the two surfaces.

This works via a progressive refinement which goes as follows for input surfaces $\mathcal{B}_1$ and $\mathcal{B}_2$:
\begin{enumerate}
\item Partition $\mathcal{B}_1$ and $\mathcal{B}_2$ into a corresponding set of triangular patches, by iteratively tracing a set of corresponding paths.
\item Progressively decimate $\mathcal{B}_1$ and $\mathcal{B}_2$, resulting in two \textit{meta meshes}\footnote{Confusing notation: What \cite{schreiner2004inter} calls a \textit{base mesh} is what we call a \textit{meta mesh}; here we stick to our nomenclature for continuity.} with identical connectivity. Figure \ref{fig:schreiner2} shows those \textit{base meshes} embedded into $\mathcal{B}_1$ and $\mathcal{B}_2$ (left) and in a more explicit representation as independent meshes (right).
\item $\mathcal{B}_1$ and $\mathcal{B}_2$ have now been transformed into identical meshes. This implicitly defines an inter-surface map $\mathcal{M}$.
\item Refine the inter-surface map $\mathcal{M}$ iteratively.
\end{enumerate}

Here we are mainly interested in steps one and two, since mesh embeddings are most relevant to this work.

\begin{wrapfigure}[15]{r}{0.5\textwidth}
  \vspace{-15pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/Schreiner2004Inter2.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{\textit{Base meshes} represented as implicit embeddings as well as explicit meshes. \cite{schreiner2004inter}}
  \label{fig:schreiner2}
\end{wrapfigure}

Since the goal is partitioning $\mathcal{B}_1$ and $\mathcal{B}_2$, patch boundaries have to be determined first. Patch boundaries are computed based on feature vertices, which can be either input by the user or automatically computed. The way in which Schreiner et al. find feature pairs on $\mathcal{B}_1$ and $\mathcal{B}_2$ is similar in spirit to the previously discussed methods of \cite{praun2001consistent} and \cite{kraevoy2004cross}.

Given pairs of feature vertices, the shortest path between them is traced using Dijkstra's algorithm \cite{dijkstra1959note}, with constraints to not intersect with already traced paths. In order to ensure connectivity, it is also vital to start the search from the correct sectors (correct starting positions in relation to already traced edges). Paths are traced greedily while making sure that a spanning tree of the feature vertices is built first, as suggested by \cite{praun2001consistent}. This guarantees that all paths can be inserted successfully. A few special cases have to be addressed though:
\begin{itemize}
\item \textbf{Higher genus surfaces}: Surfaces of high genus are handled by inserting additional feature points to ensure disc topology of each patch.
\item \textbf{Avoiding separating cycles}:  When an inserted path forms a cycle, perform a search to ensure the cycle does not block other feature points; if it does replace the path with one forming a non-separating cycle, using an algorithm similar to that of \cite{lazarus2001computing}.
\item \textbf{Avoiding swirls}: At certain vertices the outgoing edges can form an awkward geometric shape and longer than necessary edges. This can be addressed heuristically by connecting far off vertices first, and by considering edges that trace through the wrong sectors last. Swirls are also a problem in some embedded meta mesh configurations, however they appear in a different context, where these heuristics are not applicable.
\item \textbf{Handling boundaries}: Schreiner et al. handle boundaries by triangulating them using a central point, computing the meshes as normal, and then removing those boundary-center vertices again.
\end{itemize}
Many of these considerations are relevant to problems that reappear later during the triangulation and refinement of our embedded meta meshes. Note that \cite{schreiner2004inter} differentiates itself from the previously mentioned \cite{praun2001consistent} and \cite{kraevoy2004cross} in that it incrementally modifies its meta mesh via decimation. This is an approach that we will also attempt with our data structure, along the lines of an Incremental Isotropic Remeshing algorithm as described in Section \ref{subsec:IncrementalIsotropicRemeshing}.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/Aigerman2014Lifted.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Lifted Bijections algorithm, steps a) to c), source \cite{aigerman2014lifted}}
  \label{fig:aigerman1}
\end{figure}

Lastly, Aigerman et al. \cite{aigerman2014lifted} present a surface-to-surface mapping method using a cut-graph. Their method works in three steps, as follows, where the steps described below correspond to the steps in Figure \ref{fig:aigerman1}.
\begin{enumerate}[label=(\alph*)]
\item \textbf{Cutting the two meshes to disks}: The first step of the algorithm is the most interesting part. Later steps require the two input meshes to be flattened, and flattening requires cutting them into disk-topology first. These cuts happen by calculating a minimal spanning tree between feature vertices, also known as the \textit{cut-graph}. The edges of this cut-graph consist of sequences of edges on the mesh the cut graph is embedded into, which corresponds to the way in which we embed meta edges in our data structure. Figure \ref{fig:aigerman1}.(a) especially visualizes how the cut-graph's edges are embedded into the base surface mesh.
\item \textbf{Computing the joint flattenings}: In the second step, the cut meshes are computationally flattened -- see Figure \ref{fig:aigerman1}.(b).
\item \textbf{Bijection Lifting}: Lastly, a bijection between the flattened meshes is computed iteratively. This bijection, combined with the cut and flattening, can then become a bijection between the two original meshes, and that is the resulting surface-to-surface map -- see Figure \ref{fig:aigerman1}.(c).
\end{enumerate}
In essence, \cite{aigerman2014lifted} segments the input meshes by computing a \textit{cut-graph}, and cuts the entire mesh down to one disc. This is a major difference between \cite{aigerman2014lifted} and the previously mentioned \cite{praun2001consistent, kraevoy2004cross, schreiner2004inter} which cut the input mesh down to multiple discs.

The shared goal of these four methods described in this section is finding a surface-to-surface map between two (or more) input meshes $\mathcal{B}_i$. This task can be substituted by using our meta mesh embedding as follows:
\begin{enumerate}
\item For a set of input base meshes $\left\{\mathcal{B}_i|i\in\mathbb{N}\right\}$ find a common meta mesh $\mathcal{M}$ and embed it into each base mesh $\mathcal{B}_j\in\left\{\mathcal{B}_i|i\in\mathbb{N}\right\}$ in the form $\Phi_j\left(\mathcal{M}\right)$.
\item Extend the mesh embeddings $\Phi_j\left(\mathcal{M}\right)$ to full surface-to-surface maps,
\item Combine those surface-to-surface maps $\Phi_j\left(\mathcal{M}\right)$ via inversion and composition to reach a surface-to-surface map $\Phi_{i,j}\left(\mathcal{M}\right)$ for each pair of base meshes $\mathcal{B}_i, \mathcal{B}_j\in\left\{\mathcal{B}_i|i\in\mathbb{N}\right\}$.
\end{enumerate}
By following these steps, our embedded meta mesh structure can be expanded to compute surface-to-surface maps.
