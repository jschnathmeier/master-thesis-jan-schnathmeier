\subsection{Path Embeddings}
\label{subsec:PathEmbeddings}

When embedding a mesh $\mathcal{M}$ into a mesh $\mathcal{B}$ there is an important design choice to be made, in how the edges of $\mathcal{M}$ should be embedded into $\mathcal{B}$. Methods can differ in what elements of $\mathcal{B}$ (faces / edges) edges of $\mathcal{M}$ can lie on, but also in whether edges are allowed to overlap. In the previous Section \ref{subsec:SurfaceToSurfaceMapping} we discussed a few methods using different sorts of edge embeddings. 

\begin{wrapfigure}[10]{r}{0.5\textwidth}
  \vspace{-15pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/TracingSpaceProblem.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Tracing between $v_A$ and $v_B$; possible (left) - blocked (right)}
  \label{fig:tracingspaceproblem}
\end{wrapfigure}

The previously introduced cross-parameterization by Kraevoy et al. \cite{kraevoy2004cross} uses an interesting type of path embedding. Due to the initialization, tracing edges aways happens inside a simple surface of disc topology. However this type of tracing can run into issues depending on the submesh that the trace is performed in. Figure \ref{fig:tracingspaceproblem} illustrates this as follows: On the left a successful trace between points A and B has been performed (dotted blue line), but on the right performing such a trace is not as simple, if we assume that the trace has to run over edges of the underlying base mesh (light gray edges). In order to trace a path between A and B on the right example we would have to either trace over faces of the base mesh, or make changes on the underlying base mesh, otherwise B cannot be connected with C, and thus cannot reach A either. This is a reoccuring problem that also has to be addressed in our implementation -- here is how Kraevoy et al. approach it.

\begin{wrapfigure}[8]{r}{0.5\textwidth}
  \vspace{-10pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/Kraevoy2004Cross4.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Path embeddings being traced over faces. \cite{kraevoy2004cross}}
  \label{fig:kraevoy4}
\end{wrapfigure}

Since tracing over edges can lead to situations where the path is ''blocked'', Kraevoy et al. trace over faces instead. They make use of the following property: for any two vertices on the border of a disc shaped mesh there has to be a sequence of faces that connectes those two points. 

\begin{wrapfigure}[12]{r}{0.35\textwidth}
  \vspace{-20pt}
   \def\svgwidth{0.35\textwidth}
   {\centering
   \input{img/TracingSpaceProblem2.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Tracing between A and B is blocked; trace over occupied edge.}
  \label{fig:tracingspaceproblem2}
\end{wrapfigure}

As such, tracing over faces sidesteps these concerns in an elegant matter and makes tracing inside a closed sector easy. Figure \ref{fig:kraevoy4}.(a) shows such a face trace between two vertices that would be blocked when naively tracing over edges.

However, since Kraevoy et al. want to embed meta edges on base edges and not base faces, the tracing over faces is only the first step in their path embedding method. After that trace, a series of edge splits have to be performed on the base mesh where sector borders or meta edges block off the path around the face trace, as can be seen in Figure \ref{fig:kraevoy4}.(b). These splits then allow embedding a meta edge on base edges, as shown in Figure \ref{fig:kraevoy4}.(c).

Another previously discussed approach is ''Consistent Mesh Parameterization'' \cite{praun2001consistent}. As already mentioned in Section \ref{subsec:SurfaceToSurfaceMapping}, Praun et al. handle tracing order by creating a minimal spanning tree over the meta vertices first, in order to avoid edges being blocked off. However, the problem seen in Figure \ref{fig:tracingspaceproblem} can still occur since the aim here is also to embed a meta edge onto base edges. This underlying problem is initially circumvented, by allowing multiple meta edges to share some base edges, as illustrated in Figure \ref{fig:tracingspaceproblem2}.

However, that is only a temporary solution, since allowing the embedding to persist in this form would overlap meta edges and thus lose the injectivity of the map. Thus, these duplicate edges are only traced tentatively, and then sorted by length in a priority queue. The longer an edge is the earlier it will be traced, reducing the average edge length (since tracing later might increase their length due to obstacles). In this second tracing pass overlapping edges are no longer allowed, and tentative edges that violate this are retraced. Finally, the edges are straightened, and base edge splits are performed where necessary to ensure connectivity .

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   {\tiny \input{img/Livesu2020Obtaining1.pdf_tex}\par}
   }
  \caption{Prying apart edges $l_i$ and $l_j$ which meet at vertex $v_m$. \cite{livesu2020obtaining}}
  \label{fig:livesu1}
\end{figure}

Allowing overlapping paths certainly is a very relaxed constraint that simplifies edge embedding. Another method from Livesu et al. \cite{livesu2020obtaining} also makes use of this idea, but in a different way than the previous \cite{praun2001consistent}. Where Praun et al. only allowed overlapping edges tentatively to measure lengths, Livesu et al. fully embed all meta edges into the base mesh while allowing overlapping. Those edges are then pried apart in the next step. 

\begin{wrapfigure}[7]{r}{0.5\textwidth}
  \vspace{-25pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   {\tiny \input{img/Livesu2020Obtaining2.pdf_tex}\par}
   }
   \vspace{-1pt}
  \caption{Triangle split to pry apart edges $l_i$ and $l_j$. \cite{livesu2020obtaining}}
  \label{fig:livesu2}
\end{wrapfigure}

Figure \ref{fig:livesu1} shows different ways of prying a patch where two meta edges converge apart, via an edge split or a vertex split. Figure \ref{fig:livesu2} shows a triangle split. By iteratively applying splits on patches where overlapping meta edges meet, they are eventually completely separated. Livesu et al. present and compare three different splitting methods for this purpose:

\begin{enumerate}
\item \textbf{Edge split}: As pictured in Figure \ref{fig:livesu1} in the middle, refining via edge splits preserves the input geometry but increases the amount of mesh elements the most, compared to the other two methods.
\item \textbf{Vertex split}: As can be seen in the right part of Figure \ref{fig:livesu1}, a vertex split creates fewer new mesh elements than an edge split, but does not preserve the original geometry if the faces of the fan are not coplanar.
\item \textbf{Triangle split}: Figure \ref{fig:livesu2} shows a special case where one of the triangle fans is comprised of only one face. In that case, a face split of that face suffices.
\end{enumerate}
Given the strengths and weaknesses of these split operations, Livesu et al. propose a combined method using either of these options situationally:
\begin{enumerate}[resume]
\item \textbf{Hybrid split}: First, check for planarity of either face fan, if either is planar perform a vertex split on that side; note that a triangle split is equivalent to a vertex split on a size 1 fan, and is always planar. If neither side fan is planar, perform an edge split instead.
\end{enumerate}

\begin{wrapfigure}[11]{r}{0.5\textwidth}
  \vspace{-20pt}
   \def\svgwidth{0.5\textwidth}
   {\centering
   \input{img/Lazarus2001Computing1.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{The loops data structure proposed by \cite{lazarus2001computing}. Base mesh in black, loops in red.}
  \label{fig:lazarus1}
\end{wrapfigure}

Whereas the methods presented so far embed meta edges on base edges and perform different forms of refinement on the underlying base mesh, there are also other ways to embed a meta edge into a base mesh. For instance Lazarus et al. \cite{lazarus2001computing} propose what they call the \textit{loops datastructure}. In this data structure, embedded loops are allowed to run across rather than along edges of the underlying mesh. Figure \ref{fig:lazarus1} visualizes how embedded loops can be embedded into the mesh in such a way, by effectively partitioning base edges as can be seen in the right part of the figure.

This approach easily solves the space problem shown in Figure \ref{fig:tracingspaceproblem} and discussed earlier in this section. It shares a certain similarity with the approach of Kraevoy et al. where they initially traced over faces. Albeit this way the original geometry is preserved, and no changes have to be made on the underlying base mesh itself, this type of embedding requires a complicated secondary datastructure on top of the existing mesh datastructure. Embedding meta edges on base edges is probably preferable in most applications in order to keep the data structure simple. When performing subsequent mesh processing it would be cumbersome to have to deal with two separate mesh datastructures.

\begin{figure}[ht]
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/Sharp2019Navigating1.pdf_tex}\par
   }
  \caption{Crude input meshes (gray) represented as high-quality intrinsic triangulations (colored). \cite{sharp2019navigating}}
  \label{fig:sharp1}
\end{figure}

Lastly, \cite{sharp2019navigating} presents a data structure that enables the user to treat a crude input mesh as a high-quality intrinsic triangulation while preserving original geometry as shown in Figure \ref{fig:sharp1}. Creating a bijection - a surface-to-surface map - between the original mesh and a geometrically identical Delaunay triangulation allows the application of mesh algorithms on the underlying crude mesh through the bijection. Sharp et al. realize this embedding through a special data structure.

\begin{wrapfigure}[11]{r}{0.4\textwidth}
  \vspace{-10pt}
   \def\svgwidth{0.4\textwidth}
   {\centering
   \input{img/Sharp2019Navigating2.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Signpost data structure. \cite{sharp2019navigating}}
  \label{fig:sharp2}
\end{wrapfigure}

On top of a traditional mesh data structure, a \textit{signpost data structure} is built. This signpost data structure stores the direction and distance to each neighbor for every vertex of the original mesh. Figure \ref{fig:sharp2} is an apt visual metaphor for this signpost data strucutre. By storing data on vertices as described, it then becomes possible to modify the connectivity of the mesh implicitly and run a Delaunay triangulation over the original set of vertices while retaining the original connectivity and geometry. The idea is to store the new triangulation intrinsically, allowing for manipulation of edges without affecting the original geometry. Inside the triangles of the original mesh, \textit{intrinsic} triangles are stored.

\begin{wrapfigure}[9]{l}{0.25\textwidth}
   \def\svgwidth{0.25\textwidth}
   {\centering
   {\scriptsize \input{img/Sharp2019Navigating3.pdf_tex}\par}
   }
   \vspace{-1pt}
  \caption{Extrinsic vs intrinsic flips. \cite{sharp2019navigating}}
  \label{fig:sharp3}
\end{wrapfigure}

\textit{Intrinsic} in this context means that all information about new connectivity is stored in the signpost data structure, and thus the underlying surface need not be changed \textit{extrinsically}, see Figure \ref{fig:sharp3}. The paper then describes how to create and navigate such an intrinsic Delaunay triangulation; detailing that would be beyond the scope of this work.

The important parallel to our embedded meta mesh structure to take away from this is the signpost data structure allowing for such an intrinsic embedding. Given such data structures, it becomes trivially easy to embed edges of a meta mesh by representing them entirely intrinsically. Of course, similar to the previously introduced \cite{lazarus2001computing}, the downside of using such a structure is the overhead in implementing an additional data structure such as the signpost data structure. That may definitely be worth the effort for certain applications, but if the goal is to embed meta edges onto edges of an underlying base mesh this approach is obviously not suitable.

Secondly, the intrinsic representation does not permit placing original vertices inside intrinsic triangles - a triangle with an original vertex inside it would not have to be flat anymore, and thus be unsuitable for the intrinsic datastructure. This is a heavy restriction that makes this datastructure unable to represent a variety of embeddings which other methods such as ours can represent.\footnote{Specifically, meta faces with Gaussian curvature could not be represented intrinsically.}

Given the differences in the methods for embedding paths described in this section, it is now clear that there is a variety of ways to approach this problem. Embedding meta edges in our data structure certainly faces some similar challenges.
