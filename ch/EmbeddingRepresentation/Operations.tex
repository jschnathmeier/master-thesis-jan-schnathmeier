\section{Operations}
\label{sec:Operations}

Since we operate on a halfedge data structure \cite{kettner1998designing}, we require a sensible set of operations that allow the implementation of algorithms that modify the structure and connectivity of $\mathcal{M}$ on our data structure. For conventional orientable 2-manifold meshes, a minimal and complete set of operations would be the following \cite{akleman2003minimal}:
\begin{enumerate}
\item \textbf{Vertex insertion}: Inserts a new meta vertex $v^\mathcal{M}_x$ into the meta mesh $\mathcal{M}$. Whereas in a conventional mesh this insertion could happen anywhere, we need to restrict it to the positions of base vertices $V^\mathcal{B}$.
\item \textbf{Vertex deletion}: Deletes a meta vertex $v^\mathcal{M}_x$.
\item \textbf{Edge insertion}: Inserts a meta edge $e^\mathcal{M}_x$. In conventional meshes edges are straight lines between the two vertices they connect, but a meta edge can take much more complex shapes due to it being a concatenation of base edges. Here we need to be careful to preserve cyclical edge order and to not cross edges. Finding a set of connecting base edges between two verices, while using neighboring meta edges as restrictions, can be done via the Dijkstra \cite{dijkstra1959note} or A* \cite{hart1968formal} tracing algorithms.
\item \textbf{Edge deletion}: Deletes a meta edge $e^\mathcal{M}_x$.
\end{enumerate}
All of those operations are sensible to have and need to be part of our implementation to make it workable, with edge insertion being especially interesting since meta halfedges $e^\mathcal{M}_i$ are embedded as sequences of base edges $e^\mathcal{B}_j$; but more on that in Section \ref{subsec:RestrictedPathTracing}, where we discuss the implementation in detail. As Akleman et al. show, this set of operations is minimal and complete, but in practice algorithms on meshes can be built better with an extended set of operations. 

Whereas the previous set of operations generates and changes meshes, we need an additional set of operations to be used by mesh algorithms. Changing a mesh $\mathcal{M}$ via the abovementioned operations runs the risk of leaving the context of a valid mesh embedding and can also affect mesh topology. For mesh algorithms we prefer a stable set of operations that cannot change mesh topology and keeps the embedding valid, as this also makes those algorithms more stable. For that purpose, we propose a set of four operations we call \textit{atomic mesh operations}.

\begin{figure}[ht]
\begin{centering}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/ConceptCollapse.pdf_tex}\par
   }
  \caption[width=.9\linewidth]{Halfedge collapse of $h^\mathcal{M}_{A,B}$ halfedge.}
  \label{fig:ConceptCollapse}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/ConceptRotation.pdf_tex}\par
   }
  \caption[width=.9\linewidth]{Edge rotation of $e^\mathcal{M}_{A,B}$ edge.}
  \label{fig:ConceptRotation}
\end{subfigure}
\end{centering}
\end{figure}

\begin{enumerate}
\item \textbf{Halfedge collapse}: In this operation a halfedge $h^\mathcal{M}_{A,B}$ is removed from the mesh, and all vertices connected to vertex $v^\mathcal{M}_{A}$ are then connected to vertex $v^\mathcal{M}_{B}$. Afterwards, loops are removed to avoid double edges; see Figure \ref{fig:ConceptCollapse} for reference.
\item \textbf{Edge rotation}: The input is an edge $e^\mathcal{M}_{A,B}$, which is removed and then retraced by walking along the sector around it counter-clockwise (halfedge direction) from $v^\mathcal{M}_{A}$ and $v^\mathcal{M}_{B}$ and picking the next vertices respectively. See Figure \ref{fig:ConceptRotation}. \footnote{In a trimesh this corresponds to flipping the edge $e^\mathcal{M}_{A,B}$, and in most of our applications this is the case.}.
\end{enumerate}

\begin{figure}[ht]
\begin{centering}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/ConceptEdgeSplit.pdf_tex}\par
   }
  \caption[width=.9\linewidth]{Edge split of $e^\mathcal{M}_{A,B}$ edge on vertex $v^\mathcal{M}_{C}$.}
  \label{fig:ConceptEdgeSplit}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/ConceptFaceSplit.pdf_tex}\par
   }
  \caption[width=.9\linewidth]{Face split into vertex $v^\mathcal{M}_{B}$.}
  \label{fig:ConceptFaceSplit}
\end{subfigure}
\end{centering}
\end{figure}

\begin{enumerate}[resume]
\item \textbf{Edge split}: For an input edge $e^\mathcal{M}_{A,B}$, insert a new meta vertex $v^\mathcal{M}_{C}$ on $e^\mathcal{M}_{A,B}$ and connect it to all vertices in the adjacent faces of $e^\mathcal{M}_{A,B}$. Note that $v^\mathcal{M}_{C}$ cannot be chosen arbitrarily, but has to lie on the base mesh $\mathcal{B}$. See Figure \ref{fig:ConceptEdgeSplit}.
\item \textbf{Face split}: For an input base vertex $v^\mathcal{B}_{A}$, create a meta vertex $v^\mathcal{M}_{B}$ and connect it to all vertices of the face it lies in. See Figure \ref{fig:ConceptFaceSplit}. Similar to the edge split, we cannot accept any input $v^\mathcal{B}_{A}$ inside an adjacent face, but $v^\mathcal{B}_{A}$ \textit{must} be a base vertex in $V^\mathcal{B}$ to preserve the embedding.
\end{enumerate}

These four \textit{atomic operations} are sufficient to represent other common mesh operations such as:

\begin{figure}[ht]
\begin{centering}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/ConceptEdgeRelocation.pdf_tex}\par
   }
  \caption[width=.9\linewidth]{Relocation of $v^\mathcal{M}_{A}$ onto $v^\mathcal{B}_{B}$.}
  \label{fig:ConceptEdgeRelocation}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
   \def\svgwidth{0.9\linewidth}
   {\centering
   \input{img/ConceptFaceRelocation.pdf_tex}\par
   }
  \caption[width=.9\linewidth]{Relocation of $v^\mathcal{M}_{A}$ onto $v^\mathcal{B}_{B}$.}
  \label{fig:ConceptFaceRelocation}
\end{subfigure}
\end{centering}
\caption{Relocation onto an edge and relocation onto a face, conceptually very similar.}
\label{fig:ConceptRelocation}
\end{figure}

\begin{enumerate}[resume]
\item \textbf{Vertex Relocation}: Moving a vertex $v^\mathcal{M}_{A}$ into one of its adjacent faces or edges into a new base vertex $v^\mathcal{B}_{B}$. This can be done by first doing a face split / edge split at $v^\mathcal{B}_{B}$, then a collapse of the halfedge $h^\mathcal{M}_{A,B}$. See Figure \ref{fig:ConceptRelocation}.
\end{enumerate}
If further operations are required, they can be easily implemented as sequences of operations, like vertex relocation was implemented as the concatenation of a split and a collapse. For instance, a vertex split as used by \cite{livesu2020obtaining} could be performed as a sequence of an edge or face split, and a vertex relocation opposite of that split.

In fact, it can be shown that our four \textit{atomic operations} are complete for trimeshes, since they allow for arbitrary changes to connectivity and vertex positions. Any  \textit{atomic operation} on a trimesh will still result in a trimesh, which is a very nice property since many algorithms run entirely on trimeshes. While our set of \textit{atomic operations} is more restrictive in what changes can be made using them, these restrictions ensure that the mesh embedding stays valid and topology is not broken. This is true for polymeshes too, and not restricted to trimeshes.

If the desired context is a polymesh and not just a trimesh, this can be achieved from a trimesh by using operations from the complete and minimal set presented at the start of this section (eg. by deleting one edge between two triangles we now have a quad and the mesh is no longer a trimesh). For convenience sake, it also makes sense to implement a vertex deletion function, which also deletes all adjacent edges, since edges with an unconnected end are usually undesirable.








