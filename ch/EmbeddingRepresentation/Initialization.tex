\section{Initialization}
\label{sec:Initialization}

In order to work with meta mesh embeddings, we first need to initialize them in some way. The embedding consists of three components:
\begin{enumerate}
\item A base mesh $\mathcal{B}$.
\item A meta mesh $\mathcal{M}$.
\item Links between $\mathcal{B}$ and $\mathcal{M}$, defining an embedding $\Phi\left(\mathcal{M}\right)$ on $\mathcal{B}$ as detailed in Section \ref{sec:DataStructure} on data structure.
\end{enumerate}
It is clear that one of the input parameters has to be $\mathcal{B}$, but afterwards we are presented with a few options for further input parameters:

\begin{enumerate}
\item $\mathcal{B}, \Phi, \mathcal{M}$: With all parameters given, the embedding is ready and no further initialization needs to be done. Unfortunately, this leaves a difficult task to the user in having to input a pre-initialized embedding.
\item $\mathcal{B}, \mathcal{M}$: Giving just the two meshes $\mathcal{B}$ and $\mathcal{M}$ and then embedding $\mathcal{M}$ onto $\mathcal{B}$ creates a similar\footnote{The difference is that Praun et al. additionally give the embedding of the vertices $\Phi\left(V^\mathcal{M}\right)$ as an input.} initialization to that of \cite{praun2001consistent}, and could be handled with a similar method for finding the embedding $\Phi$. However, designing a fitting mesh $\mathcal{M}$ in such a fashion is no trivial task, and thus the input should be further simplified.
\item $\mathcal{B}, \Phi\left(V^\mathcal{M}\right)\subset V^\mathcal{B}$: Giving a subset $\Phi\left(V^\mathcal{M}\right)\subset V^\mathcal{B}$ of the vertices of $\mathcal{B}$ as an input implicitly defines the vertex embedding which is part of $\Phi$. The connectivity and actual edges of $\mathcal{M}$ can then be triangulated. This type of input is the easiest for a user, since all that needs to be given is a set of feature points $\Phi\left(V^\mathcal{M}\right)\subset V^\mathcal{B}$.
\begin{enumerate}[label=(\alph*)]
\item A special case of this input type is $\Phi\left(V^\mathcal{M}\right)=V^\mathcal{B}$, in which case $E^\mathcal{M}=E^\mathcal{B}$ is a trivial triangulation, and with that
$\Phi\left(\mathcal{M}\right)=\mathcal{B}$. Thus, the initialization immediately terminates by copying $\mathcal{B}$ and adding the necessary pointers between $\Phi\left(\mathcal{M}\right)$ and $\mathcal{B}$.
\item A further simplification of this input type is automatically detecting or randomly assigning a set of feature points $\Phi\left(V^\mathcal{M}\right)$, then proceeding as usual.
\end{enumerate}
\end{enumerate}

\begin{wrapfigure}[41]{r}{0.23\textwidth}
  \vspace{-20pt}
   \def\svgwidth{0.22\textwidth}
   {\centering
   \input{img/RockerarmTriangulation.pdf_tex}\par
   }
   \vspace{7pt}
  \caption{$\Phi(\mathcal{M})$ triangulation pipeline.}
  \label{fig:RockerarmTriangulation}
\end{wrapfigure}

In our implementation we decide for an initialization with input parameters ''$\mathcal{B}, \Phi\left(V^\mathcal{M}\right)\subseteq V^\mathcal{B}$'', since this is the easiest for the user. We also implement a way to randomly select  $\Phi\left(V^\mathcal{M}\right)$ by giving either a ratio or a total number of vertices in  $V^\mathcal{B}$ to be selected. An initialization with inputs ''$\mathcal{B}, \Phi, \mathcal{M}$'' is implicitly supported as it already defines the whole embedding and needs no further work. And an initialization of the type ''$\mathcal{B}, \mathcal{M}$'' can be simulated by inputting $\mathcal{B}, \Phi\left(V^\mathcal{M}\right)\in\Phi\left(\mathcal{M}\right)$ and then flipping edges of $\Phi\left(\mathcal{M}\right)$ until it reaches the desired connectivity. This is always possible in our embedding, since $\Phi\left(\mathcal{M}\right)$ is a pseudo-triangulation, and thus less restrictive than traditional meshes \cite{watanabe1999diagonal}. Flips on our embedding $\Phi\left(\mathcal{M}\right)$ are very unrestricted, as we allow self-edges, and any non-border flip on $\Phi\left(\mathcal{M}\right)$ is legal.

Now the task is creating a meta mesh $\mathcal{M}$ and an embedding $\Phi$, on the base mesh $\mathcal{B}$ given an input of $\mathcal{B}$ and $\Phi\left(V^\mathcal{M}\right)\subseteq V^\mathcal{B}$. This can be done using Delaunay Triangulation \cite{delaunay1934sphere} on $\mathcal{B}$, with $\Phi\left(V^\mathcal{M}\right)$ as the feature points to grow Voronoi regions from.

The Delaunay Triangulation process is illustrated on the right. Figure \ref{fig:RockerarmTriangulation}.(a) shows the input, a base mesh $\mathcal{B}$ with selected feature vertices $\Phi\left(V^\mathcal{M}\right)$ marked in red. In the second step, Figure \ref{fig:RockerarmTriangulation}.(b), Voronoi regions are grown around each feature point. This is done by growing regions outwardly from each feature vertex, and assigning each base vertex to the feature vertex it is closest to geodesically (by navigating over surface edges). 

In the next step, the edges $E^\mathcal{M}$ and thus the whole connectivity of $\mathcal{M}$ are derived; Figure \ref{fig:RockerarmTriangulation}.(c) shows the meta mesh $\mathcal{M}$. Lastly, $\mathcal{M}$ is embedded onto $\mathcal{B}$, creating the meta mesh embedding $\Phi\left(\mathcal{M}\right)$, shown as the colored edges in Figure \ref{fig:RockerarmTriangulation}.(d).

After assigning each vertex to a Voronoi region, we derive the connectivity from the Voronoi regions by iterating over the faces of $\mathcal{B}$. Since $\mathcal{B}$ is a triangle mesh, there has to be one unique face in $\mathcal{B}$ for which the Voronoi regions of each of its three vertices differ, for each face in $\Phi\left(F^\mathcal{M}\right)$. Thus we derive the edges $\Phi\left(E^\mathcal{M}\right)$ of $\Phi\left(\mathcal{M}\right)$.

These edges $\Phi\left(E^\mathcal{M}\right)$ still need to be traced on $\mathcal{B}$ to complete the embedding $\Phi\left(\mathcal{M}\right)$. Given a very fine meta mesh $\mathcal{M}$, or thin features on $\mathcal{M}$, it can happen that a Voronoi region borders with the same Voronoi region twice. In such a case it is necessary to ensure that edges are traced through the correct border (eg. from two sides around a handle rather than twice around the same side). This is easily done by restricting edge tracing so that an edge trace cannot cross Voronoi borders \textit{except} the one corresponding to its edge.

In the case where a Voronoi region borders with itself, we stop our triangulation and ask the user for a new input -- or randomize with a new seed if the initial feature points were randomly chosen. We do the same when a Voronoi region has non-disk topology, in order to keep the initialization simple.

This type of initialization always results in a triangle mesh $\mathcal{M}$, and produces a regular mesh with nice properties. For instance, the minimum angle between two edges is maximized when using Delaunay triangulation on planar surfaces, since an edge is always shorter or equal in length to the flipped edge. With that, any flip can only result in a smaller minimum angle, not a larger one. While this and other properties of Delaunay triangulation may not entirely apply to our setting, it still ensures good properties.

Due to the ease with which a Delaunay triangulation can be computed, and the regularity of it, we believe that this is a very stable method for initialization when starting with random vertices $\Phi\left(V^\mathcal{M}\right)$. If the positions of $\Phi\left(V^\mathcal{M}\right)$ should also be optimized another approach is starting with $\Phi\left(\mathcal{M}\right)=\mathcal{B}$ and then iteratively refining $\mathcal{M}$; more on that in Chapter \ref{ch:EmbeddedIsotropicRemeshing}.

\begin{wrapfigure}[9]{r}{0.45\textwidth}
  \vspace{-20pt}
  \begin{centering}
   \includegraphics[width=0.45\textwidth]{img/HalfsphereMetamesh.png}\par
  \end{centering}
  \caption{Halfsphere triangulated}
  \label{fig:HalfsphereMetamesh}
\end{wrapfigure}

\textbf{On meshes with boundaries}: This method can be expanded to work on meshes with boundaries, by making the following additions. When selecting $\Phi\left(V^\mathcal{M}\right)$, ensure that there is at least one embedded vertex $\Phi\left(v^{\mathcal{M}}_b\right) \in b^\mathcal{B}$ for each boundary $b^\mathcal{B}$. Grow the Voronoi regions as usual and infer the connectivity from it. This connectivity is now only missing connectivity along the boundaries, which can be added by traversing each base boundary and embedding meta edges along them. The faces adjacent to those boundary edges may not be triangles yet, in which case they have to be triangulated. This ensures full connectivity, and the triangulation is done. Figure \ref{fig:HalfsphereMetamesh} shows a triangulation of a simple sphere with half of it cut off, resulting in a large boundary.
