\section{Data Structure}
\label{sec:DataStructure}

In order to embed a meta mesh $\mathcal{M}$ into a base mesh $\mathcal{B}$ further additions to the data structure are needed. To start with, here is a definition of the meta mesh components and how they embed into the base mesh:
\begin{itemize}
\item Each meta vertex $v^{\mathcal{M}}_i$ is mapped to a base vertex $v^\mathcal{B}_j$ through the embedding $\Phi\left(\mathcal{M}\right)$ so that $\Phi\left(v^\mathcal{M}_i\right)=v^\mathcal{B}_j$ (but not every base vertex has to be a meta vertex).
\item Each meta edge $e^\mathcal{M}_i$ is embedded into $\mathcal{B}$ as a contiguous sequence of base edges such that $\Phi\left(e^\mathcal{M}_i\right):=\{e^\mathcal{B}_1, e^\mathcal{B}_2, \dots, e^\mathcal{B}_n\}$. Meta edges are not allowed to intersect since that would break continuity of the embedding.
\end{itemize}

\begin{figure}
   \def\svgwidth{\textwidth}
   {\centering
   \input{img/EmbeddingDataStructure.pdf_tex}\par
   }
   \vspace{-1pt}
  \caption{Data structure of the embedded meta mesh.}
  \label{fig:embeddingdatastructure}
\end{figure}

Figure \ref{fig:embeddingdatastructure} visualizes a meta edge $e^\mathcal{M}_i$ and two meta vertices $v^\mathcal{M}_x, v^\mathcal{M}_y$ embedded into a base mesh $\mathcal{B}$. The edges $E^\mathcal{B}\in\mathcal{B}$ are colored in gray, and the vertices $V^\mathcal{B}\in\mathcal{B}$ in red, blue vertices denote base vertices that are also embedded meta vertices $\Phi\left(V^\mathcal{M}\right)$, and one embedded meta halfedge $\Phi\left(h^\mathcal{M}_j\right)=\left\{h^\mathcal{B}_1, h^\mathcal{B}_2, \dots, h^\mathcal{B}_n\right\}$ is colorized in black in the base mesh. The blue line represents the meta halfedge $h^{\mathcal{M}}_j$ as a direct connection between two vertices $v^\mathcal{M}_x, v^\mathcal{M}_y$ unlike the sequence of base edges that represents the embedding $\Phi\left(h^\mathcal{M}_j\right)$.

Much like the halfedge data structure OpenMesh is based on, the embedding is represented via a series of pointers between halfedges and vertices.

\begin{enumerate}
\item Each meta vertex $v^{\mathcal{M}}_x$ is connected to a base vertex $v^\mathcal{B}_y$ through the embedding $\Phi\left(v^\mathcal{M}_x\right)$.
\item Each base vertex $v^\mathcal{B}_y$ \textit{can be} connected to a meta vertex $v^{\mathcal{M}}_x$ if there is an embedding with $\Phi\left(v^\mathcal{M}_x\right)=v^\mathcal{B}_y$, otherwise that pointer is in a special state denotating that the base vertex $v^\mathcal{B}_y$ is not connected.
\item Each meta halfedge $h^{\mathcal{M}}_{A,N}$ is connected to the \textit{first} base halfedge $\mathbf{h}^{\mathcal{B}}_{\mathbf{A},\mathbf{B}}$ it consists of in the embedding: $\Phi\left(h^\mathcal{M}_{A,N}\right):=\left\{\mathbf{h}^{\mathcal{B}}_{\mathbf{A},\mathbf{B}}, h^{\mathcal{B}}_{B,C}, \dots, h^{\mathcal{B}}_{M,N}\right\}$.
\item Each base halfedge $h^{\mathcal{B}}_{b}$ that a meta halfedge $h^{\mathcal{M}}_{a}$ lies on is connected to that meta halfedge.
\item Each base halfedge that lies on an embedded meta halfedge such as e.g. $\Phi\left(h^\mathcal{M}_{A,D}\right)=\left\{h^{\mathcal{B}}_{A,B}, h^{\mathcal{B}}_{B,C}, h^{\mathcal{B}}_{C,D}\right\}$ points towards the next base halfedge in that sequence (if it is not the last). Here, $h^{\mathcal{B}}_{A,B}$ points to $h^{\mathcal{B}}_{B,C}$, $h^{\mathcal{B}}_{B,C}$ points to $h^{\mathcal{B}}_{C,D}$, and $h^{\mathcal{B}}_{C,D}$ points to nothing.
\end{enumerate}

While it would be possible to add additional pointers between meta mesh and base mesh elements such as eg. face pointers, the cost of updating face pointers outweighs the benefits. If e.g. each base face $f^\mathcal{B}_i$ pointed towards its embedded meta face $\Phi\left(f^\mathcal{M}_j\right)$, every time a meta edge was traced the base faces in the new meta faces would have to be traversed in a breadth-first search. But the cost of performing searches every step outweighs the benefit of quickly looking up face connections, since halfedges already allow full traversal of the embedding.

\begin{wrapfigure}[19]{r}{0.45\textwidth}
  \begin{centering}
   \includegraphics[width=0.45\textwidth]{img/CatMetaMesh.png}\par
  \end{centering}
  \caption{A base mesh with embedded meta mesh: left - The meta mesh represented explicitly: right; colored edges correspond}
  \label{fig:catmetamesh}
\end{wrapfigure}

We do not directly connect base faces to meta faces, and in practice we also see that this functionality is rarely needed. In the case that the embedded meta face of a base face needs to be looked up, this can still be done via a search at that time. It is computationally cheaper to determine the meta face associated to a base face by doing an outward search until a meta edge is reached at the moment that information is needed, rather than performing two such searches after every meta edge trace.

However, one important restriction on meta faces $f^\mathcal{M}_i\in F^\mathcal{M}$ is that they, and by extension their embeddings $\Phi\left(f^\mathcal{M}_i\right)$, are \textit{always simple}. An embedded face $\Phi\left(f^\mathcal{M}_i\right)$ consists of a set of faces $\Phi\left(f^\mathcal{M}_i\right)=\left\{f^\mathcal{B}_j\in F^\mathcal{B'} \big|  F^\mathcal{B'}\subseteq F^\mathcal{B}\right\}$ on the surface of $\mathcal{B}$, and is called simple if it has no handles and exactly one closed external boundary. Restricting meta faces to simple faces has the advantage of making tracing new edges a lot easier, since we don't have to consider how to navigate around handles. 

In this implementation, the meta mesh has an explicit representation as a mesh, which is then connected with its underlying base mesh via pointers, as seen in Figure \ref{fig:catmetamesh}. But it would also be feasible to represent the meta mesh entirely implicitly via properties on base mesh elements. It can however be argued that having an explicit representation is worth it since it makes the structure more transparent and operations on the meta mesh easier to apply. Another big advantage of having an explicit representation of the meta mesh is the speed of mesh traversal. To traverse from one meta vertex to another on the base mesh would require the traversal of a potentially long sequence of base halfedges, whereas by traversing meta mesh halfedges a lot of operations can be skipped. This increases the speed and comfort of operations on the meta mesh at the cost of the space required to represent the meta mesh $\mathcal{M}$ explicitly.